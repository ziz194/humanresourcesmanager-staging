<?php
/**
 * Quadriga Plattform Web
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c] 2016 Quadriga Media GmbH
*/

return [
    'router' => [
        'routes' => [
            'hrm' => [
                'type'    => 'Zend\Mvc\Router\Http\Hostname',
                'options' => [
                    'route'    => 'qpweb.local',
                    'defaults' => [
                        'controller' => 'QP\Hrm\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'priority' => -1000,
            ],
            'pr' => [
                'type'    => 'Zend\Mvc\Router\Http\Hostname',
                'options' => [
                    'route'    => 'qpweb-pr.local',
                    'defaults' => [
                        'controller' => 'QP\Pr\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'priority' => -1000,
            ],
        ],
    ],
];