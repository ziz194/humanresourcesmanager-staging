<?php
/**
 * Quadriga Plattform Website
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */

return [
    'static_salt'        => '2f92sa8SDdF4sbFQfaSXGQ47Gs',
    'url_encode_phrase'  => '!Cmon cnt y0u thnk of 1thing yourself!',
    'reg_token_lifetime' => (60*60*24*2),         // register link is valid for 2 days
    'rec_token_lifetime' => (60*60*24),           // recover link is valid for 24 hours
    'session_config'     => [
        'remember_me_seconds' => (60*30),         // logged in for 30 min
        'cookie_lifetime'     => time()+(60*60*24*300),  // cookie valid for 300 days
        'use_cookies'         => true,
        'cookie_secure'       => false,
        'cookie_httponly'     => true,
    ],

    'doctrine' => [
        'configuration' => [
            'orm_default' => [
                'metadata_cache' => 'array', // apc or array
                'query_cache'    => 'array', // apc or array
                'result_cache'   => 'array', // apc or array
            ],
        ],
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => 'data/DoctrineORMModule/Migrations',
            ]
        ],
        'connection' => [
            // default connection name
            'orm_default' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params'      => [
                    'charset'       => 'utf8',
                    'driverOptions' => [
                        1002 => 'SET NAMES utf8'
                    ]
                ]
            ]
        ]
    ],
    'logger' => [
        'isAllowedToSendMails' => false,
    ],
    'tracker' => [
        'params' => [
            'web_id_account'        => '',  // Web-ID (mit User Acccount)
            'session'               => '',
            'event'                 => '',
            'channel'               => '',  // Kanal // Facebook
            'device'                => '',  // Devices
            'os'                    => '',  // Betriessystem
            'browser'               => '',
            'product_id'            => '',  // Produkt (ITEM)
            'product_options'       => '',  // Produkt-Optionen
            'url'                   => '',
            'ipv4'                  => '',
            'ipv6'                  => '',
            'page_loaded'           => '',  // Seiten geladen
            'page_load_time'        => '',  // Seite geladen Uhrzeit

            // JS
//            'region'                => '',
//            'timezone'              => '',
//            'view_port'             => '',  // Web-Version // Small,Large
//            'event_object_position' => '',  // Contents Position // Navigation
//            'js_click'              => '',  // Klicken Verhalten Ja/nein
//            'js_click_params'       => '',  // Klicken/Events-Parameter (Buttons/Link/Teaser zurselben URL)
//            'js_click_time'         => '',  // Klicken Uhrzeit
//            'js_exit_page'          => '',  // Ausstiegsseite
//            'js_exit_time'          => '',  // Ausstiegsuhrzeit
        ],
    ],
    'debugging' => [
        'showHTMLCommentsOfStartingAndEndingPartials' => false,
    ],
    'main_routes' => [
        'hrm' => [
            'wissen' => [
                'entity' => 'post'
            ],
            'news' => [
                'entity' => 'post'
            ],
            'weiterbildung' => [
                'entity' => 'education'
            ],
            'jobboerse' => [
                'entity' => 'job'
            ],
            'events' => [
                'entity' => 'event'
            ],
            'experte' => [
                'entity' => 'person'
            ],
            'unternehmen' => [
                'entity' => 'company'
            ],
            'partner' => [
                'entity' => 'company'
            ],
            'merkliste' => [
                'entity' => 'bookmarks'
            ],
//            'magazin' => [
//                'entity' => 'post'
//            ]
        ]
    ]
];
