<?php
/**
 * Quadriga Plattform Web
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c] 2016 Quadriga Media GmbH
*/

return [
    'acl' => [
        'redirect_route' => [
            'options' => [
                'name' => 'error',
            ],
        ],

        'roles' => [
            \QP\User\Entity\Role::ROLE_DEFAULT
        ],

        'acl' => [
            'resources' => [
                'QP\Hrm\Controller\Index',
                'QP\Pr\Controller\Index',

                'QP\Common\Controller\Index',

                'QP\User\Controller\Auth',
                'QP\User\Controller\Profile',
                'QP\User\Form\User',
            ],
            'permissions' => [
                \QP\User\Entity\Role::ROLE_DEFAULT => [
                    'QP\Hrm\Controller\Index' => [
                        'index',
                    ],
                    'QP\Pr\Controller\Index' => [
                        'index',
                    ],
                ],
            ],
        ],
    ]
];