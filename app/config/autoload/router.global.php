<?php
/**
 * Quadriga Plattform Web
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c] 2016 Quadriga Media GmbH
*/

return [
    'router' => [
        'routes' => [
            'hrm' => [
                'type'    => 'Zend\Mvc\Router\Http\Hostname',
                'options' => [
                    'route' => '[:subdomain.]:humanresourcesmanager.de',
                    'constraints' => array(
                        'subdomain' => '([a-zA-Z0-9-]+)?([^.])',
                        'project_domain' => '(([a-zA-Z0-9-]+)\.)+([a-zA-Z0-9]+)',
                    ),
                    'defaults' => [
                        'controller' => 'QP\Hrm\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'priority' => -1000,
            ],
            'pr' => [
                'type'    => 'Zend\Mvc\Router\Http\Hostname',
                'options' => [
                    'route' => '[:subdomain.]:pressesprecher.com',
                    'constraints' => array(
                        'subdomain' => '([a-zA-Z0-9-]+)?([^.])',
                        'project_domain' => '(([a-zA-Z0-9-]+)\.)+([a-zA-Z0-9]+)',
                    ),
                    'defaults' => [
                        'controller' => 'QP\Pr\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'priority' => -1000,
            ],
        ],
    ],
];