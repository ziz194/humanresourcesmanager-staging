<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Melanie Kuhles <melanie.kuhles@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */

return [
    'doctrine' => [
        'connection' => [
            // default connection name
            'orm_default' => [
                'params'      => [
                    'host'          => '###DATABASE_HOST###',
                    'port'          => '###DATABASE_PORT###',
                    'user'          => '###DATABASE_USER###',
                    'password'      => '###DATABASE_PASSWORD###',
                    'dbname'        => '###DATABASE_DATABASE_NAME###',
                ]
            ]
        ]
    ],
    'application' => [
        'appPath'     => '###APPLICATION_ROOT_ON_SERVER###',
        'hostname'    => '###APPLICATION_HOSTNAME###',
        'environment' => '###APPLICATION_ENVIRONMENT###'
    ],
    'mail' => [
        'transport' => [
            'options' => [
                'host'              => '###MAIL_HOST###',
                'connection_class'  => '###MAIL_CONNECTION_CLASS###',
                'port'              => '###MAIL_PORT###',
                'connection_config' => [
                    'username' => '###MAIL_USERNAME###',
                    'password' => '###MAIL_PASSWORD###',
                    'ssl'      => '###MAIL_SSL###'
                ],
            ],
        ],
        'system_sender_email' => '###MAIL_SYSTEM_SENDER###',
    ],
];
