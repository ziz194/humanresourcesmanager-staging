<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */

return [
    //------------------------------------ text parts used all over the site ---

    'HRM_HEAD_TITLE_SEARCH'                             => 'Suche',
    'HRM_HEAD_TITLE_SEARCH_KEYWORD'                     => 'Suchwort...',
    'HRM_HEAD_TITLE_SEARCHsearch/post'                  => 'News - Ergebnisse',
    'HRM_HEAD_TITLE_SEARCHsearch/event'                 => 'Event - Ergebnisse',
    'HRM_HEAD_TITLE_SEARCHsearch/education'             => 'Weiterbildung - Ergebnisse',
    'HRM_HEAD_TITLE_SEARCHsearch/job'                   => 'Jobbörse - Ergebnisse',
    'HRM_HEAD_TITLE_SEARCHsearch/person'                => 'Experten - Ergebnisse',
    'HRM_HEAD_TITLE_SEARCHsearch/company'               => 'HR-Partner - Ergebnisse',
    'HRM_HEAD_TITLE_SEARCHsearch/all'                   => 'Global - Ergebnisse',
    'HRM_HEAD_TITLE_SEARCHsearch/multi'                 => 'Global - Ergebnisse',
    'HRM_HEAD_TITLE_SEARCHsearch/bookmarks'             => 'Merkliste',
    'HRM_HEAD_TITLE_404'                                => 'Seite nicht gefunden',

    'HRM_BREADCRUMB_HOME'                               => 'Home',
    'HRM_BREADCRUMB_NEWS'                               => 'News',
    'HRM_BREADCRUMB_WEITERBILDUNG'                      => 'Weiterbildung',
    'HRM_BREADCRUMB_JOBBOERSE'                          => 'Jobbörse',
    'HRM_BREADCRUMB_EVENTS'                             => 'Events',
    'HRM_BREADCRUMB_EXPERTE'                            => 'Experten',
    'HRM_BREADCRUMB_PARTNER'                            => 'HR-Partner',
    'HRM_BREADCRUMB_MAGAZIN'                            => 'Magazin',
    'HRM_BREADCRUMB_FUER-UNTERNEHMEN'                   => 'Für Unternehmen',

    'HRM_BREADCRUMB_EDUCATION_TAGUNG'                   => 'Tagungen',
    'HRM_BREADCRUMB_EDUCATION_SEMINAR'                  => 'Seminare',
    'HRM_BREADCRUMB_EDUCATION_ELEARNING'                => 'e-Learning',
    
    'HRM_BREADCRUMB_EVENTS_EXTERNAL'                    => 'Weitere Termine',

    'JOB_LIST_BUTTON_SHOW_ALL'                          => 'Alle Jobs anzeigen',
    'NEWS_LIST_BUTTON_SHOW_ALL'                         => 'Alle News anzeigen',
    'EDUCATION_LIST_BUTTON_SHOW_ALL'                    => 'Alle Kurse anzeigen',
    'EVENTS_LIST_BUTTON_SHOW_ALL'                       => 'Alle Events anzeigen',
    'EXPERTS_LIST_BUTTON_SHOW_ALL'                      => 'Alle Experten anzeigen',
    'CONTACT_BUTTON'                                    => 'Kontakt',

    'HRM_MAP_LINK'                                      => 'Karte ansehen',

    'HRM_FORM_VALIDATION_ERROR_MESSAGE'                 => 'Bitte korrigieren Sie die markierten Felder',
    'HRM_FORM_DONE_MESSAGE'                             => 'Ihre Anmeldung ist bei uns eingegangen.',

    'HRM_EVENT_HEADLINE_REVIEW'                         => 'Rückblick',
    'HRM_EVENT_HEADLINE_CONTACT'                        => 'Kontakt',

    'HRM_SEARCH_PLACEHOLDER_TOPIC'                      => 'Themen oder Stichwörter',
    'HRM_SEARCH_PLACEHOLDER_EVENTS'                     => 'Event oder Stichwörter',
    'HRM_SEARCH_PLACEHOLDER_INDUSTRY'                   => 'Industrie',
    'HRM_SEARCH_PLACEHOLDER_FORMAT'                     => 'Formate',
    'HRM_SEARCH_PLACEHOLDER_JOB'                        => 'Jobtitel oder Firmenname',
    'HRM_SEARCH_PLACEHOLDER_PLACE'                      => 'PLZ oder Ort',
    'HRM_SEARCH_PLACEHOLDER_BRANCH'                     => 'Branche',
    'HRM_SEARCH_PLACEHOLDER_COMPANY'                    => 'Firmenname, Fachbereich',
    'HRM_SEARCH_PLACEHOLDER_EXPERT'                     => 'Name, Jobtitel oder Fachbereich',

    'HRM_SEARCH_PLACEHOLDER_GLOBAL_MAIN'                => 'Alle',
    'HRM_SEARCH_PLACEHOLDER_GLOBAL_NEWS'                => 'News',
    'HRM_SEARCH_PLACEHOLDER_GLOBAL_EDUCATION'           => 'Weiterbildung',
    'HRM_SEARCH_PLACEHOLDER_GLOBAL_JOB'                 => 'Jobbörse',
    'HRM_SEARCH_PLACEHOLDER_GLOBAL_EVENTS'              => 'Events',
    'HRM_SEARCH_PLACEHOLDER_GLOBAL_PERSON'              => 'Experte',
    'HRM_SEARCH_PLACEHOLDER_GLOBAL_PARTNER'             => 'HR-Partner',

    'HRM_GLOBAL_HEADLINE_SEARCH'                        => 'Suchergebnisse',
    'HRM_GLOBAL_FILTER'                                 => 'Filter',
    'HRM_GLOBAL_FILTER_ALPHABET_ALL'                    => 'Alle',
    'HRM_GLOBAL_SEARCH_NO_RESULTS'                      => 'Ihre Filterkombination ergab leider keine Ergebnisse. Versuchen Sie beispielsweise eine andere Filterkombination oder nutzen Sie unsere allgemeine Suche.',
    'HRM_GLOBAL_FILTER_LABEL_CUSTOM_DATE'               => 'Benutzerdefiniert',
    'HRM_GLOBAL_FILTER_LABEL_FROM'                      => 'Von',
    'HRM_GLOBAL_FILTER_LABEL_TILL'                      => 'Bis',

    // filter group names
    'post_tags_p1'                                      => 'Themen',
    'post_role'                                         => 'Quelle',
    'post_type'                                         => 'Format',

    'event_tags_p1'                                     => 'Themen',
    'event_format'                                      => 'Formate',
    'event_careerLevel'                                 => 'Karriere-Level',
    'event_month'                                       => 'Zeitraum',
    'event_location'                                    => 'Ort',
    'event_costType'                                    => 'Kosten',
    'event_language'                                    => 'Sprache',

    'event-external_tags_p1'                            => 'Themen',
    'event-external_format'                             => 'Formate',
    'event-external_careerLevel'                        => 'Karriere-Level',
    'event-external_month'                              => 'Zeitraum',
    'event-external_location'                           => 'Ort',
    'event-external_costType'                           => 'Kosten',
    'event-external_language'                           => 'Sprache',

    'education_tags_p1'                                 => 'Themen',
    'education_format'                                  => 'Formate',
    'education_careerLevel'                             => 'Karriere-Level',
    'education_month'                                   => 'Zeitraum',
    'education_location'                                => 'Ort',
    'education_costType'                                => 'Kosten',
    'education_language'                                => 'Sprache',

    'job_tags_p1'                                       => 'Berufsfeld',
    'job_occupational_field'                            => 'Berufsfeld',
    'job_company'                                       => 'Unternehmen',
    'job_job_location'                                  => 'Arbeitsort',
    'job_work_experience'                               => 'Berufserfahrung',
    'job_work_hours'                                    => 'Arbeitszeit',
    'job_type_of_employment'                            => 'Anstellungsart',

    'person_tags_p1'                                    => 'Spezialgebiet',
    'person_profile.role'                               => '??Rolle??',
    'person_tags_sector'                                => 'Branchen',
    'person_worksFor'                                   => 'Unternehmen',

    'company_tags_p1'                                   => 'Spezialgebiet',
    'company_tags_sector'                               => 'Branchen',

    'bookmarks_index'                                   => 'Bereich',
    'post'                                              => 'News',
    'education'                                         => 'Weiterbildung',
    'event'                                             => 'Events',
    'job'                                               => 'Jobbörse',
    'person'                                            => 'Experten',
    'company'                                           => 'HR-Partner',

    'all_index'                                         => 'Bereich',
    'all_month'                                         => 'Zeitraum',
    'all_language'                                      => 'Sprache',

    // tags p1 (fields, professions)
    'Szene'                                             => 'Personalszene',
    'szene'                                             => 'Personalszene',
    'arbetisrecht'                                      => 'Arbetisrecht',
    'fuehrung'                                          => 'Führung',
    'beratung_personalreferent'                         => 'Beratung, Personalreferent',
    'changemanagement'                                  => 'Changemanagement',
    'hr_job_market'                                     => 'HR Arbeitsmarkt',
    'hr_business_partner'                               => 'HR-Business Partner',
    'hr_international'                                  => 'HR International',
    'hr_kommunikation'                                  => 'HR-Kommunikation',
    'hr_stategie'                                       => 'HR-Stategie',
    'hr_trend'                                          => 'HR Trend',
    'hr_it_and_learning'                                => 'HR und IT Lernen',
    'lohnabrechnung'                                    => 'Lohnabrechnung',
    'personnel_appraisal'                               => 'Personalgespräch',
    'personnel_management'                              => 'Personalmanagement',
    'personalleitung'                                   => 'Personalleitung',
    'personalsachbearbeitung'                           => 'Personalsachbearbeitung',
    'politics_society'                                  => 'Politik und Gesellschaft',
    'recruiting'                                        => 'Recruiting',
    'technology_hr'                                     => 'Technologie und HR',

    // branches
    'automotive'                                        => 'Automotive',
    'banken_versicherungen'                             => 'Banken & Versicherungen',
    'bau_immobilien'                                    => 'Bau & Immobilien',
    'bildung_kultur'                                    => 'Bildung & Kultur',
    'chemie_pharma'                                     => 'Chemie & Pharma',
    'dienstleistung'                                    => 'Dienstleistung',
    'Energie_versorgung'                                => 'Energie & Versorgung',
    'gastronomie_touristik_freizeit'                    => 'Gastronomie, Touristik & Freizeit',
    'gesundheit_soziales'                               => 'Gesundheit & Soziales',
    'handel'                                            => 'Handel',
    'industrie'                                         => 'Industrie',
    'it_telekommunikation'                              => 'IT & Telekommunikation',
    'konsumgueter_nahrungsmittel'                       => 'Konsumgüter & Nahrungsmittel',
    'maschinenbau_metallindustrie'                      => 'Maschinenbau & Metallindustrie',
    'medien'                                            => 'Medien',
    'oeffentlicher_dienst'                              => 'Öffentlicher Dienst',
    'recht'                                             => 'Recht',
    'verarbeitungsindustrie'                            => 'Verarbeitungsindustrie',
    'verkehr_logistik'                                  => 'Verkehr & Logistik',

    // career level
    'mit_berufserfahrung'                               => 'Mit Berufserfahrung',
    'ohne_berufserfahrung'                              => 'Ohne Berufserfahrung',
    'mit_personalverantwortung'                         => 'Mit Personalverantwortung',

    // working hours
    'vollzeit'                                          => 'Vollzeit',
    'teilzeit'                                          => 'Teilzeit',

    // type of employment
    'festanstellung'                                    => 'Festanstellung',
    'befristeter_vertrag'                               => 'Befristeter Vertrag',
    'praktikum'                                         => 'Praktikum',
    'studentenjob'                                      => 'Studentenjob',

    //  Contact form text
    'HRM_CONTACT_PLACEHOLDER_TITLE'                     => 'Anrede',
    'HRM_CONTACT_SALUTATION_MALE'                       => 'Herr',
    'HRM_CONTACT_SALUTATION_FEMALE'                     => 'Frau',
    'HRM_CONTACT_PLACEHOLDER_NAME'                      => 'Vorname',
    'HRM_CONTACT_PLACEHOLDER_LASTNAME'                  => 'Name',
    'HRM_CONTACT_PLACEHOLDER_EMAIL'                     => 'E-mail',
    'HRM_CONTACT_PLACEHOLDER_TEL'                       => 'Telefon',
    'HRM_CONTACT_PLACEHOLDER_COMPANY'                   => 'Firma',
    'HRM_CONTACT_PLACEHOLDER_POSITION'                  => 'Position',
    'HRM_CONTACT_PLACEHOLDER_MESSAGE'                   => 'Nachricht',

    'CONTACT_MAIL_SUBJECT'                              => 'Eine Kontaktanfrage von HRM', // TODO: Make this dynamic
    'EVENT_SUBMIT_MAIL_SUBJECT'                         => 'Eine Eventeinreichung von HRM', // TODO: Make this dynamic

    'HRM_CONTACT_PLACEHOLDER_REQUEST'                   => 'Anliegen',
    'HRM_CONTACT_REQUEST_1'                             => 'Was soll das hier?',
    'HRM_CONTACT_REQUEST_2'                             => 'Warum nicht in Rot?',
    'HRM_CONTACT_REQUEST_3'                             => 'Wiso eigentlich 42?',
    'HRM_CONTACT_REQUEST_4'                             => 'Sind wir schon da?',

    // event format
    'mba_study'                                         => 'MBA Studium',
    'seminare_intensiv'                                 => 'Seminare - Intensiv',
    'seminare_standard'                                 => 'Seminare - Standard',
    'seminare_in_house'                                 => 'Seminare - In-House',
    'elearning_live_stream'                             => 'eLearning - Live Stream',
    'elearning_video_on_demand'                         => 'eLearning - Video on demand',
    'elearning_podcast'                                 => 'eLearning - Podcast',
    'non_degree'                                        => 'Non Degree',
    'conference'                                        => 'Tagung',
    'congress'                                          => 'Kongress',
    'award'                                             => 'Award',

    // event education level
    'anfaenger'                                         => 'Anfänger',
    'professional'                                      => 'Professional',
    'management'                                        => 'Management',

    // event cost types
    'free'                                              => 'Kostenlos',
    'paid'                                              => 'Kostenplfichtig',

    // event languages
    'deutsch'                                           => 'Deutsch',
    'english'                                           => 'English',
    'other'                                             => 'Andere',


    // article roles and formats
    'company'                                           => 'HR-Partner',
    'hrm_author'                                        => 'HRM Autoren',
    'hrm_referent'                                      => 'HRM Referent',
    'guest'                                             => 'Gastautoren',
    'article'                                           => 'Artikel',
    'interview'                                         => 'Interview',

    //Contact form error message
    'HRM_CONTACT_ERROR_NAME'                            => 'Bitte geben Sie Ihren Vornamen ein',
    'HRM_CONTACT_ERROR_LASTNAME'                        => 'Bitte geben Sie Ihren Namen ein',
    'HRM_CONTACT_ERROR_VALIDMAIL'                       => 'Bitte geben Sie eine gültige e-Mail Adresse ein',
    'HRM_CONTACT_ERROR_MAIL'                            => 'Bitte geben Sie Ihre e-Mail Adresse ein',
    'HRM_CONTACT_ERROR_TEL'                             => 'Bitte geben Sie Ihre Telefonnummer ein',
    'HRM_CONTACT_ERROR_VALIDTEL'                        => 'Bitte geben Sie eine gültige Telefonnummer',
    'HRM_CONTACT_ERROR_MESSAGE'                         => 'Bitte geben Sie eine Nachricht ein',
    'HRM_CONTACT_ERROR_NAME_LENGTH'                     => 'Bitte geben Sie einen Namen mit einer Länge zw. 2 und 255 Zeichen ein.',
    'HRM_CONTACT_ERROR_CHOICE'                          => 'Bitte wählen Sie mindestens einen Newsletter aus.',

    'HRM_ARTICLE_HEADLINE_AUTHOR_BOX'                   => 'Informationen zum Autor',
    'HRM_ARTICLE_HEADLINE_MORE_ARTICLE_BOX'             => 'Weitere Artikel von',
    'HRM_ARTICLE_HEADLINE_NEXT_COURSES_BOX'             => 'Nächste Kurse von',

    'HRM_JOB_POST_HEADLINE'                             => 'Jobinformationen',
    'HRM_JOB_POST_DESCRIPTION'                          => 'Jobbeschreibung',

    'HRM_JOB_POST_CONTACT_HEADLINE'                     => 'Kontaktinformationen',

    'HRM_CONTACT_ME'                                    => 'Kontaktieren',
    'HRM_FOLLOW'                                        => '+ Folgen',
    'HRM_REGISTER'                                      => 'Jetzt anmelden',
    'HRM_CONTACT_NOW'                                   => 'Jetzt Kontaktieren',
    'HRM_EARLY0BIRD_PRICE'                              => 'Frühbucherpreis',

    //Key visuals Buttons
    'HRM_READ_ARTICLE'                                  => 'Artikel Durchlesen',
    'ACCORDION_CONTENT_MORE_POSTS'                      => 'weitere Beiträge',

    'HRM_REQUEST_FILE_PER_MAIL_DESC'                    => 'Bitten senden Sie mir das White Paper an folgende E-Mail-Adresse.',
    'HRM_REQUEST_FILE_PER_MAIL_PLACEHOLDER'             => 'E-mail',

    'HRM_EXPERT_HEADLINE_PROFILE'                       => 'Experten-Profil',
    'HRM_EXPERT_HEADLINE_PROFILE_ABOUT'                 => 'Zur Person',
    'HRM_EXPERT_HEADLINE_PROFILE_SECTION'               => 'Fachbereiche',
    'HRM_EXPERT_HEADLINE_INTERVIEW'                     => 'Interview mit',
    'HRM_EXPERT_HEADLINE_POSTS'                         => 'Beiträge',
    'HRM_EXPERT_HEADLINE_EVENTS'                        => 'Angebote',
    'HRM_EXPERT_HEADLINE_CONTACT'                       => 'Kontaktinformationen',

    'HRM_COMPANY_HEADLINE_PROFILE'                      => 'Unternehmensrofil',
    'HRM_COMPANY_HEADLINE_PROFILE_ABOUT'                => 'Über',
    'HRM_COMPANY_HEADLINE_PROFILE_SECTION'              => 'Fachbereiche',
    'HRM_COMPANY_HEADLINE_FACTS'                        => 'Unternehmensfakten',
    'HRM_COMPANY_HEADLINE_WHITE_PAPERS'                 => 'White Papers',
    'HRM_COMPANY_HEADLINE_GALLERY'                      => 'Bilder & Videos',
    'HRM_COMPANY_HEADLINE_POSTS'                        => 'Unternehmensbeiträge',
    'HRM_COMPANY_HEADLINE_CONTACT'                      => 'Kontaktinformationen',

    'HRM_STUDY_HEADLINE_CONCEPT'                        => 'Studienkonzept',
    'HRM_STUDY_HEADLINE_STRUCTURE'                      => 'Struktur',
    'HRM_STUDY_HEADLINE_CURRICULUM'                     => 'Curriculum',
    'HRM_STUDY_HEADLINE_REQUIREMENTS'                   => 'Studienvoraussetzungen',
    'HRM_STUDY_HEADLINE_PROGRESSION'                    => 'Studienverlauf & Termine',
    'HRM_STUDY_HEADLINE_PROGRESSION_OVERVIEW'           => 'Übersicht',
    'HRM_STUDY_BULLET_PROGRESSION_DURATION'             => 'Studiendauer:',
    'HRM_STUDY_BULLET_PROGRESSION_PRESENCE'             => 'Präsenzzeit:',
    'HRM_STUDY_BULLET_PROGRESSION_TOUR'                 => 'Studienreise:',
    'HRM_STUDY_PROGRESSION_DAYS'                        => 'Tage',
    'HRM_STUDY_PROGRESSION_MONTHS'                      => 'Monate (berufsbegleitend)',
    'HRM_STUDY_HEADLINE_FINANCING'                      => 'Kosten, Finanzierung & Stipendium',
    'HRM_STUDY_HEADLINE_MENTORING'                      => 'Mentoringprogramm',
    'HRM_STUDY_HEADLINE_ADVISORY_BOARD'                 => 'Beirat',
    'HRM_STUDY_HEADLINE_ADVISORY_MEMBERS'               => 'Beiratsmitglieder',
    'HRM_STUDY_HEADLINE_TOUR'                           => 'Studienreise',
    'HRM_STUDY_HEADLINE_APPLICATION'                    => 'Bewerbungsprozess',
    'HRM_STUDY_HEADLINE_PAPERS'                         => 'Bewerbungsunterlagen',
    'HRM_STUDY_HEADLINE_CONTACT'                        => 'Ansprechpartner',

    'HRM_SEMINAR_HEADLINE_CONTENT'                      => 'Programminhalte',
    'HRM_SEMINAR_HEADLINE_REFERENT'                     => 'Referent(in)',
    'HRM_SEMINAR_HEADLINE_TARGET_AUDIENCE'              => 'Zielgruppe',
    'HRM_SEMINAR_HEADLINE_CONTACT'                      => 'Ansprechpartner',
    'HRM_SEMINAR_HEADLINE_TRAVEL_SERVICE'               => 'Anreise &amp; Services',
    'HRM_SEMINAR_HEADLINE_REVIEW'                       => 'Rückblick',

    'HRM_ELEARNING_HEADLINE_PROGRAMME_PERFORMERS'       => 'Modulinhalte und Referenten',
    'HRM_ELEARNING_HEADLINE_PERFORMER'                  => 'Referent(in)',
    'HRM_ELEARNING_HEADLINE_PERFORMERS'                 => 'Referenten',
    'HRM_ELEARNING_HEADLINE_TARGET_AUDIENCE'            => 'Zielgruppe',
    'HRM_ELEARNING_HEADLINE_CONTACT'                    => 'Ansprechpartner',
    'HRM_ELEARNING_HEADLINE_TRAVEL_SERVICE'             => 'Anreise &amp; Services',
    'HRM_ELEARNING_HEADLINE_REVIEW'                     => 'Rückblick',
    'HRM_ELEARNING_TIME_TILL'                           => 'bis',
    'HRM_ELEARNING_HEADLINE_MODULE'                     => 'Modul',

    'HRM_CONFERENCE_HEADLINE_AGENDA'                    => 'Programm',
    'HRM_CONFERENCE_DOWNLAOD_AGENDA'                    => 'Download Programm',
    'HRM_AGENDA_TIME_FROM'                              => 'ab',
    'HRM_AGENDA_TIME_TILL'                              => 'bis',
    'HRM_AGENDA_MORE_BUTTON'                            => 'Mehr Informationen',
    'HRM_AGENDA_LESS_BUTTON'                            => 'Weniger Informationen',


    'HRM_ARTICLES_GO_TO_ARTICLE_LIST'                   => 'Zur Artikel Liste',
    'HRM_ARTICLES_NEXT_ARTICLE'                         => 'Nächster Artikel',
    'HRM_ARTICLES_PREVIOUS_ARTICLE'                     => 'Vorheriger Artikel',
    'HRM_SHOW_ALL_COURSES'                              => 'Alle Kurse',

    'HRM_READ_MORE'                                     => 'weiterlesen',
    'HRM_SHOW_ALL'                                      => 'Alle Anzeigen',
    'HRM_MORE_INFORMATION'                              => 'Mehr Informationen',
    'HRM_REGISTER_EVENT'                                => 'Event Melden',

    'HRM_A_MAGAZINE_BY'                                 => 'Ein Magazin von:',
    'HRM_MAGAZINE_ISSUE'                                => 'Ausgabe:',

    'HRM_FORM_SUBMIT'                                   => 'Absenden',

    //Terminmeldung form
    'HRM_EVENT_FORM_TITLE'                              => 'Titel der Veranstaltung',
    'HRM_EVENT_FORM_TYPE'                               => 'Art der Veranstaltung',
    'HRM_EVENT_FORM_THEME'                              => 'Themenfeld',
    'HRM_EVENT_FORM_DESCRIPTION'                        => 'Kurzbeschreibung von Teilnehmern & Thema (max. 250 Zeichen)',
    'HRM_EVENT_FORM_HOST'                               => 'Veranstalter',
    'HRM_EVENT_FORM_LOCATION'                           => 'Veranstaltungsort',
    'HRM_EVENT_FORM_STREET'                             => 'Straße',
    'HRM_EVENT_FORM_PLZ'                                => 'PLZ',
    'HRM_EVENT_FORM_TEL'                                => 'Telefon',
    'HRM_EVENT_FORM_PARTNER'                            => 'Ansprechpartner',
    'HRM_EVENT_FORM_DATE_FROM'                          => 'Von ',
    'HRM_EVENT_FORM_DATE_UNTIL'                         => 'Bis ',
    'HRM_EVENT_FORM_EMAIL'                              => 'E-mail',
    'HRM_EVENT_FORM_HOUR'                               => 'Stunde ',
    'HRM_EVENT_FORM_MINUTES'                            => 'Minuten',
    'HRM_EVENT_FORM_ALLDAY'                             => 'Ganztägig',
    'HRM_EVENT_GUEST_REGISTER'                          => 'Nur mit Anmeldung',
    'HRM_EVENT_GUEST_WITH_COSTS'                        => 'Mit Kostenbeitrag',
    'HRM_EVENT_GUEST_MEMBERS_ONLY'                      => 'Nur für Mitglieder/geladene Gäste',
    // submit events types
    'HRM_EVENT_FORM_TYPE_SEMINAR'                       => 'Seminar',
    'HRM_EVENT_FORM_TYPE_TAGUNG'                        => 'Tagung',
    'HRM_EVENT_FORM_TYPE_E_LEARNING'                    => 'e-learning',
    'HRM_EVENT_FORM_TYPE_FORUM'                         => 'Forum',
    'HRM_EVENT_FORM_TYPE_AWARD'                         => 'Award',
    'HRM_EVENT_FORM_TYPE_KONGRESS'                      => 'Kongress',
    'HRM_EVENT_FORM_TYPE_GALA'                          => 'Gala',
    'HRM_EVENT_FORM_TYPE_FESTIVAL'                      => 'Festival',
    'HRM_EVENT_FORM_TYPE_ABENDVERANSTALTUNG'            => 'Abendveranstaltung',
    'HRM_EVENT_FORM_TYPE_MESSE'                         => 'Messe',
    'HRM_EVENT_FORM_TYPE_TAG_OFFENE_TUER'               => 'Tag der offenen Tür',
    'HRM_EVENT_FORM_TYPE_NETWORKING'                    => 'Networking',
    'HRM_EVENT_FORM_TYPE_ONLINE_EVENT'                  => 'Online Event',
    'HRM_EVENT_FORM_TYPE_SONSTIGE'                      => 'Sonstige',

    //Terminmeldung error messages
    'HRM_EVENT_FORM_TITLE_EMPTY'                        => 'Bitte geben Sie den Titel der Veranstaltung ein ',
    'HRM_EVENT_FORM_THEME_EMPTY'                        => 'Bitte geben Sie das Themenfeld ein ',
    'HRM_EVENT_FORM_DATE_FROM_EMPTY'                    => 'Bitte ein Startdatum eintragen ',
    'HRM_EVENT_FORM_DATE_UNTIL_EMPTY'                   => 'Bitte ein Enddatum eintragen ',
    'HRM_EVENT_FORM_DESCRIPTION_EMPTY'                  => 'Bitte geben Sie eine Kurzbeschreibung ein',
    'HRM_EVENT_FORM_HOST_EMPTY'                         => 'Bitte den Namen des Veranstalters eintragen',
    'HRM_EVENT_FORM_LOCATION_EMPTY'                     => 'Bitte den Ort eintragen',
    'HRM_EVENT_FORM_PARTNER_EMPTY'                      => 'Bitte den Ansprechpartner eintragen',
    'HRM_EVENT_FORM_HOUR_EMPTY'                         => 'Bitte die Stunde eingeben" ',
    'HRM_EVENT_FORM_MINUTES_EMPTY'                      => 'Bitte die Minuten eingeben"',
    'HRM_CONTACT_ERROR_VALIDHOUR'                       => 'Bitte eine Stunde zwischen 00 und 23 eintragen',
    'HRM_CONTACT_ERROR_VALIDMINUTES'                    => 'Bitte eine Minute zwischen 00 und 59 eintragen',


    'HRM_EVENTS_CONGRESS_LINK_TO_EVENT'                 => 'Zur Kongresswebseite',
    'HRM_EVENTS_CONGRESS_PERFORMER'                     => 'ReferentenInnen',

    'HRM_EVENTS_AWARD_LINK_TO_EVENT'                    => 'Zur Awardwebseite',
    'HRM_EVENTS_AWARD_DATE_NAME'                        => 'Gala am',

    'HRM_DOWNLOAD_MEDIA_DATA'                           => 'Mediadaten herunterladen',
    'HRM_DOWNLOAD_PRODUCT_INFORMATION'                  => 'Produktinformationen herunterladen',
    'HRM_DOWNLOAD_PRODUCT_SELECT'                       => 'Auswählen',

    // newsletter error messages
    'HRM_NEWSLETTER_HRM'                                => 'HR-Presseschau',
    'HRM_NEWSLETTER_PZ'                                 => 'Personalszene',
    'HRM_NEWSLETTER_HR_WORK'                            => 'HR-Arbeitsmarkt',

    'HRM_NEWSLETTER_DESC_HRM'                           => 'Die HRM-Presseschau liefert einen täglichen Überblick über personalrelevante Nachrichten aus anderren Medien sowie tagesaktuelle Stellenausschreibungen.',
    'HRM_NEWSLETTER_DESC_PZ'                            => 'Die Personalszene berichtet über die aktuellen Wechsel aus dem HR-Bereich.',
    'HRM_NEWSLETTER_DESC_HR_WORK'                       => 'Im HR-Arbeitsmarkt werden Stellenausschreibungen rund um das Personalmanagement veröffentlicht.',

    'HRM_NEWSLETTER_SUBSCRIBE'                          => 'Newsletter abonieren',

    'HRM_NEWSLETTER_SUBSCRIBE_DESC_HRM'                 => 'Ich möchte den täglich erscheinenden Newsletter "HR-Presseschau" sowie weitere Informationen zu Veranstaltungen und Angeboten der Quadriga Media Berlin GmbH per E-Mail erhalten. ',
    'HRM_NEWSLETTER_SUBSCRIBE_DESC_PZ'                  => 'Ich möchte den regelmäßig erscheinenden Newsletter "Personalszene" sowie weitere Informationen zu Veranstaltungen und Angeboten der Quadriga Media Berlin GmbH per E-Mail erhalten. ',
    'HRM_NEWSLETTER_SUBSCRIBE_DESC_HR_WORK'             => 'Ich möchte den regelmäßig erscheinenden Newsletter "HR-Arbeitsmarkt" sowie weitere Informationen zu Veranstaltungen und Angeboten der Quadriga Media Berlin GmbH per E-Mail erhalten.',

    'HRM_NEWSLETTER_FORM_FIRST_NAME'                    => 'Vorname',
    'HRM_NEWSLETTER_FORM_LAST_NAME'                     => 'Nachname',
    'HRM_NEWSLETTER_FORM_EMAIL'                         => 'E-Mail',

    //Right Teaser Titles
    'HRM_FOR_EMPLOYER_BOX'                              => 'Für Arbeitgeber',
    'HRM_PARTNER_FOR_BANNER'                            => 'Ansprechpartner für Anzeigenschaltung',

    // shop form
    'SHOP_FORM_DELETE_PARTICIPANT'                      => 'Teilnehmer löschen',
    'SHOP_FORM_ADD_PARTICIPANT'                         => 'Weitere Teilnehmer',
    'SHOP_FORM_PARTICIPANTS_HL'                         => 'Teilnehmer',
    'SHOP_FORM_SHOPPING_CART_BUTTON_TITLE'              => 'In den Warenkorb',
    'SHOP_FORM_ORDER_BUTTON_TITLE'                      => 'Sofort kaufen',
    'SHOP_FORM_DOWNLOAD_FAX_PDF'                        => 'Download FAX-Anmeldeformular',
];
