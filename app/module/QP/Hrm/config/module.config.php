<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c] 2016 Quadriga Media GmbH
 */
namespace QP\Hrm;

return [
    //------------------------------------------------------------- Services ---
    'service_manager' => [
        'aliases' => [
            'translator' => 'MvcTranslator',
        ],
    ],
    //---------------------------------------------------------- Controllers ---
    'controllers' => [
        'invokables' => [
            'QP\Hrm\Controller\Index' => 'QP\Hrm\Controller\IndexController',
            'QP\Hrm\Controller\Ajax'  => 'QP\Hrm\Controller\AjaxController',
        ],
    ],
    //---------------------------------------------------------------- Views ---
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'controller_map' => [
            'QP\User' => true,
        ],
    ],
    //--------------------------------------------------------------- Routes ---
    'router' => [
        'routes' => [
            'ajax-search' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/ajax/search/:entity',
                    'defaults' => [
                        'controller' => 'QP\Hrm\Controller\Ajax',
                        'action'     => 'search',
                    ],
                ],
            ],
            'end-point' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/:main[/:tag_1][/:id].html',
                    'defaults' => [
                        'controller' => 'QP\Hrm\Controller\Index',
                        'action'     => 'endpoint',
                    ],
                ],
            ],
        ],
    ],
    //------------------------------------------------------------- Doctrine ---
//    'doctrine' => [
//        'driver' => [
//            __NAMESPACE__ . '_driver' => [
//                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
//                'cache' => 'array',
//                'paths' => [
//                    __DIR__ . '/../src/' . str_replace('\\', '/', __NAMESPACE__) . '/Entity'
//                ],
//            ],
//            'orm_default' => [
//                'drivers' => [
//                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
//                ]
//            ]
//        ],
//    ],
    //----------------------------------------------------------- Translator ---
    'translator' => [
        'locale' => 'de_DE',
        'translation_file_patterns' => [
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php'
            ],
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../../../../vendor/zendframework/zend-i18n-resources/languages/',
                'pattern'  => '%s/Zend_Captcha.php'
            ],
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../../../../vendor/zendframework/zend-i18n-resources/languages/',
                'pattern'  => '%s/Zend_Validate.php'
            ]
        ],
    ],
];
