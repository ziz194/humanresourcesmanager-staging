<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Hrm\ViewLogic\Index;

use QP\Common\ViewLogic;
use QP\Common\Entity\Page;
use QP\Common\ViewLogic\Response;

/**
 * Class EndPointLogic
 * @package QP\Hrm\ViewLogic\Index
 */
class EndPointLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{
    const PORTAL_PREFIX = 'hrm';
    const PARTIAL_PATH  = 'qp/hrm/partials/';

    /**
     * @var string
     */
    private $main;

    /**
     * @var string
     */
    private $entity;

    /**
     * @var string
     */
    private $tag1;

    /**
     * @var string
     */
    private $id;

    /**
     * @var array $config
     */
    private $config;

    /**
     * IndexLogic constructor.
     *
     * @param string $main
     * @param string $tag1
     * @param string $id
     * @param array  $config
     */
    public function __construct($main, $tag1, $id, $config)
    {
        $this->main   = $main;
        $this->tag1   = $tag1;
        $this->id     = $id;
        $this->config = $config;

        if (!$this->id ) {
            $this->id = $this->tag1;
        }
    }

    /**
     * Generate the view response.
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse()
    {
        $portal = $this->getPortalConfig(self::PORTAL_PREFIX);
        //\FirePHP::getInstance(true)->info($nav);

        try {
            \FirePHP::getInstance(true)->info('detail');

            // resolve url to entity
            if (isset($this->config['main_routes'][self::PORTAL_PREFIX][$this->main]['entity'])) {
                $this->entity = $this->config['main_routes'][self::PORTAL_PREFIX][$this->main]['entity'];
                //\FirePHP::getInstance(true)->info($this->entity);
            } else {
                $msg = 'The requested end point entity could not be resolved by the given url ('.$this->main.', '.$this->id.'.html)!'
                     . "\n\n".print_r($this->config['main_routes'], true);
                $e   = new \Exception($msg);
                $this->getErrorLogger()->logException($e);
                throw $e;
            }

            $endPoint = $this->getItems($this->entity, '/'.$this->id.'.html', $portal['tags'], '');
            //\FirePHP::getInstance(true)->info($endPoint);

            $title = '';
            if (isset($endPoint['data'][0]['title'])) {
                $title = $endPoint['data'][0]['title'];
            }
            if (isset($endPoint['data'][0]['name'])) {
                $title = $endPoint['data'][0]['name'];
            }
            $this->layout->setVariable('headTitle', ucfirst($this->main).' - '.$title);
            $this->layout->setVariable('dataNode', 'detail-'.str_replace('/','-',$this->entity));

            $config = [
                'endPoint' => $endPoint['data'][0],
                'main'     => $this->main,
                'template' => $this->entity,
                'config'   => [
                    'mainRoutes'   => $this->config['main_routes'][self::PORTAL_PREFIX],
                    'tags'         => $portal['tags'],
                    'portal'       => self::PORTAL_PREFIX,
                    'partialPath'  => self::PARTIAL_PATH,
                    'activePageId' => $endPoint['data'][0]['_id'],
                    'pathParts'    => [$this->main,$this->id.'.html'],
                    'breadMiddle'  => $endPoint['data'][0],
                    'breadEndName' => $title,
                ],
            ];

            $productId      = '';
            $productOptions = '';
            // TODO: This is only pseudo code!!!
            if (isset($config['endPoint']['shopData']['id'])) {
                $productId = $config['endPoint']['shopData']['id'];
            }
            if (isset($config['endPoint']['shopData']['options'])) {
                $productOptions = $config['endPoint']['shopData']['options'];
            }
            $this->getTracker()->trackPageLoad($this->handleUser([]), $this->trackTime, [
                'product_id'      => $productId,
                'product_options' => $productOptions,
            ]);

            return new Response\TemplateResponse(
                'qp/hrm/index/index',
                [
                    'nav'        => $portal['nav'],
                    'config'     => $config,
                ]
            );

        } catch (\Exception $e) {
            \FirePHP::getInstance(true)->warn('('.$this->main.') '.$this->entity.', '.$this->id);
            \FirePHP::getInstance(true)->warn($e->getMessage());
            $this->getErrorLogger()->logException($e);
            $this->layout->setVariable('headTitle', 'HRM_HEAD_TITLE_404');
            return new Response\TemplateResponse(
                'qp/common/error/404',
                [
                    'httpStatus' => 404,
                    'nav'        => $portal['nav'],
                    'config'     => [
                        'config' => [
                            'portal'       => self::PORTAL_PREFIX,
                            'tags'         => $portal['tags'],
                            'activePageId' => '404',
                            'pathParts'    => $this->main,
                            'breadMiddle'  => '',
                            'breadEndName' => '404',
                        ],
                    ],
                ]
            );
        }
    }
}
