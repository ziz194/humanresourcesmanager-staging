<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Hrm\ViewLogic\Index;

use QP\Common\ViewLogic;
use QP\Common\Entity\Page;
use QP\Common\ViewLogic\Response;

/**
 * Class IndexLogic
 * @package QP\Hrm\ViewLogic\Index
 */
class IndexLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{
    const PORTAL_PREFIX = 'hrm';
    const PARTIAL_PATH  = 'qp/hrm/partials/';

    /**
     * @var string
     */
    private $path;
    /**
     * @var string
     */
    private $pathParts;

    /**
     * @var array $params
     */
    private $params;

    /**
     * @var Page
     */
    private $page;

    /**
     * @var array $config
     */
    private $config;

    /**
     * IndexLogic constructor.
     *
     * @param string $path
     * @param array  $params
     * @param array  $config
     */
    public function __construct($path, $params, $config)
    {
        $this->config    = $config;
        $this->params    = $params;
        $this->path      = $path;
        $this->pathParts = preg_split('/[\/._]/', substr($this->path,1));
    }

    /**
     * Generate the view response.
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse()
    {
        $this->getTracker()->trackPageLoad($this->handleUser([]), $this->trackTime);

        $portal = $this->getPortalConfig(self::PORTAL_PREFIX);
        //\FirePHP::getInstance(true)->info($nav);
        //\FirePHP::getInstance(true)->info($this->path);
        /** @var \QP\Common\Entity\Nav\Node $node */
        $node = $portal['nav']->getChildNodeByPath($this->path);
        //\FirePHP::getInstance(true)->info($node);

        try {
            // ------------ check if the given URL path is in the navigation ---
            if (!is_null($node) && !isset($this->params['q'])) {
                \FirePHP::getInstance(true)->info('index - nav');
                // fetch page data from cache or api
                $cache   = $this->getCacheAdapter();
                $key     = md5('page_'.$node->getId());
                $success = false;
                /** @var $nav \QP\Common\Entity\Page */
                $this->page = $cache->getItem($key, $success);
                if (!$success) {
                    $this->page = $this->getApiClient()->getRepository('QP\Common\Entity\Page')->readOne($node->getId());
                    $cache->setItem($key, $this->page);
                }
                //\FirePHP::getInstance(true)->info($this->page);
                if (is_null($this->page) || !$this->page) {
                    $e = new \Exception('The requested Page could not be found!');
                    $this->getErrorLogger()->logException($e);
                    throw $e;
                }

                $this->layout->setVariable('headTitle', $this->page->getTitle());
                $this->layout->setVariable('dataNode', $node->getId());
                $this->layout->setVariable('dataParentId', $node->getParentId());

                $config = [
                    'page'     => $this->page,
                    'template' => $this->page->getTemplate(),
                    'config'   => [
                        'mainRoutes'   => $this->config['main_routes'][self::PORTAL_PREFIX],
                        'tags'         => $portal['tags'],
                        'aggWhiteList' => $portal['aggWhiteList'],
                        'portal'       => self::PORTAL_PREFIX,
                        'partialPath'  => self::PARTIAL_PATH,
                        'activePageId' => $node->getId(),
                        'pathParts'    => $this->pathParts,
                        'breadMiddle'  => '',
                        'breadEndName' => $this->page->getTitle(),
                    ],
                ];

                $config['content'] = $this->preparePage($config, self::PORTAL_PREFIX);
                //\FirePHP::getInstance(true)->info($config);
                return new Response\TemplateResponse(
                    'qp/hrm/index/index',
                    [
                        'nav'    => $portal['nav'],
                        'config' => $config,
                    ]
                );
            }
            // ---------------------------------------- if not search for it ---
            else {
                \FirePHP::getInstance(true)->info('index - search');
                $searchFail  = false;
                $entity      = 'search/all';
                $query       = '?from=0&size=15';
                $queryFilter = $this->pathParts;
                $searchAfterLoading = false;
                $q = '';
                if (isset($this->params['q']) && $this->params['q']) {
                    $q = $this->params['q'];
                    $searchAfterLoading = true;
                }

                // check if a main can be found
                if (isset($queryFilter[0]) && isset($this->config['main_routes'][self::PORTAL_PREFIX][$queryFilter[0]]['entity'])) {
                    $entity = 'search/'.$this->config['main_routes'][self::PORTAL_PREFIX][$queryFilter[0]]['entity'];
                    unset($queryFilter[0]);
                }

                if (count($this->params))
                {
                    unset($this->params['q']);
                    foreach ($this->params as $param => $value) {
                        if(!in_array($value, $queryFilter)) {
                            $queryFilter[$param] = $value;
                        }
                    }
                }

                $all    = $this->getItems($entity, $query, $portal['tags'], self::PORTAL_PREFIX);
                $params = $this->matchParamsByAggs($queryFilter, $all['aggs'], $q, $this->params);
                $query  = $params['query'];
                $q      = $params['q'];
                try {
                    if ($params['hsc'] === 200) {
                        $data  = $this->getItems($entity, $query, $portal['tags'], self::PORTAL_PREFIX);
                    }
                } catch (\Exception $e) {
                    $data       = $all;
                    $searchFail = true;
                }
                $data['aggs'] = $all['aggs'];


                //\FirePHP::getInstance(true)->info($data);
                if (is_null($data) || !$data) {
                    $e = new \Exception('The requested search had a bad result set! ('.$entity.$query.')');
                    $this->getErrorLogger()->logException($e);
                    throw $e;
                }

                if ($params['hsc'] === 200) {
                    $this->layout->setVariable('headTitle', 'HRM_HEAD_TITLE_SEARCH'.$entity);
                } else {
                    $this->layout->setVariable('headTitle', 'HRM_HEAD_TITLE_404');
                }
                $this->layout->setVariable('dataNode', 'detail-'.str_replace('/','-',$entity));

                $config = [
                    'fail'         => $searchFail,
                    'headline'     => '',
                    'items'        => $data['data'],
                    'entity'       => $entity,
                    'aggWhiteList' => $portal['aggWhiteList'][$entity],
                    'aggs'         => $data['aggs'],
                    'pagination'   => $data['pagination'],
                    'queryFilter'  => $queryFilter,
                    'queryQ'       => $q,
                    'template'     => 'search',
                    'searchAfterLoading' => $searchAfterLoading,
                    'config' => [
                        'mainRoutes'   => $this->config['main_routes'][self::PORTAL_PREFIX],
                        'tags'         => $portal['tags'],
                        'portal'       => self::PORTAL_PREFIX,
                        'partialPath'  => self::PARTIAL_PATH,
                        'activePageId' => $data['function'],
                        'pathParts'    => $this->pathParts,
                        'breadMiddle'  => $query,
                        'breadEndName' => 'HRM_HEAD_TITLE_SEARCH'.$entity,
                    ],
                ];

                $response = new Response\TemplateResponse(
                    'qp/hrm/index/index',
                    [
                        'httpStatus' => $params['hsc'],
                        'nav'        => $portal['nav'],
                        'config'     => $config,
                    ]
                );
                return $response;
            }


        } catch (\Exception $e) {
            \FirePHP::getInstance(true)->warn($this->path);
            \FirePHP::getInstance(true)->warn($e->getMessage());
            $this->getErrorLogger()->logException($e);
            $this->layout->setVariable('headTitle', 'HRM_HEAD_TITLE_404');
            return new Response\TemplateResponse(
                'qp/common/error/404',
                [
                    'httpStatus' => 404,
                    'nav'        => $portal['nav'],
                    'config'     => [
                        'config' => [
                            'portal'       => self::PORTAL_PREFIX,
                            'tags'         => $portal['tags'],
                            'activePageId' => '404',
                            'pathParts'    => $this->pathParts,
                            'breadMiddle'  => '',
                            'breadEndName' => 'HRM_HEAD_TITLE_404',
                        ],
                    ],
                ]
            );
        }
    }
}
