<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Hrm\ViewLogic\Ajax;

use Zend\Http\Request;
use QP\Common\ViewLogic;
use QP\Common\ViewLogic\Response;
use Zend\View\Model\ViewModel;

/**
 * Class SearchLogic
 * @package QP\Hrm\ViewLogic\Ajax
 */
class SearchLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{
    const PORTAL_PREFIX = 'hrm';
    const PARTIAL_PATH  = 'qp/hrm/partials/';

    /**
     * @var string $entity
     */
    private $entity;

    /**
     * @var array $params
     */
    private $params;

    /**
     * @var Request $request
     */
    private $request;

    /**
     * @var array $post
     */
    private $post;

    /**
     * @var array $config
     */
    private $config;

    /**
     * CreateLogic constructor.
     *
     * @param string             $entity
     * @param array              $params
     * @param \Zend\Http\Request $request
     * @param array              $config
     */
    public function __construct($entity, $params, Request $request, $config)
    {
        $this->entity  = $entity;
        $this->params  = $params;
        $this->request = $request;
        $this->config  = $config;
        $this->post    = $this->request->getPost();
    }

    /**
     * Generate the view response.
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse()
    {
        $this->getTracker()->trackPageLoad($this->handleUser([]), $this->trackTime, ['event'=>'search_filter']);

        try {
            \FirePHP::getInstance(true)->info('ajax - search');
            $portal = $this->getPortalConfig(self::PORTAL_PREFIX);
            //\FirePHP::getInstance(true)->info($this->request->getPost());
            $postParams = $this->gluePostParams();
            //\FirePHP::getInstance(true)->info($postParams);
            //\FirePHP::getInstance(true)->info('?'.$postParams.'from='.$this->params['from'].'&size='.$this->params['size']);
            $items      = null;
            $searchFail = '';
            try {
                $items = $this->getItems('search/'.$this->entity, '?'.$postParams.'from='.$this->params['from'].'&size='.$this->params['size'], $portal['tags'], self::PORTAL_PREFIX);
            } catch (\Exception $e) {
                $searchFail = 'fail';
                $items = $this->getItems('search/'.$this->entity, '?from=0&size=15', $portal['tags'], self::PORTAL_PREFIX);
            }
            //\FirePHP::getInstance(true)->info($items);
            //\FirePHP::getInstance(true)->info($searchFail);
            if (is_null($items)) {
                $e = new \Exception('The requested '.$this->entity.' data could not be found!');
                $this->getErrorLogger()->logException($e);
                throw $e;
            }

            $view       = new ViewModel();
            $viewRender = $this->getViewRenderer();

            $main = '';
            foreach ($this->config['main_routes'][self::PORTAL_PREFIX] as $mainRoute => $mainRoutesConfig) {
                if ($mainRoutesConfig['entity'] === $this->entity) {
                    $main = $mainRoute;
                }
            }
            $config = [
                'data' => [
                    'items'        => $items['data'],
                    'entity'       => 'search/'.$this->entity,
                ],
                'config' => [
                    'mainRoutes'   => $this->config['main_routes'][self::PORTAL_PREFIX],
                    'tags'         => $portal['tags'],
                    'portal'       => self::PORTAL_PREFIX,
                    'partialPath'  => self::PARTIAL_PATH,
                    'activePageId' => $items['function'],
                ],
                'main' => $main
            ];

            $view->setVariables($config);
            $view->setTemplate('qp/hrm/partials/filter-results');
            $list = $viewRender->render($view);

            $view->setVariables(['data' => $items]);
            $view->setTemplate('qp/hrm/partials/pagination');
            $pagination = $viewRender->render($view);

            $alph = '';
            if (isset($items['aggs']['alph']) && !isset($this->post['alph'])) {
                $view->setVariables(['data' => $items]);
                $view->setTemplate('qp/hrm/partials/filter-alphabet');
                $alph = $viewRender->render($view);
            }

            foreach ($items['aggs'] as $key => $agg) {
                foreach ($agg['buckets'] as $bKey => $bucket) {
                    if(isset($bucket['key_as_string'])) {
                        $items['aggs'][$key]['buckets'][$bKey]['id'] = 'id_'.md5($bucket['key_as_string']);
                    } else {
                        $items['aggs'][$key]['buckets'][$bKey]['id'] = 'id_'.md5($bucket['key']);
                    }
                }
            }
            //\FirePHP::getInstance(true)->info($items['aggs']);

            return new Response\TemplateResponse(
                'qp/hrm/ajax/search',[
                    'json' => json_encode([
                        'fail'  => $searchFail,
                        'total' => $items['pagination']['total'],
                        'list'  => $list,
                        'aggs'  => $items['aggs'],
                        'alph'  => $alph,
                        'pagination' => $pagination,
                    ])
                ]
            );
        } catch (\Exception $e) {
            \FirePHP::getInstance(true)->info('ajax - search - fail');
            \FirePHP::getInstance(true)->info($e->getMessage());
            return new Response\TemplateResponse(
                'qp/hrm/ajax/search',[
                    'json' => json_encode([
                        'fail'  => 'fail',
                        'total' => 0,
                        'list'  => '',
                        'aggs'  => '',
                        'alph'  => '',
                        'pagination' => '',
                    ])
                ]
            );
        }
    }

    protected function gluePostParams()
    {
        $post = $this->post->toArray();
        if (isset($post['tags_sub'])) {
            if (isset($post['tags_p1'])) {
                $post['tags_p1'] = $post['tags_p1'] .','. $post['tags_sub'];
            } else {
                $post['tags_p1'] = $post['tags_sub'];
            }
            unset($post['tags_sub']);
        }

        $postParams = '';
        foreach($post as $key => $value) {
            $binder = '&';
            if ($postParams === '') {
                $binder = '';
            }
            $value = str_replace('.amp.', '&', $value);
            $postParams .= $binder.$key.'='.urlencode($value);
        }
        if ($postParams !== '') {
            $postParams = $postParams.'&';
        }
        return $postParams;
    }
}