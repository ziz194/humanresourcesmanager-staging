<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Hrm\Controller;

use QP\Common\Controller\AbstractController;

/**
 * Class AjaxController
 * @package QP\Hrm\Controller
 */
class AjaxController extends AbstractController
{

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function searchAction()
    {
        $this->layout('layout/empty');
        $entity    = $this->params()->fromRoute('entity');
        $viewLogic = $this->getViewLogicFactory()->build(
            'Hrm\ViewLogic\Ajax\SearchLogic',
            [
                $entity,
                $this->params()->fromQuery(),
                $this->getRequest(),
                $this->getServiceLocator()->get('Config'),
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }
}
