<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Hrm\Controller;

use QP\Common\Controller\AbstractController;

/**
 * Class IndexController
 * @package QP\Hrm\Controller
 */
class IndexController extends AbstractController
{

    /**
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->layout('layout/hrm');
        $path = $this->getRequest()->getUri()->getPath();
        $viewLogic = $this->getViewLogicFactory()->build(
            'Hrm\ViewLogic\Index\IndexLogic',
            [
                $path,
                $this->params()->fromQuery(),
                $this->getServiceLocator()->get('Config'),
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }

    /**
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function endpointAction()
    {
        $this->layout('layout/hrm');
        $main      = $this->params()->fromRoute('main');
        $tag1      = $this->params()->fromRoute('tag_1');
        $id        = $this->params()->fromRoute('id');
        $viewLogic = $this->getViewLogicFactory()->build(
            'Hrm\ViewLogic\Index\EndPointLogic',
            [
                $main,
                $tag1,
                $id,
                $this->getServiceLocator()->get('Config'),
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }
}

