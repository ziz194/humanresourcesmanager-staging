<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Hrm;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;

/**
 * Class Module
 * @package QP\Hrm
 */
class Module implements AutoloaderProviderInterface
{
    /**
     * Bootstrap the module.
     *
     * @param \Zend\Mvc\MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $config = $e->getApplication()->getServiceManager()->get('Config');
        $devEnv = (isset($config['application']['environment'])
            && $config['application']['environment'] == 'development') ? true : false;
        $e->getViewModel()->setVariable("isDevelopment", $devEnv);
//        $e->getViewModel()->setVariable('tracking', $config['tracking']);
//        \FirePHP::getInstance(true)->info($config['router']);
    }

    /**
     * Load the module config.
     *
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Load namespaces for module.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__)
                ],
            ],
        ];
    }
}
