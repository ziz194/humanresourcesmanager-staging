<form method="post" action="<?= $form_action ?>">
    <input type="hidden" name="product" value="<?= $id ?>">
    <input type="hidden" name="success_redirect" value="<?= $redirect ?>">
    <input type="hidden" name="amorderattr[formurl]" value="<?= $amorderattr_value ?>">
    <?= $alert ?>
    <?= $form_header ?>

    <?php if(isset($specials)):?>
        <div class="specials">
            <?= $specials ?>
        </div>
    <?php endif; ?>

    <?php if(isset($cart_options)):?>
        <div class="cart-options">
            <?= $cart_options ?>
        </div>
    <?php endif; ?>

    <?= $participants_list ?>

    <?php if(isset($order_attributes)):?>
        <div class="order-attributes">
            <?= $order_attributes ?>
        </div>
    <?php endif; ?>

    <?= $payment_options_and_partner ?>

    <div class="form-info">
        <small>
            <span>* <?= $logic->translate('required-fields') ?></span>
            <?php if ($show_vat): ?>
                <span>
                ** <?= $logic->translate('excl-vat-remark') ?>
                </span>
            <?php endif; ?>
        </small>
    </div>


    <div class="tante-emma-submit-button">
        <div class="download-fax-pdf">
            <a href="">
                <i class="material-icons">picture_as_pdf</i>
                <?= $logic->translate('download-fax-pdf') ?>
            </a>
        </div>
        <button class="button-info">
            <i class="material-icons">shopping_cart</i>
            <?= $logic->translate('shopping-cart-button-title') ?>
        </button>
        <button class="button-order">
            <?= $button_title ?>
        </button>
    </div>
</form>