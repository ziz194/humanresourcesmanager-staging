<?php if(isset($fields)): ?>
    <?php foreach($fields as $field): ?>
        <div class="participants-field col-<?= $field->groupName ?>">
            <?= $field->content ?>
        </div>
    <?php endforeach;?>
<?php endif; ?>
