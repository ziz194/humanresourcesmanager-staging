<?php if(isset($show_selectbar)): ?>
    <div class="tante-emma-content">
<?php if($show_selectbar): ?>
        <div class="optionen list-group">
<?php foreach($products as $i => $item ): ?>
            <div class="choose-date list-group-item list-group-item-<?= $i ?>" sku="<?= $item->sku ?>">
                <div class="img-circle"></div>
                <div class="list-group-item-heading clearfix">
                    <div class="tante-emma-date">
                        <?php //$list->getDatesTitle($item) ?>
                        <?= $list->getRenderedDates($item); ?>
                    </div>
                    <div class="tante-emma-title">
                        <?= $list->getTitle($item); ?>
                    </div>
                    <div class="tante-emma-price">
                        <?= $list->getDatesPriceInfo($item, ' <sup>**</sup>') ?>
                    </div>
                </div>
            </div>
<?php endforeach;?>
        </div>
<?php endif; ?>
        <div id="result"></div>
<?= $single_forms ?>
    </div>
<?php endif; ?>
