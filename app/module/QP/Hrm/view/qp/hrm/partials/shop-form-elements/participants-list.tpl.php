<?php if(isset($max_available)): ?>
<div class="participants-list">
    <!--h2><?= $logic->translate('participants-hl', 'single-form') ?></h2-->
    <div class="participants">
        <div class="participant">
            <h3><?= $logic->translate('nth-person-hl', 'single-form', array('number' => 1)) ?></h3>
            <?= $form->renderParticipant(0); ?>
        </div>
<?php for ($i = 1; $i < $max_available; $i++): ?>
        <div class="participant more_participants">
            <h3>
                <?= $logic->translate('nth-person-hl', 'single-form', array('number' => $i + 1)) ?>
                <div class="del_person btn tante-emma-button">
                    <i class="material-icons">clear</i>
                    <?= $logic->translate('delete') ?>
                </div>
            </h3>
            <?= $form->renderParticipant($i); ?>
        </div>
<?php endfor; ?>
    </div>
<?php if ($max_available > 1): ?>
        <div class="add_person btn tante-emma-button tante-emma-button">
            <i class="material-icons">add</i>
            <?= $logic->translate('add-person-text', 'single-form')?>
        </div>
<?php endif; ?>
    <div class="alert alert-info hide max_user" role="alert">
        <?= $logic->translate('max-bookable-person', 'single-form', array('number' => $max_available)) ?>
    </div>
</div>
<?php endif; ?>