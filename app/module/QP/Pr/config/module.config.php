<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c] 2016 Quadriga Media GmbH
 */
namespace QP\Pr;

return [
    //------------------------------------------------------------- Services ---
    'service_manager' => [
        'aliases' => [
            'translator' => 'MvcTranslator',
        ],
    ],
    //---------------------------------------------------------- Controllers ---
    'controllers' => [
        'invokables' => [
            'QP\Pr\Controller\Index'    => 'QP\Pr\Controller\IndexController',
        ],
    ],
    //---------------------------------------------------------------- Views ---
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'controller_map' => [
            'QP\User' => true,
        ],
    ],
    //--------------------------------------------------------------- Routes ---
    //------------------------------------------------------------- Doctrine ---
    //----------------------------------------------------------- Translator ---
    'translator' => [
        'locale' => 'de_DE',
        'translation_file_patterns' => [
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php'
            ],
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../../../../vendor/zendframework/zend-i18n-resources/languages/',
                'pattern'  => '%s/Zend_Captcha.php'
            ],
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../../../../vendor/zendframework/zend-i18n-resources/languages/',
                'pattern'  => '%s/Zend_Validate.php'
            ]
        ],
    ],
];
