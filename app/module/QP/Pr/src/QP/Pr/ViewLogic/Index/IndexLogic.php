<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Pr\ViewLogic\Index;

use QP\Common\ViewLogic;
use QP\Common\ViewLogic\Response;

/**
 * Class HomeLogic
 * @package QP\Pr\ViewLogic\IndexLogic
 */
class IndexLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{
    const PORTAL_PREFIX = 'pr';

    /**
     * @var string
     */
    private $path;

    /**
     * IndexLogic constructor.
     *
     * @param string $path
     */
    public function __construct($path)
    {
        $this->path = $path;
        if ($path === '/') {
            $this->path = $path.'home';
        }
    }
    /**
     * Generate the view response.
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse()
    {
        try {
            /** @var $nav \QP\Common\Entity\Nav */
            $nav  = $this->getApiClient()->getRepository('QP\Common\Entity\Nav')->readOne(self::PORTAL_PREFIX);
            //\FirePHP::getInstance(true)->info($nav);
            $node = $nav->getChildNodeByPath($this->path);
            //\FirePHP::getInstance(true)->info($node);
            if (is_null($node)) {
                throw new \Exception('The requested Page could not be found!');
            }
            $page = $this->getApiClient()->getRepository('QP\Common\Entity\Page')->getLatest(
                self::PORTAL_PREFIX,
                $node->getId()
            );

            //\FirePHP::getInstance(true)->info($nav);
            \FirePHP::getInstance(true)->info($node);
            \FirePHP::getInstance(true)->info($node->getId());
            \FirePHP::getInstance(true)->info($page);
            return new Response\TemplateResponse(
                'qp/hrm/index/index',
                [
                    'nav'    => $nav,
                    'page'   => $page,
                    'config' => [
                        'activePageId' => $node->getId(),
                        'portal'       => self::PORTAL_PREFIX,
                    ],
                ]
            );

        } catch (\Exception $e) {
            return new Response\TemplateResponse(
                'qp/common/error/404',
                []
            );
        }
    }
}
