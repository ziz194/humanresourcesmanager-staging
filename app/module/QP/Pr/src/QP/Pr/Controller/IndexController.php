<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Pr\Controller;

use QP\Common\Controller\AbstractController;

/**
 * Class IndexController
 * @package QP\Pr\Controller
 */
class IndexController extends AbstractController
{

    /**
     * Redirect to the login or central resource page
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->layout('layout/Pr');
        $path = $this->getRequest()->getUri()->getPath();
        $viewLogic = $this->getViewLogicFactory()->build(
            'Pr\ViewLogic\Index\IndexLogic',
            [
                $path
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }
}
