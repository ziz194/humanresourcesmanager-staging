<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */

return [
    //------------------------------------ text parts used all over the site ---

    // login
    'AUTH_FORM_HL'                                  => 'Anmeldung',
    'AUTH_LOGIN_FORM_USER'                          => 'Email',
    'AUTH_LOGIN_FORM_PASS'                          => 'Passwort',
    'AUTH_LOGIN_FORM_BUTTON'                        => 'Anmelden',
    'AUTH_LOGIN_FORM_FORGOT_MY_PASSWORD'            => 'Ich habe mein Passwort vergessen',

    'AUTH_FAIL_ACCOUNT_NOT_ACTIVE'                  => 'Ihr Benutzerkonto ist noch nicht aktiviert.',
    'AUTH_FAIL_WRONG_CREDENTIALS'                   => 'Falsche Login Daten.',
    'AUTH_FAIL_ACCOUNT_DOES_NOT_EXIST'              => 'Falsche Login Daten.', // in the system it might be a difference but for security reasons give the same error message

    'AUTH_ERROR_HEADLINE'                           => 'Authentifizierungsfehler',

    'AUTH_ERROR_REQUIRED_FIELD_MISSING'             => 'Pflichtfelder fehlen.',
    'AUTH_ERROR_NO_VALID_ROLE'                      => 'Ihnen ist keine Rolle zugewiesen. Bitte wenden Sie sich an den Systemadministrator.',
    'AUTH_ERROR_UNKNOWN'                            => 'Es ist ein unbekannter Fehler aufgetreten.',

    'AUTH_LOGOUT_HL'                                => 'Sie haben sich erfolgreich abgemeldet!',
    'AUTH_LOGOUT_LINK_TO_LOGIN'                     => 'Zurück zum Login',

    // password recover
    'FORGOT_FORM_HL'                                => 'Passwort vergessen',
    'FORGOT_INFO_TXT'                               => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    'FORGOT_FORM_EMAIL'                             => 'E-Mail-Adresse',
    'FORGOT_FORM_CAPTCHA'                           => 'Captcha bitte ausfüllen',
    'FORGOT_FORM_BUTTON'                            => 'Absenden',

    'FORGOT_PASSWORD_SUCCESS'                       => 'Eine Email mit einem Link zum Zurücksetzen Ihres Passwortes wurden Ihnen zugeschickt.',
    'FORGOT_PASSWORD_FAIL_NO_SUCH_USER'             => 'Sorry, es ist ein Fehler aufgetreten. Wir haben das mitbekommen und werden daran arbeiten. Bitte versuchen Sie es später erneut.',  // in the system it might be a difference but for security reasons give the same error message

    'FORGOT_PASSWORD_MAIL_SUBJECT'                  => 'HRManager: Sie haben Ihr Passwort vergessen aber das ist nicht schlimm.',
    'FORGOT_PASSWORD_MAIL_TEXT'                     => 'Hallo %%NAME%%,<br><br>'.
                                                       'es ist kein Problem das Sie Ihr Passwort vergessen haben. Das passiert jedem einmal und mit dem Lesen dieser E-Mail ist auch schon der zweite Schritt getan.<br>'.
                                                       'Bitte klicken Sie auf diesen Link und um ein neues Passwort für Ihren Account zu setzen:<br><br>'.
                                                       '%%LINK%%<br><br>'.
                                                       'Bitte folgen Sie dann den Anweisung auf der folgenden Seite.<br><br>'.
                                                       'Viele freundliche Grüße,<br>'.
                                                       '- Ihr HRM SERVICE TEAM -<br><br><br>',

    'RECOVER_FORM_HL'                               => 'Ein neues Passwort vergeben',
    'RECOVER_INFO_TXT'                              => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    'RECOVER_FORM_HL_PASSWORD'                      => 'Ihr neues Passwort und bitte wiederholen Sie es.',
    'RECOVER_PASSWORD_FORM_PASSWORD_NEW'            => 'Neues Passwort',
    'RECOVER_PASSWORD_FORM_PASSWORD_REPEAT'         => 'Passwort wiederholen',
    'RECOVER_PASSWORD_FORM_BUTTON'                  => 'Passwort setzen',
    'RECOVER_MESSAGE_INACTIVE'                      => 'Dieser Nutzer ist noch nicht aktiviert. Bitte schließen Sie erst Ihre registrierung ab.',
    'RECOVER_MESSAGE_NO_SUCH_USER'                  => 'Der angeforderte Nutzer existiert nicht.',
    'RECOVER_MESSAGE_EXPIRED'                       => 'Der Zeitraum zum Zurücksetzen Ihres Passwortes ist leider schon abgelaufen.',
    'RECOVER_PASSWORD_SUCCESS'                      => 'Ihr Passwort wurde erfolgreich gespeichert. Sie können sich nun anmelden.',
    'RECOVER_PASSWORD_FORM_VALIDATOR_PASS_NOT_SAME' => 'Die Passwörter stimmen nicht überein',
    'RECOVER_PASSWORD_FORM_VALIDATOR_PASS_MISSING'  => 'Sie müssen Ihr Passwort zwei mal angeben',

    // profile
    'PROFILE_HL'                                    => 'Nutzerprofil: ',
    'PROFILE_HL_BASIC_DATA'                         => 'Stammdaten',
    'PROFILE_FIRSTNAME'                             => 'Vorname',
    'PROFILE_LASTNAME'                              => 'Nachname',
    'PROFILE_EMAIL'                                 => 'E-Mail-Adresse',
    'PROFILE_STATE'                                 => 'Status',
    'PROFILE_STATE_ACTIVE'                          => 'aktiv',
    'PROFILE_STATE_INACTIVE'                        => 'inaktiv',
    'PROFILE_CREATED'                               => 'Erstellt',
    'PROFILE_LAST_MODIFIED'                         => 'Zuletzt bearbeitet',
    'PROFILE_EDIT_BASIC_DATA'                       => 'Stammdaten bearbeiten',

    'PROFILE_HL_PICTURE'                            => 'Profilbild',
    'PROFILE_RESET_PICTURE'                         => 'Neues Profilbild hochladen',

    'PROFILE_HL_SECURE'                             => 'Sicherheit',

    'RESET_PASSWORD_FORM_PASSWORD_OLD'              => 'Altes Passwort',
    'RESET_PASSWORD_FORM_PASSWORD_NEW'              => 'Neues Passwort',
    'RESET_PASSWORD_FORM_PASSWORD_REPEAT'           => 'Passwort widerholen',
    'PROFILE_RESET_PASSWORD'                        => 'Passwort ändern',
    'PROFILE_RESET_PASSWORD_INCORRECT_OLD'          => 'Falsches Passwort',
    'RESET_PASSWORD_SUCCESS'                        => 'Ihr Passwort wurde erfolgreich geändert.',


    // user module logging
    'AUTH_LOGGER_USER_LOGIN'                        => 'Nutzer hat sich angemeldet',
    'AUTH_LOGGER_USER_LOGOUT'                       => 'Nutzer hat sich abgemeldet',
    'AUTH_LOGGER_FORGOT_PASS'                       => 'Nutzer hat email zum Passwort zurücksetzen angefordert',
    'AUTH_LOGGER_USER_RECOVER_PASS'                 => 'Nutzer hat sein Passwort zurückgesetzt',
    'PROFILE_LOGGER_CHANGE_PASS'                    => 'Nutzer hat sein Passwort geändert',
    'PROFILE_LOGGER_EDIT'                           => 'Nutzer hat seine Stammdaten geändert',
];
