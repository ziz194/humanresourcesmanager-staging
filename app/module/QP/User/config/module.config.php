<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c] 2016 Quadriga Media GmbH
 */
namespace QP\User;

return [
    //------------------------------------------------------------- Services ---
    'service_manager' => [
        'aliases' => [
            'translator' => 'MvcTranslator',
        ],
        'factories' => [
            'AclService'  => 'QP\User\Service\AclFactory',
            'UserService' => 'QP\User\Service\UserServiceFactory',
        ],
        'invokables' => [
            'Zend\Authentication\AuthenticationService' => 'Zend\Authentication\AuthenticationService',
        ]
    ],
    //---------------------------------------------------------- Controllers ---
    'controllers' => [
        'invokables' => [
            'QP\User\Controller\Auth'    => 'QP\User\Controller\AuthController',
            'QP\User\Controller\Profile' => 'QP\User\Controller\ProfileController',
        ],
    ],
    //---------------------------------------------------------------- Views ---
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'controller_map' => [
            'QP\User' => true,
        ],
    ],
    //--------------------------------------------------------------- Routes ---
    'router' => [
        'routes' => [
            'user-login' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/user/login',
                    'defaults' => [
                        'controller' => 'QP\User\Controller\Auth',
                        'action'     => 'login',
                    ],
                ],
            ],
            'user-logout' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/user/logout',
                    'defaults' => [
                        'controller' => 'QP\User\Controller\Auth',
                        'action'     => 'logout',
                    ],
                ],
            ],
            'user-forgot' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/user/forgot',
                    'defaults' => [
                        'controller' => 'QP\User\Controller\Auth',
                        'action'     => 'forgot',
                    ],
                ],
            ],
            'user-recover' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/user/recover/:recoverToken',
                    'defaults' => [
                        'controller' => 'QP\User\Controller\Auth',
                        'action'     => 'recover',
                    ],
                ],
            ],
            'profile' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/profile',
                    'defaults' => [
                        'controller' => 'QP\User\Controller\Profile',
                        'action'     => 'profile',
                    ],
                ],
            ],
            'profile-edit' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/profile/edit',
                    'defaults' => [
                        'controller' => 'QP\User\Controller\Profile',
                        'action'     => 'edit',
                    ],
                ],
            ],
            'profile-password' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/profile/password',
                    'defaults' => [
                        'controller' => 'QP\User\Controller\Profile',
                        'action'     => 'password',
                    ],
                ],
            ],
        ],
    ],
    //------------------------------------------------------------- Doctrine ---
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/' . str_replace('\\', '/', __NAMESPACE__) . '/Entity'
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ],
    //----------------------------------------------------------- Translator ---
    'translator' => [
        'locale' => 'de_DE',
        'translation_file_patterns' => [
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php'
            ],
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../../../../vendor/zendframework/zend-i18n-resources/languages/',
                'pattern'  => '%s/Zend_Captcha.php'
            ],
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../../../../vendor/zendframework/zend-i18n-resources/languages/',
                'pattern'  => '%s/Zend_Validate.php'
            ]
        ],
    ],
];
