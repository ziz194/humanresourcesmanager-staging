<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Authentication;

use QP\User\Entity\User;
use Zend\Authentication\Result;

class Login implements \Zend\Authentication\Adapter\AdapterInterface
{
    /**
     * @var User
     */
    private $_me;

    /**
     * @var string
     */
    private $_email;

    /**
     * @var string
     */
    private $_password;

    /**
     * @var string
     */
    private $_staticSalt;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $_entityManager;

    /**
     * @param   string $email
     * @param   string $password
     * @param   string $staticSalt
     * @param   \Doctrine\ORM\EntityManager $entityManager
     */
    public function __construct($email, $password, $staticSalt, $entityManager)
    {
        $this->_email         = $email;
        $this->_password      = $password;
        $this->_staticSalt    = $staticSalt;
        $this->_entityManager = $entityManager;
    }

    /**
     * Authenticate user.
     *
     * @return \Zend\Authentication\Result
     */
    public function authenticate()
    {
        // get the user by the given email
        $this->_me = $this->_entityManager->getRepository('QP\User\Entity\User')->readOneByEmail($this->_email);

        if (!is_null($this->_me)) {
            $meArray = $this->_me->getArrayCopy();
            // if the is not active yet
            if ($meArray['state'] !== User::STATE_ACTIVE) {
                return new Result(Result::FAILURE_IDENTITY_AMBIGUOUS, null, array(
                    'AUTH_FAIL_ACCOUNT_NOT_ACTIVE'
                ));
            }
            // if login is successful
            if (self::validatePassword($this->_password, $this->_staticSalt, $this->_me)) {
                return new Result(Result::SUCCESS, $meArray);
            }

            // if the credentials are bogus
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, array(
                'AUTH_FAIL_WRONG_CREDENTIALS'
            ));
        }

        // if the user does not exist
        return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null, array(
            'AUTH_FAIL_ACCOUNT_DOES_NOT_EXIST'
        ));
    }

    /**
     * @param   string $givenPass
     * @param   string $staticSalt
     * @param   User $user
     *
     * @return  bool
     */
    static function validatePassword($givenPass, $staticSalt, $user)
    {
        $bcrypt     = new \Zend\Crypt\Password\Bcrypt();
        $password   = $user->getPassword();
        $hashedPass = \Zend\Crypt\Hash::compute('sha256', $staticSalt . $givenPass . $user->getSalt());

        if ($bcrypt->verify($hashedPass, $password)) {
            return true;
        }

        return false;
    }
}