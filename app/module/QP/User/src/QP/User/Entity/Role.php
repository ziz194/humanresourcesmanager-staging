<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * A doctrine model representing a user role.
 *
 * @author Andy Killat
 *
 * @ORM\Table(name="roles")
 * @ORM\Entity(repositoryClass="QP\User\EntityRepository\RoleRepository")
 */
class Role
{
    const ROLE_DEFAULT      = 'Gast';
//    const ROLE_EDITOR       = 'Redakteur';
//    const ROLE_EDITOR_HRM   = 'Redakteur HRM';
//    const ROLE_EDITOR_PR    = 'Redakteur PR';
//    const ROLE_SUPER_EDITOR = 'Super_Redakteur';
//    const ROLE_ADMIN        = 'Admin';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="QP\User\Entity\User", mappedBy="roles")
     */
    private $users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * Add a user to the role
     *
     * @param \QP\User\Entity\User $user
     */
    public function addUser(User $user)
    {
        $this->users->add($user);
    }

    /**
     * Remove a user from the role
     *
     * @param \QP\User\Entity\User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get the roles name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
