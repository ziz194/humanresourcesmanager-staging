<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Entity;

use QP\Common\Entity\Base;
use Doctrine\ORM\Mapping as ORM;

/**
 * A doctrine model representing a event submit data set.
 *
 * @author Andy Killat
 *
 * @ORM\Table(name="event_submit")
 * @ORM\Entity(repositoryClass="QP\User\EntityRepository\EventSubmitRepository")
 * @ORM\HasLifecycleCallbacks
 */
class EventSubmit extends Base
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="QP\User\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="request", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_type", type="string", length=255, nullable=true)
     */
    private $additionalType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="from_date", type="datetime", nullable=false)
     */
    private $fromDate;

    /**
     * @var string
     *
     * @ORM\Column(name="from_time", type="string", length=255, nullable=true)
     */
    private $fromTime;

    /**
     * @var bool
     *
     * @ORM\Column(name="from_full_day", type="boolean", nullable=true)
     */
    private $fromFullDay;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="until_date", type="datetime", nullable=false)
     */
    private $untilDate;

    /**
     * @var string
     *
     * @ORM\Column(name="until_time", type="string", length=255, nullable=true)
     */
    private $untilTime;

    /**
     * @var bool
     *
     * @ORM\Column(name="until_full_day", type="boolean", nullable=true)
     */
    private $untilFullDay;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="guest_register", type="text", nullable=true)
     */
    private $guestRegister;

    /**
     * @var string
     *
     * @ORM\Column(name="guest_costs", type="text", nullable=true)
     */
    private $guestCosts;

    /**
     * @var string
     *
     * @ORM\Column(name="guest_member", type="text", nullable=true)
     */
    private $guestMember;

    /**
     * @var string
     *
     * @ORM\Column(name="host", type="string", length=255, nullable=true)
     */
    private $host;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="plz", type="string", length=255, nullable=true)
     */
    private $plz;

    /**
     * @var string
     *
     * @ORM\Column(name="partner", type="string", length=255, nullable=true)
     */
    private $partner;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255, nullable=true)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false)
     */
    protected $createDate;

    /**
     * Get a class representation in array format.
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return [
            'id'             => $this->id,
            'userId'         => $this->userId,
            'title'          => $this->title,
            'additionalType' => $this->additionalType,
            'fromDate'       => $this->fromDate,
            'fromTime'       => $this->fromTime,
            'fromFullDay'    => $this->fromFullDay,
            'untilDate'      => $this->untilDate,
            'untilTime'      => $this->untilTime,
            'untilFullDay'   => $this->untilFullDay,
            'message'        => $this->message,
            'guestRegister'  => $this->guestRegister,
            'guestCosts'     => $this->guestCosts,
            'guestMember'    => $this->guestMember,
            'host'           => $this->host,
            'location'       => $this->location,
            'street'         => $this->street,
            'plz'            => $this->plz,
            'partner'        => $this->partner,
            'tel'            => $this->tel,
            'email'          => $this->email,
            'createDate'     => $this->createDate,
        ];
    }

    /**
     * Fill the user with array data.
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->id = array_key_exists('id', $data)
            ? $data['id'] : $this->id;
        $this->userId = array_key_exists('userId', $data)
            ? $data['userId'] : $this->userId;
        $this->title = array_key_exists('title', $data)
            ? $data['title'] : $this->title;
        $this->additionalType = array_key_exists('additionalType', $data)
            ? trim($data['additionalType']) : $this->additionalType;
        $this->fromDate = array_key_exists('fromDate', $data)
            ? $data['fromDate'] : $this->fromDate;
        $this->fromTime = array_key_exists('fromTime', $data)
            ? $data['fromTime'] : $this->fromTime;
        $this->fromFullDay = array_key_exists('fromFullDay', $data)
            ? $data['fromFullDay'] : $this->fromFullDay;
        $this->untilDate = array_key_exists('untilDate', $data)
            ? $data['untilDate'] : $this->untilDate;
        $this->untilTime = array_key_exists('untilTime', $data)
            ? $data['untilTime'] : $this->untilTime;
        $this->untilFullDay = array_key_exists('untilFullDay', $data)
            ? $data['untilFullDay'] : $this->untilFullDay;
        $this->message = array_key_exists('message', $data)
            ? $data['message'] : $this->message;
        $this->guestRegister = array_key_exists('guestRegister', $data)
            ? $data['guestRegister'] : $this->guestRegister;
        $this->guestCosts = array_key_exists('guestCosts', $data)
            ? $data['guestCosts'] : $this->guestCosts;
        $this->guestMember = array_key_exists('guestMember', $data)
            ? $data['guestMember'] : $this->guestMember;
        $this->host = array_key_exists('host', $data)
            ? $data['host'] : $this->host;
        $this->location = array_key_exists('location', $data)
            ? $data['location'] : $this->location;
        $this->street = array_key_exists('street', $data)
            ? $data['street'] : $this->street;
        $this->plz = array_key_exists('plz', $data)
            ? $data['plz'] : $this->plz;
        $this->partner = array_key_exists('partner', $data)
            ? $data['partner'] : $this->partner;
        $this->tel = array_key_exists('tel', $data)
            ? $data['tel'] : $this->tel;
        $this->email = array_key_exists('email', $data)
            ? $data['email'] : $this->email;
        $this->createDate = array_key_exists('createDate', $data)
            ? $data['createDate'] : $this->createDate;
    }
}
