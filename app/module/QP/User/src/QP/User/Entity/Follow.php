<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Entity;

use QP\Common\Entity\Base;
use Doctrine\ORM\Mapping as ORM;

/**
 * A doctrine model representing a follow data set.
 *
 * @author Andy Killat
 *
 * @ORM\Table(name="follow")
 * @ORM\Entity(repositoryClass="QP\User\EntityRepository\FollowRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Follow extends Base
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="QP\User\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=255, nullable=true)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="platform", type="string", length=255, nullable=true)
     */
    private $platform;

    /**
     * @var string
     *
     * @ORM\Column(name="user_agent", type="string", length=255, nullable=true)
     */
    private $userAgent;

    /**
     * @var string
     *
     * @ORM\Column(name="screen_width", type="string", length=255, nullable=true)
     */
    private $screenWidth;

    /**
     * @var string
     *
     * @ORM\Column(name="screen_height", type="string", length=255, nullable=true)
     */
    private $screen_height;

    /**
     * @var string
     *
     * @ORM\Column(name="screen_available_width", type="string", length=255, nullable=true)
     */
    private $screen_available_width;

    /**
     * @var string
     *
     * @ORM\Column(name="screen_available_height", type="string", length=255, nullable=true)
     */
    private $screen_available_height;

    /**
     * @var string
     *
     * @ORM\Column(name="color_depth", type="string", length=255, nullable=true)
     */
    private $colorDepth;

    /**
     * @var string
     *
     * @ORM\Column(name="dpi_x", type="string", length=255, nullable=true)
     */
    private $dpiX;

    /**
     * @var string
     *
     * @ORM\Column(name="dpi_y", type="string", length=255, nullable=true)
     */
    private $dpiY;

    /**
     * @var string
     *
     * @ORM\Column(name="device_pixel_ratio", type="string", length=255, nullable=true)
     */
    private $devicePixelRatio;

    /**
     * @var string
     *
     * @ORM\Column(name="timezone_offset", type="string", length=255, nullable=true)
     */
    private $timezoneOffset;

    /**
     * @var string
     *
     * @ORM\Column(name="java_enabled", type="string", length=255, nullable=true)
     */
    private $javaEnabled;

    /**
     * @var string
     *
     * @ORM\Column(name="acrobat", type="string", length=255, nullable=true)
     */
    private $acrobat;

    /**
     * @var string
     *
     * @ORM\Column(name="mime_types", type="text", nullable=true)
     */
    private $mimeTypes;

    /**
     * @var string
     *
     * @ORM\Column(name="browser_plugins", type="text", nullable=true)
     */
    private $browserPlugins;

    /**
     * @var string
     *
     * @ORM\Column(name="system_colors", type="text", nullable=true)
     */
    private $systemColors;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address_v4", type="string", length=255, nullable=true)
     */
    private $ipAddressV4;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address_v6", type="string", length=255, nullable=true)
     */
    private $ipAddressV6;

    /**
     * @var string
     *
     * @ORM\Column(name="http_referrer", type="string", length=255, nullable=true)
     */
    private $httpReferrer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false)
     */
    protected $createDate;

    /**
     * Get a class representation in array format.
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return [
            'id'                      => $this->id,
            'userId'                  => $this->userId,
            'language'                => $this->language,
            'platform'                => $this->platform,
            'userAgent'               => $this->userAgent,
            'screenWidth'             => $this->screenWidth,
            'screen_height'           => $this->screen_height,
            'screen_available_width'  => $this->screen_available_width,
            'screen_available_height' => $this->screen_available_height,
            'colorDepth'              => $this->colorDepth,
            'dpiX'                    => $this->dpiX,
            'dpiY'                    => $this->dpiY,
            'devicePixelRatio'        => $this->devicePixelRatio,
            'timezoneOffset'          => $this->timezoneOffset,
            'javaEnabled'             => $this->javaEnabled,
            'acrobat'                 => $this->acrobat,
            'mimeTypes'               => $this->mimeTypes,
            'browserPlugins'          => $this->browserPlugins,
            'systemColors'            => $this->systemColors,
            'ipAddressV4'             => $this->ipAddressV4,
            'ipAddressV6'             => $this->ipAddressV6,
            'httpReferrer'            => $this->httpReferrer,
            'createDate'              => $this->createDate,
        ];
    }

    /**
     * Fill the user with array data.
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->id = array_key_exists('id', $data)
            ? $data['id'] : $this->id;
        $this->userId = array_key_exists('userId', $data)
            ? $data['userId'] : $this->userId;
        $this->language = array_key_exists('language', $data)
            ? $data['language'] : $this->language;
        $this->platform = array_key_exists('platform', $data)
            ? $data['platform'] : $this->platform;
        $this->userAgent = array_key_exists('userAgent', $data)
            ? $data['userAgent'] : $this->userAgent;
        $this->screenWidth = array_key_exists('screenWidth', $data)
            ? $data['screenWidth'] : $this->screenWidth;
        $this->screen_height = array_key_exists('screen_height', $data)
            ? $data['screen_height'] : $this->screen_height;
        $this->screen_available_width = array_key_exists('screen_available_width', $data)
            ? $data['screen_available_width'] : $this->screen_available_width;
        $this->screen_available_height = array_key_exists('screen_available_height', $data)
            ? $data['screen_available_height'] : $this->screen_available_height;
        $this->colorDepth = array_key_exists('colorDepth', $data)
            ? $data['colorDepth'] : $this->colorDepth;
        $this->dpiX = array_key_exists('dpiX', $data)
            ? $data['dpiX'] : $this->dpiX;
        $this->dpiY = array_key_exists('dpiY', $data)
            ? $data['dpiY'] : $this->dpiY;
        $this->devicePixelRatio = array_key_exists('devicePixelRatio', $data)
            ? $data['devicePixelRatio'] : $this->devicePixelRatio;
        $this->timezoneOffset = array_key_exists('timezoneOffset', $data)
            ? $data['timezoneOffset'] : $this->timezoneOffset;
        $this->javaEnabled = array_key_exists('javaEnabled', $data)
            ? $data['javaEnabled'] : $this->javaEnabled;
        $this->acrobat = array_key_exists('acrobat', $data)
            ? $data['acrobat'] : $this->acrobat;
        $this->mimeTypes = array_key_exists('mimeTypes', $data)
            ? $data['mimeTypes'] : $this->mimeTypes;
        $this->browserPlugins = array_key_exists('browserPlugins', $data)
            ? $data['browserPlugins'] : $this->browserPlugins;
        $this->systemColors = array_key_exists('systemColors', $data)
            ? $data['systemColors'] : $this->systemColors;
        $this->ipAddressV4 = array_key_exists('ipAddressV4', $data)
            ? $data['ipAddressV4'] : $this->ipAddressV4;
        $this->ipAddressV6 = array_key_exists('ipAddressV6', $data)
            ? $data['ipAddressV6'] : $this->ipAddressV6;
        $this->httpReferrer = array_key_exists('httpReferrer', $data)
            ? $data['httpReferrer'] : $this->httpReferrer;
        $this->createDate = array_key_exists('createDate', $data)
            ? $data['createDate'] : $this->createDate;
    }
}
