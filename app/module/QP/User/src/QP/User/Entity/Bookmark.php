<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Entity;

use QP\Common\Entity\Base;
use Doctrine\ORM\Mapping as ORM;

/**
 * A doctrine model representing a newsletter data set.
 *
 * @author Andy Killat
 *
 * @ORM\Table(name="bookmark")
 * @ORM\Entity(repositoryClass="QP\User\EntityRepository\BookmarkRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Bookmark extends Base
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="QP\User\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="portal_id", type="string", length=255, nullable=true)
     */
    private $portalId;

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=255, nullable=true)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="es_id", type="string", length=255, nullable=true)
     */
    private $esId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false)
     */
    protected $createDate;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPortalId()
    {
        return $this->portalId;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return string
     */
    public function getEsId()
    {
        return $this->esId;
    }

    /**
     * Get a class representation in array format.
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return [
            'id'             => $this->id,
            'userId'         => $this->userId,
            'portalId'       => $this->portalId,
            'entity'         => $this->entity,
            'esId'           => $this->esId,
            'createDate'     => $this->createDate,
        ];
    }

    /**
     * Fill the user with array data.
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->id = array_key_exists('id', $data)
            ? $data['id'] : $this->id;
        $this->userId = array_key_exists('userId', $data)
            ? $data['userId'] : $this->userId;
        $this->portalId = array_key_exists('portalId', $data)
            ? $data['portalId'] : $this->portalId;
        $this->entity = array_key_exists('entity', $data)
            ? $data['entity'] : $this->entity;
        $this->esId = array_key_exists('esId', $data)
            ? $data['esId'] : $this->esId;
        $this->createDate = array_key_exists('createDate', $data)
            ? $data['createDate'] : $this->createDate;
    }
}
