<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Entity;

use QP\Common\Entity\Base;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * A doctrine model representing a user.
 *
 * @author Andy Killat
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="QP\User\EntityRepository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User extends Base
{
    const STATE_ACTIVE   = 1;
    const STATE_INACTIVE = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="id_hash", type="string", length=255, nullable=true)
     */
    private $idHash;

    /**
     * @var string
     *
     * @ORM\Column(name="salutation", type="string", length=128, nullable=true)
     */
    private $salutation;
    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=128, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=128, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=128, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=128, nullable=true)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=128, nullable=true)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=128, nullable=true)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100, nullable=true)
     */
    protected $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=100, nullable=true)
     */
    protected $salt;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer", nullable=true)
     */
    protected $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="cookie_only", type="integer", nullable=true)
     */
    protected $cookieOnly;

    /**
     * @var integer
     *
     * @ORM\Column(name="session_only", type="integer", nullable=true)
     */
    protected $sessionOnly;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_modified", type="datetime", nullable=true)
     */
    protected $lastModified;

    /**
     * @ORM\ManyToMany(targetEntity="QP\User\Entity\Role")
     * @ORM\JoinTable(name="user_has_role",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $roles;
    private $aclRole;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * Get the id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIdHash()
    {
        return $this->idHash;
    }

    /**
     * @param string $idHash
     */
    public function setIdHash($idHash)
    {
        $this->idHash = $idHash;
    }

    /**
     */
    public function createIdHash()
    {
        $this->idHash = sha1($this->id);
    }

    /**
     * Get the user roles
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set the user roles
     *
     * @param \Doctrine\Common\Collections\ArrayCollection $roles
     */
    public function setRoles(ArrayCollection $roles)
    {
        $this->roles = $roles;
    }

    /**
     * Add a single role to the user roles
     *
     * @param \QP\User\Entity\Role $role
     */
    public function addRole(Role $role)
    {
        $this->roles->add($role);
        $role->addUser($this);
    }

    /**
     * Remove a single role to the user roles
     *
     * @param \QP\User\Entity\Role $role
     */
    public function removeRole(Role $role)
    {
        $this->roles->removeElement($role);
        $role->removeUser($this);
    }

    /**
     * Set role ids for user.
     *
     * @param array $roleIds
     * @param \Doctrine\Common\Collections\ArrayCollection $allRoles
     */
    public function setRoleIds(array $roleIds, $allRoles)
    {
        $removedRoles = array();
        // 1) search all roles that already exist by their id
        if (!empty($this->roles)) {
            $this->roles->filter(function ($role) use (
                &$roleIds,
                &$removedRoles
            ) {
                $removedRoles[] = $role;
                return false;
            });
        }
        // 2) add new roles for those that don't exist yet
        foreach ($allRoles as $role) {
            $this->addRole($role);
        }
        // 3) remove roles that belonged to the element before, but not anymore
        foreach ($removedRoles as $role) {
            $this->removeRole($role);
        }

    }

    /**
     * Get the ACL role of the user.
     *
     * @return @param \Zend\Permissions\Acl\Role\GenericRole The ACL role of the user
     */
    public function getAclRole()
    {
        return $this->aclRole;
    }

    /**
     * Set the ACL role of the user.
     *
     * @param \Zend\Permissions\Acl\Role\GenericRole The ACL role of the user
     */
    public function setAclRole($aclRole)
    {
        $this->aclRole = $aclRole;
    }

    /**
     * Get a class representation in array format.
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $roleNames = [];
        foreach ($this->roles as $role) {
            $roleNames[] = $role->getName();
        }
        return [
            'id'           => $this->id,
            'idHash'       => $this->idHash,
            'roleNames'    => $roleNames,
            'concatRole'   => implode('',$roleNames),
            'salutation'   => $this->salutation,
            'firstname'    => $this->firstname,
            'lastname'     => $this->lastname,
            'fullname'     => $this->firstname . ' ' . $this->lastname,
            'shortName'    => substr($this->firstname,0 ,1) . $this->lastname,
            'email'        => $this->email,
            'tel'          => $this->tel,
            'company'      => $this->company,
            'position'     => $this->position,
            'state'        => $this->state,
            'cookieOnly'   => $this->cookieOnly,
            'sessionOnly'  => $this->sessionOnly,
            'createDate'   => $this->createDate,
            'lastModified' => $this->lastModified,
        ];
    }

    /**
     * Fill the user with array data.
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->id = array_key_exists('id', $data)
            ? $data['id'] : $this->id;
        $this->aclRole = array_key_exists('acl_role', $data)
            ? $data['acl_role'] : $this->aclRole;
        $this->salutation = array_key_exists('salutation', $data)
            ? trim($data['salutation']) : $this->salutation;
        $this->firstname = array_key_exists('firstname', $data)
            ? $data['firstname'] : $this->firstname;
        $this->lastname = array_key_exists('lastname', $data)
            ? $data['lastname'] : $this->lastname;
        $this->email = array_key_exists('email', $data)
            ? $data['email'] : $this->email;
        $this->tel = array_key_exists('tel', $data)
            ? $data['tel'] : $this->tel;
        $this->company = array_key_exists('company', $data)
            ? $data['company'] : $this->company;
        $this->position = array_key_exists('position', $data)
            ? $data['position'] : $this->position;
        $this->password = array_key_exists('password', $data)
            ? $data['password'] : $this->password;
        $this->state = array_key_exists('state', $data)
            ? $data['state'] : $this->state;
        $this->cookieOnly = array_key_exists('cookieOnly', $data)
            ? $data['cookieOnly'] : $this->cookieOnly;
        $this->sessionOnly = array_key_exists('sessionOnly', $data)
            ? $data['sessionOnly'] : $this->sessionOnly;
        $this->createDate = array_key_exists('createDate', $data)
            ? $data['createDate'] : $this->createDate;
        $this->lastModified = new \DateTime();
    }

    /**
     * @param   string  $staticSalt
     * @param   string  $password
     * @param   string  $dynamicSalt
     *
     * @return  string
     */
    static function encryptPassword($staticSalt, $password, $dynamicSalt)
    {
        $hashedPass = \Zend\Crypt\Hash::compute('sha256', $staticSalt . $password . $dynamicSalt);
        $bcrypt = new \Zend\Crypt\Password\Bcrypt();
        $bcrypt->setCost(10);

        return $bcrypt->create($hashedPass);
    }

    /**
     * @return string
     */
    static function generateDynamicSalt()
    {
        $dynamicSalt = '';
        for( $i = 0; $i < 50; $i++ )
        {
            $dynamicSalt .= chr( rand( 42, 126 ) );
        }

        return $dynamicSalt;
    }
}
