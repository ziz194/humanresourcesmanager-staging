<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Entity;

use QP\Common\Entity\Base;
use Doctrine\ORM\Mapping as ORM;

/**
 * A doctrine model representing a newsletter data set.
 *
 * @author Andy Killat
 *
 * @ORM\Table(name="newsletter")
 * @ORM\Entity(repositoryClass="QP\User\EntityRepository\NewsletterRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Newsletter extends Base
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="QP\User\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="opi_id", type="string", length=255, nullable=true)
     */
    private $opiId;

    /**
     * @var string
     *
     * @ORM\Column(name="newsletter_name", type="string", length=255, nullable=true)
     */
    private $newsletterName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false)
     */
    protected $createDate;

    /**
     * Get a class representation in array format.
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return [
            'id'             => $this->id,
            'userId'         => $this->userId,
            'opiId'          => $this->opiId,
            'newsletterName' => $this->newsletterName,
            'createDate'     => $this->createDate,
        ];
    }

    /**
     * Fill the user with array data.
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->id = array_key_exists('id', $data)
            ? $data['id'] : $this->id;
        $this->userId = array_key_exists('userId', $data)
            ? $data['userId'] : $this->userId;
        $this->opiId = array_key_exists('opiId', $data)
            ? $data['opiId'] : $this->opiId;
        $this->newsletterName = array_key_exists('newsletterName', $data)
            ? $data['newsletterName'] : $this->newsletterName;
        $this->createDate = array_key_exists('createDate', $data)
            ? $data['createDate'] : $this->createDate;
    }
}
