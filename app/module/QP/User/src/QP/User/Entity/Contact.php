<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Entity;

use QP\Common\Entity\Base;
use Doctrine\ORM\Mapping as ORM;

/**
 * A doctrine model representing a contact data set.
 *
 * @author Andy Killat
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity(repositoryClass="QP\User\EntityRepository\ContactRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Contact extends Base
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="QP\User\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="request", type="string", length=255, nullable=true)
     */
    private $request;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false)
     */
    protected $createDate;

    /**
     * Get a class representation in array format.
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return [
            'id'         => $this->id,
            'userId'     => $this->userId,
            'request'    => trim($this->request),
            'message'    => trim($this->message),
            'createDate' => $this->createDate,
        ];
    }

    /**
     * Fill the user with array data.
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->id = array_key_exists('id', $data)
            ? $data['id'] : $this->id;
        $this->userId = array_key_exists('userId', $data)
            ? $data['userId'] : $this->userId;
        $this->request = array_key_exists('request', $data)
            ? $data['request'] : $this->request;
        $this->message = array_key_exists('message', $data)
            ? $data['message'] : $this->message;
        $this->createDate = array_key_exists('createDate', $data)
            ? $data['createDate'] : $this->createDate;
    }
}
