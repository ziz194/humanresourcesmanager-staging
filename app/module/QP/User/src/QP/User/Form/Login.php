<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Form;

use Zend\Form\Form;

class Login extends Form
{
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct('login');
        $this->setAttribute('method', 'post');

        $this->add([
            'name'    => 'email',
            'type'    => 'Zend\Form\Element\Text',
            'options' => [
                'label' => 'AUTH_LOGIN_FORM_USER',
            ],
        ]);

        $this->add([
            'name'    => 'password',
            'type'    => 'Zend\Form\Element\Password',
            'options' => [
                'label' => 'AUTH_LOGIN_FORM_PASS',
            ],
        ]);

        $this->add([
            'name'       => 'submit',
            'attributes' => [
                'type'  => 'submit',
                'value' => 'AUTH_LOGIN_FORM_BUTTON',
                'id'    => 'submitbutton',
                'class' => 'btn btn--primary',
            ],
        ]);
    }
}