<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c] 2016 Quadriga Media GmbH
 */
namespace QP\User\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class ResetPassword extends Form
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct('reset-password');
        $this->setAttributes([
            'method'      => 'post',
            'data-module' => 'ModalForm'
        ]);
        $inputFilter = new InputFilter();

        //----------------------------------------------------- new password ---
        $this->add([
            'name'    => 'password_old',
            'options' => [
                'label' => 'RESET_PASSWORD_FORM_PASSWORD_OLD',
            ],
            'attributes' => [
                'type' => 'password',
            ],
        ]);
        $inputFilter->add([
            'name'       => 'password_old',
            'required'   => true,
            'allowEmpty' => false,
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 55,
                    ],
                ],
            ],
        ]);

        //----------------------------------------------------- new password ---
        $this->add([
            'name'    => 'password_new',
            'options' => [
                'label' => 'RESET_PASSWORD_FORM_PASSWORD_NEW',
            ],
            'attributes' => [
                'type' => 'password',
            ],
        ]);
        $inputFilter->add([
            'name'       => 'password_new',
            'required'   => true,
            'allowEmpty' => false,
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 55,
                    ],
                ],
                [
                    'name' => 'QP\Common\Validator\PasswordPolicy',
                ],
            ],
        ]);

        //-------------------------------------------------- password repeat ---
        $this->add([
            'name'    => 'password_repeat',
            'options' => [
                'label' => 'RESET_PASSWORD_FORM_PASSWORD_REPEAT',
            ],
            'attributes' => [
                'type' => 'password',
            ],
        ]);
        $inputFilter->add([
            'name'       => 'password_repeat',
            'required'   => true,
            'allowEmpty' => false,
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 55,
                    ],
                ],
                [
                    'name' => 'Identical',
                    'options' => [
                        'token' => 'password_new',
                        'messages' => [
                            \Zend\Validator\Identical::NOT_SAME      => 'RESET_PASSWORD_FORM_VALIDATOR_PASS_NOT_SAME',
                            \Zend\Validator\Identical::MISSING_TOKEN => 'RESET_PASSWORD_FORM_VALIDATOR_PASS_MISSING',
                        ],
                    ],
                ],
                [
                    'name' => 'QP\Common\Validator\PasswordPolicy',
                ],
            ],
        ]);

        //----------------------------------------------------------- submit ---
        $this->add([
            'name'       => 'submitButton',
            'attributes' => [
                'type'  => 'submit',
                'value' => 'PROFILE_RESET_PASSWORD',
                'class' => 'btn btn--primary',
            ],
        ]);
        $inputFilter->add([
            'name'       => 'submitButton',
            'required'   => false,
            'allowEmpty' => true,
        ]);

        //---------------------------------------- set input filters to form ---
        $this->setInputFilter($inputFilter);
    }
}