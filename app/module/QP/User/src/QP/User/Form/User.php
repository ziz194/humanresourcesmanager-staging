<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c] 2016 Quadriga Media GmbH
 */
namespace QP\User\Form;

use Zend\InputFilter\InputFilter;
use QP\Common\Form\FormBaseAbstract;

class User extends FormBaseAbstract
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct('user');
        $this->setAttributes([
            'method'      => 'post',
            'data-module' => 'ModalForm'
        ]);
        $inputFilter = new InputFilter();

        //-------------------------------------------------------- firstname ---
        $this->add([
            'name'    => 'firstname',
            'options' => [
                'label' => 'PROFILE_FIRSTNAME',
            ],
        ]);
        $inputFilter->add([
            'name'       => 'firstname',
            'required'   => true,
            'allowEmpty' => false,
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 55,
                    ],
                ],
            ],
        ]);

        //--------------------------------------------------------- lastname ---
        $this->add([
            'name'    => 'lastname',
            'options' => [
                'label' => 'PROFILE_LASTNAME',
            ],
        ]);
        $inputFilter->add([
            'name'       => 'lastname',
            'required'   => true,
            'allowEmpty' => false,
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 55,
                    ],
                ],
            ],
        ]);

        //------------------------------------------------------------ email ---
        $this->add([
            'name'    => 'email',
            'options' => [
                'label' => 'PROFILE_EMAIL',
                'acl'   => [
                    'resource'  => 'QP\User\Form\User',
                    'privilege' => 'user-edit-email'
                ],
            ],
        ]);
        $inputFilter->add([
            'name'       => 'email',
            'required'   => true,
            'allowEmpty' => false,
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'EmailAddress',
                    'options' => [
                        'domain'   => 'true',
                        'hostname' => 'true',
                        'mx'       => 'true',
                        'deep'     => 'true',
                    ],
                ],
            ],
        ]);

        //------------------------------------------------------------ state ---
        $this->add([
            'name'    => 'state',
            'type'    => 'Zend\Form\Element\Select',
            'options' => [
                'label' => 'PROFILE_STATE',
                'value_options' => [
                    \QP\User\Entity\User::STATE_ACTIVE   => 'PROFILE_STATE_ACTIVE',
                    \QP\User\Entity\User::STATE_INACTIVE => 'PROFILE_STATE_INACTIVE',
                ],
                'acl'   => [
                    'resource'  => 'QP\User\Form\User',
                    'privilege' => 'user-edit-state'
                ],
            ],
        ]);

        //----------------------------------------------------------- submit ---
        $this->add([
            'name'       => 'submitButton',
            'attributes' => [
                'type'  => 'submit',
                'value' => 'COMMON_FORM_SAVE',
                'class' => 'btn btn--primary',
            ],
        ]);
        $inputFilter->add([
            'name'       => 'submitButton',
            'required'   => false,
            'allowEmpty' => true,
        ]);

        //---------------------------------------- set input filters to form ---
        $this->setInputFilter($inputFilter);
    }
}