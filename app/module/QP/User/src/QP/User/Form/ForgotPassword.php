<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c] 2016 Quadriga Media GmbH
 */
namespace QP\User\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use QP\Common\View\Helper\Custom\Captcha\CustomCaptcha as CaptchaImage;

class ForgotPassword extends Form
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct('forgot-password');
        $this->setAttribute('method', 'post');
        $inputFilter = new InputFilter();

        //------------------------------------------------------------ email ---
        $this->add([
            'name'    => 'email',
            'options' => [
                'label' => 'FORGOT_FORM_EMAIL',
            ],
            'attributes' => [
                'type' => 'text',
            ],
        ]);
        $inputFilter->add([
            'name'       => 'email',
            'required'   => true,
            'allowEmpty' => false,
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 6,
                        'max'      => 255,
                    ],
                ],
                [
                    'name'    => 'EmailAddress',
                    'options' => [
                        'domain'   => 'true',
                        'hostname' => 'true',
                        'mx'       => 'true',
                        'deep'     => 'true',
                        'message'  => 'Invalid email address',
                    ],
                ],
            ],
        ]);

        //---------------------------------------------------------- captcha ---
        $captchaImage = new CaptchaImage([
            'font'   => getcwd() . '/public/fonts/ubuntu-mono/UbuntuMono-B.ttf',
            'imgDir' => getcwd() . '/data/captcha/',
            'imgUrl' => '/captcha/',
            'imgAlt' => 'captcha',
            'fsize'  => 50,
            'width'  => 250,
            'height' => 100,
            'dotNoiseLevel'  => 200,
            'lineNoiseLevel' => 30
        ]);

        //add captcha element...
        $this->add([
            'type'    => 'Zend\Form\Element\Captcha',
            'name'    => 'captcha',
            'options' => [
                'label'   => 'FORGOT_FORM_CAPTCHA',
                'captcha' => $captchaImage,
                'class'   => 'captcha_input',
            ],
        ]);

        //----------------------------------------------------------- submit ---
        $this->add([
            'name'       => 'submit',
            'attributes' => [
                'type'  => 'submit',
                'value' => 'FORGOT_FORM_BUTTON',
                'class' => 'btn btn--primary',
            ],
        ]);
        $inputFilter->add([
            'name'       => 'submit',
            'required'   => false,
            'allowEmpty' => true,
        ]);

        //---------------------------------------- set input filters to form ---
        $this->setInputFilter($inputFilter);
    }
}