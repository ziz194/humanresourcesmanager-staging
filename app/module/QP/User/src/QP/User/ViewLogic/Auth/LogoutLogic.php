<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\ViewLogic\Auth;

use QP\Common\ViewLogic;
use QP\Common\ViewLogic\Response;

/**
 * Class LogoutLogic
 * @package QP\User\ViewLogic\Auth
 */
class LogoutLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{

    /**
     * LogoutLogic constructor.
     */
    public function __construct()
    {
    }

    /**
     * Generate the view response.
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse()
    {
        if ($this->getAuthenticationService()->hasIdentity()) {
            $this->getAuthenticationService()->clearIdentity();
            $this->getLogService()->info(
                'AUTH_LOGGER_USER_LOGOUT',
                ['profile' => $this->getAuthenticationService()->getIdentity()]
            );
        }

        $this->getSessionManager()->destroy();

        return new Response\TemplateResponse(
            'qp/user/auth/logout',
            []
        );
    }
}
