<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\ViewLogic\Auth;

use Zend\Http\Request;
use QP\Common\ViewLogic;
use QP\User\Entity\User;
use QP\User\Form\RecoverPassword;
use QP\Common\ViewLogic\Response;
use QP\Common\View\Helper\SystemMessage;

/**
 * Class RecoverLogic
 * @package QP\User\ViewLogic\Auth
 */
class RecoverLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{
    /**
     * @var array $config
     */
    private $config;

    /**
     * @var string $recoverToken
     */
    private $recoverToken;

    /**
     * @var \Zend\Http\Request $request
     */
    private $request;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $error;

    /**
     * @var array
     */
    private $_messages = [];

    /**
     * @param array                 $config
     * @param string                $recoverToken
     * @param \Zend\Http\Request    $request
     */
    public function __construct(array $config, $recoverToken, Request $request)
    {
        $this->config       = $config;
        $this->request      = $request;
        $this->recoverToken = $recoverToken;
    }

    /**
     * Generate the view response.
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse()
    {
        // only for logged out users if a user is logged in redirect to home
        if ($this->getAuthenticationService()->hasIdentity()) {
            return new Response\RedirectResponse('home');
        }

        // decrypt the token, get the email and the date when
        // this token has been created
        $recoverToken = $this->getCrypt()->decrypt($this->recoverToken);
        $tokenParts   = explode('$$$', $recoverToken);

        // check if the parts are set where the email and the date are stored
        if (isset($tokenParts[1]) && isset($tokenParts[2])) {
            // check if the user could be found by the email from the string
            $this->user = $this->getEntityManager()->getRepository('QP\User\Entity\User')->readOneByEmail($tokenParts[1]);
            if (is_null($this->user) || $this->user === false) {
                $this->error = 'RECOVER_MESSAGE_NO_SUCH_USER';
            } else {
                // check if the link is already expired
                if (time() > $tokenParts[2] + $this->config['rec_token_lifetime']) {
                    $this->error = 'RECOVER_MESSAGE_EXPIRED';
                } else {
                    // check if the user is already activated
                    if ($this->user->getState() !== 1) {
                        $this->error = 'RECOVER_MESSAGE_INACTIVE';
                    }
                }
            }
        }
        if ($this->error) {
            $this->getFlashMessenger()->addMessage([
                'msg' => $this->error,
                'typ' => SystemMessage::STATUS_FAIL,
            ]);
            return new Response\RedirectResponse('user-login');
        }

        // get the form for this action
        $form = new RecoverPassword();

        // after sending the form
        if ($this->request->isPost()) {
            // set the retrieved post data to this form object
            $form->setData($this->request->getPost());

            // check if all inputs are valid
            if ($form->isValid()) {
                // get data from the form
                $validatedData = $form->getData();
                $validatedData['password'] = User::encryptPassword($this->config['static_salt'], $validatedData['password_repeat'], $this->user->getSalt());

                try {
                    $this->user->exchangeArray($validatedData);
                    $this->getEntityManager()->getRepository('QP\User\Entity\User')->save($this->user);
                    $this->getEntityManager()->flush();

                    // set message for login view
                    $this->getFlashMessenger()->addMessage([
                        'msg' => 'RECOVER_PASSWORD_SUCCESS',
                        'typ' => SystemMessage::STATUS_GOOD,
                    ]);

                    $this->getLogService()->info(
                        'AUTH_LOGGER_USER_RECOVER_PASS',
                        [
                            'user'    => $this->user->getArrayCopy(),
                            'profile' => $this->user->getArrayCopy(),
                        ]
                    );
                } catch(\Exception $e) {
                    // TODO: elaborate (LOGGING)
                    $this->getFlashMessenger()->addMessage([
                        'msg' => 'COMMON_FAIL',
                        'typ' => SystemMessage::STATUS_FAIL,
                    ]);
                }
                return new Response\RedirectResponse('user-login');

            } // if the form is not valid
            else {
                // set the form error messages to the view variables
                $this->_messages = $form->getMessages();
            }
        }

        return new Response\TemplateResponse(
            'qp/user/auth/recover',
            [
                'form'         => $form,
                'messages'     => $this->_messages,
                'recoverToken' => $this->recoverToken,
            ]
        );
    }
}
