<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\ViewLogic\Profile;

use Zend\Http\Request;
use QP\Common\ViewLogic;
use QP\User\Entity\User;
use QP\User\Authentication\Login;
use QP\Common\ViewLogic\Response;
use QP\Common\View\Helper\SystemMessage;
use QP\User\Form\ResetPassword as ResetPasswordForm;

/**
 * Class PasswordLogic
 * @package QP\User\ViewLogic\Profile
 */
class PasswordLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{
    /**
     * @var array $config
     */
    private $config;

    /**
     * @var \Zend\Http\Request $request
     */
    private $request;

    /**
     * @var \Zend\I18n\Translator\Translator
     */
    private $translator;

    /**
     * @var \Zend\Mvc\Service\ViewHelperManagerFactory
     */
    private $viewHelperManager;

    /**
     * @var array
     */
    private $_messages = [];

    /**
     * LoginLogic constructor.
     *
     * @param array $config
     * @param \Zend\Http\Request $request
     * @param \Zend\I18n\Translator\Translator $translator
     * @param \Zend\Mvc\Service\ViewHelperManagerFactory $viewHelperManager
     */
    public function __construct(array $config, Request $request, $translator, $viewHelperManager)
    {
        $this->config     = $config;
        $this->request    = $request;
        $this->translator = $translator;
        $this->viewHelperManager = $viewHelperManager;
    }

    /**
     * Generate the view response.
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse()
    {
        $form = new ResetPasswordForm();
        if ($this->request->isPost()) {
            $form->setData($this->request->getPost());
            if ($form->isValid()) {
                $me   = $this->getAuthenticationService()->getIdentity();
                $user = $this->getEntityManager()->getRepository('QP\User\Entity\User')->find($me['id']);
                $validatedData = $form->getData();

                if(Login::validatePassword($validatedData['password_old'], $this->config['static_salt'], $user)){
                    if ($this->request->isXmlHttpRequest()) {
                        return new Response\JsonResponse(['success' => true]);
                    }

                    try {
                        $user->exchangeArray([
                            'password' => User::encryptPassword($this->config['static_salt'], $validatedData['password_repeat'], $user->getSalt()),
                        ]);
                        $this->getEntityManager()->getRepository('QP\User\Entity\User')->save($user);
                        $this->getEntityManager()->flush();

                        $this->getLogService()->info(
                            'PROFILE_LOGGER_CHANGE_PASS',
                            ['profile' => $user->getArrayCopy()]
                        );

                        // set message for login view
                        $this->getFlashMessenger()->addMessage([
                            'msg' => 'RESET_PASSWORD_SUCCESS',
                            'typ' => SystemMessage::STATUS_GOOD,
                        ]);
                    } catch(\Exception $e) {
                        // TODO: elaborate (LOGGING)
                        $this->getFlashMessenger()->addMessage([
                            'msg' => 'COMMON_FAIL',
                            'typ' => SystemMessage::STATUS_FAIL,
                        ]);
                    }
                    return new Response\RedirectResponse('profile');
                }
                else {
                    $this->_messages = [
                        'password_old' => [
                            'incorrectPass' => $this->translator->translate('PROFILE_RESET_PASSWORD_INCORRECT_OLD')
                        ]
                    ];
                }
            } // if the form is not valid
            else {
                // set the form error messages to the view variables
                $this->_messages = $form->getMessages();
            }

            if ($this->request->isXmlHttpRequest()) {
                $formErrors = $this->viewHelperManager->get('formErrors');
                $formErrors = $formErrors($this->_messages, $form);
                return new Response\JsonResponse([
                    'errors' => $formErrors,
                ]);
            }
        }

        return new Response\TemplateResponse(
            'qp/user/profile/password',
            [
                'form'     => $form,
                'messages' => $this->_messages
            ]
        );
    }
}