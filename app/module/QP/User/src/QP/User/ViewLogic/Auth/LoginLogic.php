<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\ViewLogic\Auth;

use Zend\Http\Request;
use QP\Common\ViewLogic;
use QP\Common\ViewLogic\Response;
use QP\User\Authentication\Login;
use QP\User\Form\Login as LoginForm;

/**
 * Class LoginLogic
 * @package QP\User\ViewLogic\Auth
 */
class LoginLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{
    /**
     * @var array $config
     */
    private $config;

    /**
     * @var \Zend\Http\Request $request
     */
    private $request;

    /**
     * @var \Zend\I18n\Translator\Translator
     */
    private $translator;

    /**
     * @var array
     */
    private $_messages = [];

    /**
     * LoginLogic constructor.
     *
     * @param array $config
     * @param \Zend\Http\Request $request
     * @param \Zend\I18n\Translator\Translator $translator
     */
    public function __construct(array $config, Request $request, $translator)
    {
        $this->config     = $config;
        $this->request    = $request;
        $this->translator = $translator;
    }

    /**
     * Generate the view response.
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse()
    {
        if ($this->getAuthenticationService()->hasIdentity()) {
            return new Response\RedirectResponse('home');
        }

        $form = new LoginForm();

        //$authResult = $this->getAuthenticationService()->authenticate( new Result(Result::SUCCESS, $meArray) );

        // when form is send
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);

            if ($form->isValid()) {
                $adapter = new Login($post['email'], $post['password'], $this->config['static_salt'], $this->getEntityManager());
                $authResult = $this->getAuthenticationService()->authenticate($adapter);

                if ($authResult->isValid()) {

                    $this->getSessionManager()->start();
                    $_SESSION['timeout_idle'] = time() + $this->config['session_config']['remember_me_seconds'];

                    $identity = $authResult->getIdentity();
                    $this->getAuthenticationService()->getStorage()->write($identity);

                    $this->getLogService()->info(
                        'AUTH_LOGGER_USER_LOGIN',
                        [
                            'user'    => $identity,
                            'profile' => $identity,
                        ]
                    );
                    return new Response\RedirectResponse('home');
                }

                $this->_messages = '';
                foreach ($authResult->getMessages() as $message) {
                    $this->_messages .= $this->translator->translate($message) . '<br />';
                }
            } else {
                $this->_messages = $form->getMessages();
            }
        }

        return new Response\TemplateResponse(
            'qp/user/auth/login',
            [
                'form' => $form,
                'messages' => $this->_messages
            ]
        );
    }
}