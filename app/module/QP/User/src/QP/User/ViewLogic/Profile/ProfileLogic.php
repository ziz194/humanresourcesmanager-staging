<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\ViewLogic\Profile;

use Zend\Http\Request;
use QP\Common\ViewLogic;
use QP\Common\ViewLogic\Response;
use QP\User\Authentication\Login;
use QP\User\Form\Login as LoginForm;

/**
 * Class ProfileLogic
 * @package QP\User\ViewLogic\Profile
 */
class ProfileLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{
    /**
     * Generate the view response.
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse()
    {
//        \FirePHP::getInstance(true)->info($this->getAuthenticationService()->getIdentity());
        return new Response\TemplateResponse(
            'qp/user/profile/profile',
            [
                'me' => $this->getAuthenticationService()->getIdentity(),
            ]
        );
    }
}