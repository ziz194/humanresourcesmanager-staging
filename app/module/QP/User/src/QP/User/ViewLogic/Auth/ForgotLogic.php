<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\ViewLogic\Auth;

use Zend\Http\Request;
use QP\Common\ViewLogic;
use QP\User\Form\ForgotPassword;
use QP\Common\ViewLogic\Response;
use QP\Common\View\Helper\SystemMessage;

/**
 * Class ForgotLogic
 * @package QP\User\ViewLogic\Auth
 */
class ForgotLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{
    /**
     * @var array $config
     */
    private $config;

    /**
     * @var string $baseUrl
     */
    private $baseUrl;

    /**
     * @var \Zend\Http\Request $request
     */
    private $request;

    /**
     * @var \Zend\I18n\Translator\Translator
     */
    private $translator;

    /**
     * @var array
     */
    private $_messages = [];

    /**
     * @param array                             $config
     * @param \Zend\Http\Request                $request
     * @param \Zend\I18n\Translator\Translator  $translator
     * @param string                            $baseUrl
     */
    public function __construct(array $config, Request $request, $translator, $baseUrl)
    {
        $this->config     = $config;
        $this->baseUrl    = $baseUrl;
        $this->request    = $request;
        $this->translator = $translator;
    }

    /**
     * Generate the view response.
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse()
    {
        // only for logged out users if a user is logged in redirect to home
        if ($this->getAuthenticationService()->hasIdentity()) {
            return new Response\RedirectResponse('home');
        }

        // get the form for this action
        $form = new ForgotPassword();

        // after sending the form
        if ($this->request->isPost()) {
            // set the retrieved post data to this form object
            $form->setData($this->request->getPost());

            // check if all inputs are valid
            if ($form->isValid()) {
                // get data from the form
                $validatedData = $form->getData();
                $user = $this->getEntityManager()->getRepository('QP\User\Entity\User')->readOneByEmail($validatedData['email']);

                if (!is_null($user)) {
                    $userArray  = $user->getArrayCopy();
                    $cipherText = $this->getCrypt()->encrypt('ND.|..$$$'.$userArray['email'].'$$$'.time().'$$$.|..ND');
                    $text = $this->translator->translate('FORGOT_PASSWORD_MAIL_TEXT');
                    $text = str_replace('%%NAME%%', $userArray['fullname'], $text);
                    $text = str_replace('%%LINK%%', ($this->baseUrl . '/user/recover/' . $cipherText), $text);

                    $ret = $this->getApiClient()->mail([
                        'to'      => $this->config['mail']['my_mail'], //$userArray['email'], TODO: replace dummy
                        'from'    => $this->config['mail']['system_sender_email'],
                        'subject' => $this->translator->translate('FORGOT_PASSWORD_MAIL_SUBJECT'),
                        'text'    => $text,
                    ]);

                    // set message for login view
                    if ($ret['result'] === 'MAIL_SUCCESS') {
                        $this->getFlashMessenger()->addMessage([
                            'msg' => 'FORGOT_PASSWORD_SUCCESS',
                            'typ' => SystemMessage::STATUS_GOOD,
                        ]);
                    } else {
                        $this->getFlashMessenger()->addMessage([
                            'msg' => 'FORGOT_PASSWORD_FAIL',
                            'typ' => SystemMessage::STATUS_FAIL,
                        ]);
                    }

                    $this->getLogService()->info(
                        'AUTH_LOGGER_FORGOT_PASS',
                        [
                            'user'    => $userArray,
                            'profile' => $userArray,
                        ]
                    );
                    return new Response\RedirectResponse('user-login');
                }
                else {
                    $this->getFlashMessenger()->addMessage([
                        'msg' => 'FORGOT_PASSWORD_FAIL_NO_SUCH_USER',
                        'typ' => SystemMessage::STATUS_FAIL,
                    ]);
                    return new Response\RedirectResponse('user-login');
                }
            } // if the form is not valid
            else {
                // set the form error messages to the view variables
                $this->_messages = $form->getMessages();
            }
        }

        return new Response\TemplateResponse(
            'qp/user/auth/forgot',
            [
                'form'     => $form,
                'messages' => $this->_messages
            ]
        );
    }
}
