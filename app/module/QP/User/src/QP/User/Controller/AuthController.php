<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Controller;

use QP\Common\Controller\AbstractController;

/**
 * Controller for handling the authentication.
 *
 * @author Andy Killat
 */
class AuthController extends AbstractController
{
    /**
     * Authenticate a user with the application.
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function loginAction()
    {
        $config     = $this->getServiceLocator()->get('Config');
        $translator = $this->getServiceLocator()->get('translator');
        $viewLogic  = $this->getViewLogicFactory()->build(
            'User\ViewLogic\Auth\LoginLogic',
            [
                $config,
                $this->getRequest(),
                $translator,
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function forgotAction()
    {
        $config     = $this->getServiceLocator()->get('Config');
        $translator = $this->getServiceLocator()->get('translator');
        $viewLogic  = $this->getViewLogicFactory()->build(
            'User\ViewLogic\Auth\ForgotLogic',
            [
                $config,
                $this->getRequest(),
                $translator,
                $this->getBaseUrl(),
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function recoverAction()
    {
        $config       = $this->getServiceLocator()->get('Config');
        $recoverToken = $this->params()->fromRoute('recoverToken');
        $viewLogic    = $this->getViewLogicFactory()->build(
            'User\ViewLogic\Auth\RecoverLogic',
            [
                $config,
                $recoverToken,
                $this->getRequest(),
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }

    /**
     * Logout current user from the application.
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function logoutAction()
    {
        $viewLogic = $this->getViewLogicFactory()->build(
            'User\ViewLogic\Auth\LogoutLogic',
            []
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }
}