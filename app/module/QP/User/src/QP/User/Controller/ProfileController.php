<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Controller;

use QP\Common\Controller\AbstractController;

/**
 * Controller for handling the user profile.
 *
 * @author Andy Killat
 */
class ProfileController extends AbstractController
{
    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function profileAction()
    {
        $viewLogic  = $this->getViewLogicFactory()->build(
            'User\ViewLogic\Profile\ProfileLogic',
            []
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function passwordAction()
    {
        $this->layout('layout/clean');
        $viewLogic  = $this->getViewLogicFactory()->build(
            'User\ViewLogic\Profile\PasswordLogic',
            [
                $this->getServiceLocator()->get('Config'),
                $this->getRequest(),
                $this->getServiceLocator()->get('translator'),
                $this->getServiceLocator()->get('ViewHelperManager'),
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction()
    {
        $this->layout('layout/clean');
        $viewLogic  = $this->getViewLogicFactory()->build(
            'User\ViewLogic\Profile\EditLogic',
            [
                $this->getServiceLocator()->get('Config'),
                $this->getRequest(),
                $this->getServiceLocator()->get('translator'),
                $this->getServiceLocator()->get('ViewHelperManager'),
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }
}