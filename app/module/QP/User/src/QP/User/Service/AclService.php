<?php
/**
 * Quadriga Plattform WEB
 *
 * AclService interprets the config array within in the navigation.global.php
 * and create a Zend Acl Class according to this config.
 *
 * It will be set to the navigation helper in order to sort out which
 * navigation points are visible for the current user role.
 *
 * Also, this service will be use in the onRoute event to check if the current
 * user role has sufficient rights to access the requested URL.
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Service;

use QP\User\Entity\Role;
use Zend\Permissions\Acl\Acl as ZendAcl;
use Zend\Permissions\Acl\Role\GenericRole;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 * Class AclService
 * @package QP\User\Service
 */
class AclService extends ZendAcl implements ServiceLocatorAwareInterface
{
    /**
     * @var array $config
     */
    private $config;

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    protected $serviceLocator;

    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * Set the service locator.
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Constructor
     *
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config)
    {
        if (!isset($config['acl']['permissions']) || !isset($config['acl']['resources'])) {
            throw new \Exception('Invalid ACL Config found');
        }

        $this->config = $config;
    }

    /**
     * Create a role based on the current identity
     *
     * @param \Zend\Authentication\AuthenticationService $authService
     * @return \Zend\Permissions\Acl\Role\GenericRole
     */
    public function createRole($authService)
    {
//        if ($authService->hasIdentity()) {
//            return $this->createUserAclRole(
//                $authService->getIdentity()['roleNames'],
//                $authService->getIdentity()['id']
//            );
//        } else {
            return $this->createGuestAclRole();
//        }
    }

    /**
     * Create a new ACL role for a user based on the user roles.
     *
     * @param array $roleNames An array of roles assigned to a user.
     * @param int $userId The id of the user to create the role for.
     * @return GenericRole The user-specific role.
     */
    public function createUserAclRole($roleNames, $userId)
    {
        $aclRoleId = 'ROLE_USER_' . $userId;
        $role = $this->createAclRole($aclRoleId);

        $permissions = $this->getPermissionsByRoleExtIds($roleNames);
        foreach ($permissions as $resource => $privileges) {
            foreach ($privileges as $privilege) {
                $this->allow($aclRoleId, $resource, $privilege);
            }
        }

        return $role;
    }

    /**
     * Create a new ACL role for a guest.
     *
     * @return \Zend\Permissions\Acl\Role\GenericRole The user-specific role.
     */
    public function createGuestAclRole()
    {
        $aclRoleId = 'ROLE_GUEST';
        $role = $this->createAclRole($aclRoleId);

        $permissions = $this->getPermissionsByRoleExtIds(array(Role::ROLE_DEFAULT));
        foreach ($permissions as $resource => $privileges) {
            foreach ($privileges as $privilege) {
                $this->allow($aclRoleId, $resource, $privilege);
            }
        }

        return $role;
    }


    /**
     * Initialize all resources available.
     */
    private function initializeResources()
    {
        $resources = $this->config['acl']['resources'];
        foreach ($resources as $resource) {
            $resource = new GenericResource($resource);
            if (!$this->hasResource($resource)) {
                $this->addResource($resource);
            }
        }
    }

    /**
     * Create an empty ACL role if it does not exist.
     *
     * @param $aclRoleId
     * @return GenericRole|\Zend\Permissions\Acl\Role\RoleInterface
     */
    private function createAclRole($aclRoleId)
    {
        if ($this->hasRole($aclRoleId)) {
            $role = $this->getRole($aclRoleId);
        } else {
            $role = new GenericRole($aclRoleId);
            $this->addRole($role);
            $this->initializeResources();
        }

        return $role;
    }

    /**
     * Get the union set of permissions a user has, based on the roles assigned.
     *
     * @param array $roles A list of all roles assigned to a user.
     *
     * @return array A list of all permissions a user has.
     */
    private function getPermissionsByRoleExtIds($roleNames)
    {
        $allPermissions = array();

        foreach ($roleNames as $roleName) {
            $resources = $this->config['acl']['permissions'][$roleName];
            foreach ($resources as $resource => $privileges) {
                foreach ($privileges as $privilege) {
                    if (!array_key_exists($resource, $allPermissions)
                        || !in_array(
                            $privilege,
                            $allPermissions[$resource]
                        )
                    ) {
                        $allPermissions[$resource][] = $privilege;
                    }
                }
            }
        }

        return $allPermissions;
    }
}
