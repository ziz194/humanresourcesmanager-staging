<?php
/**
 * Quadriga Plattform WEB
 *
 * This Factory will create and return the AclService as a Service Locator
 * Aware Interface.
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Service;

/**
 * Class AclFactory
 * @package QP\User\Service
 */
class AclFactory
{
    /**
     * Create an acl service instance
     *
     * @param array $services
     *
     * @return \QP\User\Service\AclService
     */
    public function __invoke($services)
    {
        $config = $services->get('config');
        return new AclService($config['acl']);
    }
}
