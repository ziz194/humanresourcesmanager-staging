<?php
/**
 * Quadriga Plattform WEB
 *
 * This Factory will create and return the UserService as a Service Locator
 * Aware Interface.
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\Service;

/**
 * Class UserServiceFactory
 * @package QP\User\Service
 */
class UserServiceFactory
{
    /**
     * Create an organisation service instance
     *
     * @param array $services
     *
     * @return \QP\User\Service\UserService
     */
    public function __invoke($services)
    {
        $entityManager = $services->get('EntityManager');
        return new UserService($entityManager);
    }
}
