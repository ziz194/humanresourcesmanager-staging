<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\EntityRepository;

use QP\Common\EntityRepository\DoctrineEntityRepository;

/**
 * A repository for capsuling custom follow queries.
 *
 * @author Andy Killat
 */
class FollowRepository extends DoctrineEntityRepository
{

    /**
     * Read a follow data set by user id.
     *
     * @param string $userId
     *
     * @return Object|\QP\User\Entity\Follow  A single follow data set.
     */
    public function readOneByUserId($userId)
    {
        $follow = $this->findOneBy(['user_id' => $userId]);

        return $follow;
    }
}
