<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\EntityRepository;

use QP\Common\EntityRepository\DoctrineEntityRepository;

/**
 * A repository for capsuling custom event submit queries.
 *
 * @author Andy Killat
 */
class EventSubmitRepository extends DoctrineEntityRepository
{

    /**
     * Read a event submit data set by user id.
     *
     * @param string $userId
     *
     * @return Object|\QP\User\Entity\Contact  A single event submit data set.
     */
    public function readOneByUserId($userId)
    {
        $follow = $this->findOneBy(['userId' => $userId]);

        return $follow;
    }
}
