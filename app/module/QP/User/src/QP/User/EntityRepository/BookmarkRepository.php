<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\EntityRepository;

use QP\Common\EntityRepository\DoctrineEntityRepository;

/**
 * A repository for capsuling custom bookmark queries.
 *
 * @author Andy Killat
 */
class BookmarkRepository extends DoctrineEntityRepository
{

    /**
     * Read all bookmarks by user id.
     *
     * @param string $userId
     *
     * @return array
     */
    public function readByUserId($userId)
    {
        $bookmarks = $this->findBy(['userId' => $userId]);

        return $bookmarks;
    }

    /**
     * Delete a single bookmarks by user id and es id.
     *
     * @param string $userId
     * @param string $esId
     */
    public function deleteOneByUserIdAndEsId($userId, $esId)
    {
        $query = $this->getEntityManager()->createQuery(
            'DELETE FROM QP\User\Entity\Bookmark b 
              WHERE b.userId = :userId
                AND b.esId = :esId' );
        $query->setParameter( 'userId', $userId );
        $query->setParameter( 'esId', $esId );

        return $query->getResult();
    }
}
