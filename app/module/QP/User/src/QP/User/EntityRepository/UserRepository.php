<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User\EntityRepository;

use QP\Common\EntityRepository\DoctrineEntityRepository;

/**
 * A repository for capsuling custom user queries.
 *
 * @author Andy Killat
 */
class UserRepository extends DoctrineEntityRepository
{
    /**
     * Read a user by email.
     *
     * @param string $email
     *
     * @return Object|\QP\User\Entity\User  A single user.
     */
    public function readOneByEmail($email)
    {
        $user = $this->findOneBy(['email' => $email]);

        return $user;
    }

    /**
     * Read a user by cookie.
     *
     * @return Object|\QP\User\Entity\User  A single user.
     */
    public function readOneByCookie()
    {
        $idHash = '';
        if (isset($_COOKIE['followMeId']) && $_COOKIE['followMeId']) {
            $idHash = $_COOKIE['followMeId'];
        }

        $user = $this->findOneBy(['idHash' => $idHash]);

        return $user;
    }

    /**
     * Read a user by id hash.
     *
     * @param string $idHash
     *
     * @return Object|\QP\User\Entity\User  A single user.
     */
    public function readOneByIdHash($idHash)
    {
        $user = $this->findOneBy(['idHash' => $idHash]);

        return $user;
    }
}
