<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\User;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\View\HelperPluginManager;
use Zend\Validator\AbstractValidator;
use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;

/**
 * Class Module
 * @package QP\User
 */
class Module implements AutoloaderProviderInterface
{
    /**
     * Bootstrap the module.
     *
     * @param \Zend\Mvc\MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $application  = $e->getTarget();
        $eventManager = $e->getApplication()->getEventManager();

        $services = $application->getServiceManager();
        $eventManager->attach('dispatch.error', function ($event) use ($services) {
            $exception = $event->getResult()->exception;
            if (!$exception) {
                return;
            }
            $service = $services->get('ErrorLogger');
            $service->logException($exception);
        });

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        // attached events
        $eventManager->attach('route', [$this, 'onRoute'], -100);
    }

    /**
     * Load the module config.
     *
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Load namespaces for module.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__)
                ],
            ],
        ];
    }

    /**
     * Will be executed before every new page request.
     *
     * Mainly this function checks if the user has sufficient rights to
     * access the requested page.
     *
     * @param EventInterface $ev
     */
    public function onRoute(EventInterface $ev)
    {
        // Event manager of the app
        $application    = $ev->getApplication();
        $serviceManager = $application->getServiceManager();

        $config = $serviceManager->get('Config');
        /* @var \Zend\Authentication\AuthenticationService $auth */
        $auth = $serviceManager->get('Zend\Authentication\AuthenticationService');
        /* @var \QP\User\Service\AclService $acl */
        $aclService = $serviceManager->get('AclService');
        /* @var \Zend\Session\SessionManager $sessionManager */
        $sessionManager = $serviceManager->get('Zend\Session\SessionManager');
        
        // get the requested location
        $routeMatch       = $ev->getRouteMatch();
        $controller       = $routeMatch->getParam('controller');
        $matchedRouteName = $routeMatch->getMatchedRouteName();
        //\FirePHP::getInstance(true)->info($matchedRouteName);

        $ev->getViewModel()->setVariable('route', $matchedRouteName);

        $aclRole = $aclService->createRole($auth);

        // if the user is not allowed
//        if (!$aclService->isAllowed($aclRole, $controller, $matchedRouteName)
//            && !($ev->getRequest() instanceof \Zend\Console\Request))
//        {
            // check if the user has an identity at all
//            if (!$auth->hasIdentity()) {
//                $url = $ev->getRouter()->assemble([], ['name' => 'user-login']);
//
//            } // check if there is a redirect route defined within the acl config
//            else
//            if (!empty($config['acl']['redirect_route'])) {
//                $rr  = $config['acl']['redirect_route'];
//                $url = $ev->getRouter()->assemble([], $rr['options']);
//            } else {
//                // if there is no definition set the default route
//                $url = $ev->getRouter()->assemble([], ['name' => 'error']);
//            }
//
//            return $this->redirectToLocation($ev, $url);
//        }

//        \FirePHP::getInstance(true)->info($_SESSION);
//        \FirePHP::getInstance(true)->info($config['session_config']['remember_me_seconds']);

        // lets check if the session should be terminated because of inactivity
        if(isset($_SESSION['timeout_idle']) && $_SESSION['timeout_idle'] < time()) {
            $auth->clearIdentity();
            $sessionManager->destroy();
        }
        $_SESSION['timeout_idle'] = time() + $config['session_config']['remember_me_seconds'];

        //\FirePHP::getInstance(true)->info($_COOKIE);
        if (isset($_COOKIE['followMeId']) && $_COOKIE['followMeId']) {
            $ev->getViewModel()->setVariable('followCookie', true);
        } else {
            $ev->getViewModel()->setVariable('followCookie', false);
        }

        $trans = $serviceManager->get('translator');
        $trans->setLocale('de');
        AbstractValidator::setDefaultTranslator($trans);
    }

    /**
     * Load the view helper config.
     *
     * @return array
     */
    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                // This will overwrite the native navigation helper
                'navigation' => function (HelperPluginManager $pm) {
                    $sm   = $pm->getServiceLocator();
                    $acl  = $sm->get('AclService');
                    $auth = $sm->get('Zend\Authentication\AuthenticationService');

                    $aclRole = $acl->createRole($auth);

                    $navigation = $pm->get('Zend\View\Helper\Navigation');
                    $navigation->setAcl($acl)
                               ->setRole($aclRole);

                    return $navigation;
                },
                'isAllowed' => function (HelperPluginManager $pm) {
                    $sm   = $pm->getServiceLocator();
                    $acl  = $sm->get('AclService');
                    $auth = $sm->get('Zend\Authentication\AuthenticationService');

                    $aclRole = $acl->createRole($auth);

                    $isAllowed = $pm->get('QP\Common\View\Helper\IsAllowed');
                    $isAllowed->setAcl($acl)
                              ->setRole($aclRole);

                    return $isAllowed;
                }
            ]
        ];
    }

    private function redirectToLocation($e, $url)
    {
        $response = $e->getResponse();

        // perform the redirect
        $response->getHeaders()->addHeaderLine('Location', $url);
        $response->setStatusCode(302);
        $response->sendHeaders();

        if (php_sapi_name() !== 'cli') {
            exit;
        }
    }
}
