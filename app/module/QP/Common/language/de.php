<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */

return [
    //------------------------------------ text parts used all over the site ---
    'COMMON_NO_COOKIE_TXT'                              => 'Diese Seite nutzt Cookies, um bestmögliche Funktionalität bieten zu können.',
    'COMMON_MESSAGE_404'                                => 'Oh nein, wie unangenehm. Diese Seite konnte leider nicht gefunden werden.',
    'COMMON_BUTTON_ACCEPT_COOKIE'                       => 'OK, Verstanden',

    'HEADER_NAV_NEWSLETTER'                             => 'Newsletter',
    'HEADER_NAV_SHOPPING_CART'                          => 'Warenkorb',
    'HEADER_NAV_BOOKMARKS'                              => 'Merklist',
    'HEADER_NAV_MEDIA'                                  => 'Mediadaten',

    'HEADER_NAV_TOGGLE'                                 => 'Toggle navigation',

    'HEADER_SEARCH_ALL'                                 => 'Alle',
    'HEADER_SEARCH_CONTAINS'                            => 'Contains',
    'HEADER_SEARCH_EQUAL'                               => "It's equal",
    'HEADER_SEARCH_GREATER_THAN'                        => 'Greather than',
    'HEADER_SEARCH_LESS_THAN'                           => 'Less than',
    'HEADER_SEARCH_ANYTHING'                            => 'Anything',

    'FOOTER_HRM'                                        => 'HRM',
    'FOOTER_PRODUCTS_AND_SERVICES'                      => 'Produkte und Services',
    'FOOTER_FOR_COMPANIES'                              => 'Für Unternehmen',
    'FOOTER_FOLLOW_US'                                  => 'Folgen Sie uns!',

    'FOOTER_HRM_PARTNERSHIP'                            => 'Partnerschaft',
    'FOOTER_HRM_BUSINESS_PAGE'                          => 'Business Page',
    'FOOTER_HRM_BANNER'                                 => 'Bannerwerbung',
    'FOOTER_HRM_PRINT'                                  => 'Printwerbung',
    'FOOTER_HRM_JOB_POSTING'                            => 'Stellenanzeige schalten',
    'FOOTER_HRM_SUBMIT_EVENT'                           => 'Event melden',
    'FOOTER_HRM_INDIVIDUAL_OFFER'                       => 'Individuelle Angebote',
    'FOOTER_HRM_MEDIA_DATA'                             => 'Mediadaten',

    'COMMON_TILL'                                       => 'bis',
    'COMMON_SHOW_ALL'                                   => 'Alle anzeigen',
    'HRM_COMMON_DAYS'                                   => 'Tage',

    'COMMON_PHONE_PREFIX'                               => 'Tel ',
    'COMMON_FAX_PREFIX'                                 => 'Fax ',

    'COMMON_FILTER_CHECK_ALL'                           => 'Alle',

    'January'                                           => 'Januar',
    'February'                                          => 'Februar',
    'March'                                             => 'März',
    'April'                                             => 'April',
    'May'                                               => 'Mai',
    'June'                                              => 'Juni',
    'July'                                              => 'Juli',
    'August'                                            => 'August',
    'September'                                         => 'September',
    'October'                                           => 'Oktober',
    'November'                                          => 'November',
    'December'                                          => 'Dezember',

    'Mon'                                               => 'Mo',
    'Tue'                                               => 'Di',
    'Wed'                                               => 'Mi',
    'Thu'                                               => 'Do',
    'Fri'                                               => 'Fr',
    'Sat'                                               => 'Sa',
    'Sun'                                               => 'So',

    'Monday'                                            => 'Montag',
    'Tuesday'                                           => 'Dienstag',
    'Wednesday'                                         => 'Mittwoch',
    'Thursday'                                          => 'Donnerstag',
    'Friday'                                            => 'Freitag',
    'Saturday'                                          => 'Samstag',
    'Sunday'                                            => 'Sonntag',
];
