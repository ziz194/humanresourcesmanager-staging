<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Entity\Nav;

use QP\Common\Entity\Nav;

/**
 * A model representing a nav folder entity.
 *
 * @author Andy Killat
 */
class Folder extends Node
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a class representation in array format.
     *
     * @param  string $type     determines if the returned array is associative
     *                          if DEFAULT it is associative
     *                          if ELASTIC it is not associative
     * @return array
     */
    public function getArrayCopy($type = Nav::COPY_TYPE_DEFAULT)
    {
        $arrayCopy = [
            'id'       => $this->id,
            'parentId' => $this->parentId,
            'text'     => $this->text,
            'inPublic' => Nav::FOLDER_PUBLIC,
            'icon'     => Nav::FOLDER_ICON,
            'li_attr'  => [
                'data-url'  => $this->url,
                'data-type' => Nav::FOLDER_TYPE,
                'class'     => Nav::FOLDER_CLASS,
            ],
        ];
        $children = $this->children->toArray();
        asort($children);
        if ($type === Nav::COPY_TYPE_DEFAULT) {
            foreach ($children as $child) {
                if ($child instanceof Page) {
                    $arrayCopy['children'][$child->getId()] = $child->getArrayCopy();
                }
            }
            foreach ($children as $child) {
                if ($child instanceof Folder) {
                    $arrayCopy['children'][$child->getId()] = $child->getArrayCopy();
                }
            }
        }
        if ($type === Nav::COPY_TYPE_ELASTIC) {
            foreach ($children as $child) {
                if ($child instanceof Page) {
                    $arrayCopy['children'][] = $child->getArrayCopy($type);
                }
            }
            foreach ($children as $child) {
                if ($child instanceof Folder) {
                    $arrayCopy['children'][] = $child->getArrayCopy($type);
                }
            }
        }
        return $arrayCopy;
    }

    /**
     * Fill the user with array data.
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->id = array_key_exists('id', $data)
            ? $data['id'] : $this->id;
        $this->parentId = array_key_exists('parentId', $data)
            ? $data['parentId'] : $this->parentId;
        $this->text = array_key_exists('text', $data)
            ? $data['text'] : $this->text;

        if(isset($data['li_attr']['data-url'])) {
            $this->url = $data['li_attr']['data-url'];
        }
        if (array_key_exists('children', $data)) {
            foreach ($data['children'] as $pageOrFolder) {
                if(isset($pageOrFolder['li_attr']['data-type']) &&
                    $pageOrFolder['li_attr']['data-type'] === Nav::PAGE_TYPE) {
                    $page = new Page();
                    $page->exchangeArray($pageOrFolder);
                    $this->children->set($pageOrFolder['id'], $page);
                }
                if(isset($pageOrFolder['li_attr']['data-type']) &&
                    $pageOrFolder['li_attr']['data-type'] === Nav::FOLDER_TYPE) {
                    $folder = new Folder();
                    $folder->exchangeArray($pageOrFolder);
                    $this->children->set($pageOrFolder['id'], $folder);
                }
            }
        }
    }
}
