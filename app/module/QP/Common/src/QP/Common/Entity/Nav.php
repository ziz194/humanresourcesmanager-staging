<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Entity;

use QP\Common\Entity\Nav\Page;
use QP\Common\Entity\Nav\Folder;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * A model representing a nav entity.
 *
 * @author Andy Killat
 */
class Nav
{
    const MaxDepth            = 3;

    const PAGE_TYPE           = 'page';
    const PAGE_ICON           = 'material-icons material-icon-description';
    const PAGE_ICON_ROOT      = 'material-icons material-icon-brightness_high';
    const PAGE_CLASS          = 'material-icons';
    const PAGE_CLASS_ROOT     = 'material-icons root-node';

    const FOLDER_TYPE         = 'folder';
    const FOLDER_ICON         = 'material-icons material-icon-folder';
    const FOLDER_CLASS        = 'material-icons';
    const FOLDER_PUBLIC       = false;

    const COPY_TYPE_DEFAULT   = 'default';
    const COPY_TYPE_ELASTIC   = 'elastic';
    const COPY_TYPE_WITH_ROOT = 'withRoot';

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $navId;

    /**
     * Is a collection of pages and folder
     *
     * @var array
     */
    private $nodes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->nodes = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNavId()
    {
        return $this->navId;
    }

    /**
     * They represent the first level nodes in the tree.
     *
     * @return ArrayCollection
     */
    public function getNodes()
    {
        return $this->nodes;
    }

    /**
     * Get a class representation in array format.
     *
     * @param  string $type     determines if the returned array is associative
     *                          if DEFAULT it is associative
     *                          if ELASTIC it is not associative
     * @return array
     */
    public function getArrayCopy($type = self::COPY_TYPE_DEFAULT)
    {
        $arrayCopy['navId'] = $this->navId;
        if ($type === self::COPY_TYPE_DEFAULT) {
            $arrayCopy['id'] = $this->id;
            foreach ($this->nodes as $pageOrFolder) {
                if ($pageOrFolder instanceof Page) {
                    $arrayCopy['nodes'][$pageOrFolder->getId()] = $pageOrFolder->getArrayCopy();
                }
            }
            foreach ($this->nodes as $pageOrFolder) {
                if ($pageOrFolder instanceof Folder) {
                    $arrayCopy['nodes'][$pageOrFolder->getId()] = $pageOrFolder->getArrayCopy();
                }
            }
        }
        if ($type === self::COPY_TYPE_ELASTIC || $type === self::COPY_TYPE_WITH_ROOT) {
            if($type === self::COPY_TYPE_WITH_ROOT) {
                // create a new nav entry for the root
                $root = new Page();
                $rootParams = [
                    'path'  => '/',
                    'title' => 'root',
                ];
                $idAndPath = $this->assembleIdAndPath($rootParams['title'], $rootParams['path']);
                $root->createNewNavPage($idAndPath, $rootParams);
                $arrayCopy['nodes'][0] = $root->getArrayCopy($type);
            }
            foreach ($this->nodes as $pageOrFolder) {
                if ($pageOrFolder instanceof Page) {
                    $arrayCopy['nodes'][] = $pageOrFolder->getArrayCopy(self::COPY_TYPE_ELASTIC);
                }
            }
            foreach ($this->nodes as $pageOrFolder) {
                if ($pageOrFolder instanceof Folder) {
                    $arrayCopy['nodes'][] = $pageOrFolder->getArrayCopy(self::COPY_TYPE_ELASTIC);
                }
            }
        }
        return $arrayCopy;
    }

    /**
     * Fill the Nav with array data.
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->id    = array_key_exists('id', $data) ? $data['id'] : $this->id;
        $this->navId = array_key_exists('navId', $data) ? $data['navId'] : $this->navId;

        if (array_key_exists('nodes', $data)) {
            foreach ($data['nodes'] as $pageOrFolder) {
                if(isset($pageOrFolder['li_attr']['data-type']) &&
                    $pageOrFolder['li_attr']['data-type'] === self::PAGE_TYPE) {
                    $page = new Page();
                    $page->exchangeArray($pageOrFolder);
                    $this->nodes->set($pageOrFolder['id'], $page);
                }
                if(isset($pageOrFolder['li_attr']['data-type']) &&
                    $pageOrFolder['li_attr']['data-type'] === self::FOLDER_TYPE) {
                    $folder = new Folder();
                    $folder->exchangeArray($pageOrFolder);
                    $this->nodes->set($pageOrFolder['id'], $folder);
                }
            }
        }
    }

    /**
     * @param array $validatedData
     */
    public function insertNewNode($validatedData)
    {
        // create a new nav entry for this page
        $navPage = new Page();
        $idAndPath = $this->assembleIdAndPath(
            $validatedData['title'],
            $validatedData['path']
        );
        $navPage->createNewNavPage($idAndPath, $validatedData);

        // add new node to parent
        // TODO: Test that create works with parentId
        $childNodes = $this->getChildren($validatedData['parentId']);
        $childNodes->set($navPage->getId(), $navPage);
    }

    /**
     * @param Page  $currentNavPage
     * @param array $validatedData
     */
    public function moveNode($currentNavPage, $validatedData)
    {
        // remove node from old parent
        $origChildNodes = $this->getChildren($currentNavPage->getParentId());
        $origNode       = $origChildNodes->get($currentNavPage->getId());
        $origChildNodes->remove($currentNavPage->getId());

        // update the new path and parent of this node
        $idAndPath = $this->assembleIdAndPath(
            $validatedData['title'],
            $validatedData['path']
        );
        $origNode->setUrl($idAndPath['path']);
        $origNode->setParentId($validatedData['parentId']);

        // add node to new parent
        $childNodes = $this->getChildren($validatedData['parentId']);
        $childNodes->set($origNode->getId(), $origNode);
    }

    /**
     * @param  array $validatedData
     *
     * @return bool
     */
    public function idExists($id)
    {
        if (is_null($this->getChildNodeByKey($id))) {
            return false;
        }
        return true;
    }

    /**
     * @param  array $validatedData
     *
     * @return bool
     */
    public function isPathDuplicate($validatedData)
    {
        $idAndPath = $this->assembleIdAndPath(
            $validatedData['title'],
            $validatedData['path']
        );
        if (is_null($this->getChildNodeByPath($idAndPath['path']))) {
            return false;
        }
        return true;
    }

    /**
     * @param  string $path
     *
     * @return mixed|null
     */
    public function getChildNodeByPath($path)
    {
        if ($path === '/') {
            $path = $path.'home';
        }

        foreach ($this->nodes as $pageOrFolder) {
            if ($pageOrFolder->getUrl() === $path) {
                return $pageOrFolder;
            } else {
                $subPageOrFolder = $pageOrFolder->getChildNodeByPath($path);
                if (!is_null($subPageOrFolder)) {
                    return $subPageOrFolder;
                }
            }
        }
        return null;
    }

    /**
     * @param  string $key
     * @return mixed|null
     */
    public function getChildNodeByKey($key)
    {
        if($this->nodes->containsKey($key)) {
            return $this->nodes->get($key);
        } else {
            foreach ($this->nodes as $pageOrFolder) {
                $children = $pageOrFolder->getChildNodeByKey($key);
                if($children) {
                    return $children;
                }
            }
        }
        return null;
    }

    /**
     * @param string $parentNodeId
     *
     * @return ArrayCollection
     */
    public function getChildren($nodeId)
    {
        $node = $this->getChildNodeByKey($nodeId);
        // if the node is root
        if (is_null($node) && $nodeId === 'root') {
            return $this->getNodes();
        } else {
            return $node->getChildren();
        }
    }

    /**
     * This function returns the number of levels that are above this
     * node (including the node itself)
     *
     * @return int $depth
     */
    public function getParentDepth($nodeId)
    {
        if($this->nodes->containsKey($nodeId)) {
            return 1;
        } else {
            foreach ($this->nodes as $pageOrFolder) {
                $children = $pageOrFolder->getParentDepth($nodeId);
                if($children) {
                    return (1 + $children);
                }
            }
        }
        return 0;
    }

    /**
     * The id is a representation of the path and the title.
     * The title therefor has to be stripped of all the ugly characters.
     * And here we also take care of the slashes within the path.
     *
     * @param  $title
     * @param  $parentPath
     *
     * @return array
     */
    public function assembleIdAndPath($title, $parentPath) {
        $path = substr($parentPath, 1);
        $path = str_replace('/','_',$path);
        $path = $path.'_';

        $title = mb_strtolower($title);
        $title = str_replace(' ','',$title);
        $title = str_replace('ä','ae',$title);
        $title = str_replace('ö','oe',$title);
        $title = str_replace('ü','ue',$title);
        $title = str_replace('ß','ss',$title);
        $title = preg_replace('/[^A-Za-z0-9]/', 'q', $title);

        $parentPath = $parentPath.'/';

        if($path === '_') {
            $path = '';
            $parentPath = '/';
        }

        $id = strtolower($path.$title);
        while ($this->idExists($id)) {
            $id = $id.'1';
        }
        $idAndPath = [
            'id'   => $id,
            'path' => $parentPath.strtolower($title),
        ];
        return $idAndPath;
    }
}