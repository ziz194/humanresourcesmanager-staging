<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Entity\Nav;

use QP\Common\Entity\Nav;

/**
 * A model representing a nav page entity.
 *
 * @author Andy Killat
 */
class Page extends Node
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a class representation in array format.
     *
     * @param  string $type     determines if the returned array is associative
     *                          if DEFAULT it is associative
     *                          if ELASTIC it is not associative
     * @return array
     */
    public function getArrayCopy($type = Nav::COPY_TYPE_DEFAULT)
    {
        $url   = $this->url;
        $icon  = Nav::PAGE_ICON;
        $class = Nav::PAGE_CLASS;
        if ($type === Nav::COPY_TYPE_WITH_ROOT) {
            $url   = '/';
            $icon  = Nav::PAGE_ICON_ROOT;
            $class = Nav::PAGE_CLASS_ROOT;
        }
        $arrayCopy = [
            'id'       => $this->id,
            'parentId' => $this->parentId,
            'inPublic' => $this->inPublic,
            'text'     => $this->text,
            'icon'     => $icon,
            'li_attr'  => [
                'data-url'  => $url,
                'data-type' => Nav::PAGE_TYPE,
                'class'     => $class,
            ],
        ];
        $children = $this->children->toArray();
        asort($children);
        if ($type === Nav::COPY_TYPE_DEFAULT) {
            foreach ($children as $child) {
                if ($child instanceof Page) {
                    $arrayCopy['children'][$child->getId()] = $child->getArrayCopy();
                }
            }
            foreach ($children as $child) {
                if ($child instanceof Folder) {
                    $arrayCopy['children'][$child->getId()] = $child->getArrayCopy();
                }
            }
        }
        if ($type === Nav::COPY_TYPE_ELASTIC) {
            foreach ($children as $child) {
                if ($child instanceof Page) {
                    $arrayCopy['children'][] = $child->getArrayCopy($type);
                }
            }
            foreach ($children as $child) {
                if ($child instanceof Folder) {
                    $arrayCopy['children'][] = $child->getArrayCopy($type);
                }
            }
        }
        return $arrayCopy;
    }

    /**
     * Fill the user with array data.
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->id = array_key_exists('id', $data)
            ? $data['id'] : $this->id;
        $this->parentId = array_key_exists('parentId', $data)
            ? $data['parentId'] : $this->parentId;
        $this->inPublic = array_key_exists('inPublic', $data)
            ? $data['inPublic'] : $this->inPublic;
        $this->text = array_key_exists('text', $data)
            ? $data['text'] : $this->text;

        if(isset($data['li_attr']['data-url'])) {
            $this->url = $data['li_attr']['data-url'];
        }
        if (array_key_exists('children', $data)) {
            foreach ($data['children'] as $pageOrFolder) {
                if(isset($pageOrFolder['li_attr']['data-type']) &&
                    $pageOrFolder['li_attr']['data-type'] === Nav::PAGE_TYPE) {
                    $page = new Page();
                    $page->exchangeArray($pageOrFolder);
                    $this->children->set($pageOrFolder['id'], $page);
                }
                if(isset($pageOrFolder['li_attr']['data-type']) &&
                    $pageOrFolder['li_attr']['data-type'] === Nav::FOLDER_TYPE) {
                    $folder = new Folder();
                    $folder->exchangeArray($pageOrFolder);
                    $this->children->set($pageOrFolder['id'], $folder);
                }
            }
        }
    }

    /**
     * Create a new pale nav page object with only the basic attributes.
     *
     * @param array $idAndPath      assembled ad and path
     * @param array $validatedData  Data from the create form
     *
     * @return Page
     */
    public function createNewNavPage($idAndPath, $validatedData)
    {
        $parentId = '';
        if(isset($validatedData['parentId'])) {
            $parentId = $validatedData['parentId'];
        }
        $this->exchangeArray([
            'id'       => $idAndPath['id'],
            'parentId' => $parentId,
            'inPublic' => false,
            'text'     => $validatedData['title'],
            'icon'     => Nav::PAGE_ICON,
            'li_attr'  => [
                'data-url'  => $idAndPath['path'],
                'data-type' => Nav::PAGE_TYPE,
                'class'     => Nav::PAGE_CLASS,
            ],
        ]);
        return $this;
    }
}
