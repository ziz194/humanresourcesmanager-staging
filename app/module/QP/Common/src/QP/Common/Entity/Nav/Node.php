<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Entity\Nav;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * A model representing a nav page entity.
 *
 * @author Andy Killat
 */
class Node
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $parentId;

    /**
     * @var bool
     */
    protected $inPublic;

    /**
     * @var string
     */
    protected $text;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var array
     */
    protected $children;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param string $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return string
     */
    public function getInPublic()
    {
        return $this->inPublic;
    }

    /**
     * @param string $inPublic
     */
    public function setInPublic($inPublic)
    {
        $this->inPublic = $inPublic;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
        foreach ($this->getChildren() as $child) {
            $oldUrl      = $child->getUrl();
            $oldUrlArray = explode('/', $oldUrl);
            $child->setUrl($url.'/'.$oldUrlArray[count($oldUrlArray)-1]);
        }
    }

    /**
     * @return string
     */
    public function getParentUrl()
    {
        $pathArray = explode('/',$this->url);
        unset($pathArray[count($pathArray)-1]);
        return implode('/',$pathArray);
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param  $key
     * @return mixed|null
     */
    public function getChildNodeByKey($key)
    {
        if($this->children->containsKey($key)) {
            return $this->children->get($key);
        } else {
            foreach ($this->children as $pageOrFolder) {
                $children = $pageOrFolder->getChildNodeByKey($key);
                if($children) {
                    return $children;
                }
            }
        }
        return null;
    }

    /**
     * @param  string $path
     *
     * @return mixed|null
     */
    public function getChildNodeByPath($path)
    {
        if ($path === '/') {
            $path = $path.'home';
        }
        foreach ($this->children as $pageOrFolder) {
            if ($pageOrFolder->getUrl() === $path) {
                return $pageOrFolder;
            } else {
                $subPageOrFolder = $pageOrFolder->getChildNodeByPath($path);
                if (!is_null($subPageOrFolder)) {
                    return $subPageOrFolder;
                }
            }
        }
        return null;
    }


    /**
     * This function returns the number of levels that are beneath this
     * node (including the node itself)
     *
     * @return int $depth
     */
    public function getChildDepth()
    {
        $depth      = 1;
        $childDepth = 0;
        foreach ($this->children as $child) {
            $i = $child->getChildDepth();
            if ($i > $childDepth) {
                $childDepth = $i;
            }
        }
        return ($depth+$childDepth);
    }

    /**
     * This function returns the number of levels that are above this
     * node (including the node itself)
     *
     * @return int $depth
     */
    public function getParentDepth($nodeId)
    {
        if($this->children->containsKey($nodeId)) {
            return 1;
        } else {
            foreach ($this->children as $pageOrFolder) {
                $children = $pageOrFolder->getParentDepth($nodeId);
                if($children) {
                    return (1 + $children);
                }
            }
        }
        return 0;
    }
}
