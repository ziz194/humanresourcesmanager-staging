<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * A model representing a page.
 *
 * @author Andy Killat
 */
class Page
{

    const DEFAULT_TEMPLATE = '_DEFAULT_';

    /**
     * @var string internal Elastic Search ID
     */
    private $_id;
    /**
     * @var string
     */
    private $id;

    /**
     * @var integer
     */
    private $version;

    /**
     * @var bool
     */
    private $inNav;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $template;

    /**
     * @var string
     */
    private $timings;

    /**
     * @var string
     */
    private $tags;

    /**
     * @var string
     */
    private $settings;

    /**
     * @var string
     */
    private $info;

    /**
     * @var ArrayCollection
     */
    private $templateContent;

    /**
     * @var int
     */
    private $timestamp;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->templateContent = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getESId()
    {
        return $this->_id;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return ArrayCollection
     */
    public function getTemplateContent()
    {
        return $this->templateContent;
    }

    /**
     * @param ArrayCollection $templateContent
     */
    public function setTemplateContent($templateContent)
    {
        $this->templateContent = $templateContent;
    }

    /**
     * Get a class representation in array format.
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $arrayCopy = [
            'id'              => $this->id,
            'title'           => $this->title,
            'inNav'           => $this->inNav,
            'version'         => $this->version,
            'template'        => $this->template,
            'timings'         => $this->timings,
            'tags'            => $this->tags,
            'settings'        => $this->settings,
            'info'            => $this->info,
            'templateContent' => $this->templateContent,
            'timestamp'       => $this->timestamp,
        ];
        return $arrayCopy;
    }

    /**
     * Fill the user with array data.
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->_id = array_key_exists('_id', $data)
            ? $data['_id'] : $this->_id;
        $this->id = array_key_exists('id', $data)
            ? $data['id'] : $this->id;
        $this->version = array_key_exists('version', $data)
            ? $data['version'] : $this->version;
        $this->title = array_key_exists('title', $data)
            ? $data['title'] : $this->title;
        $this->inNav = array_key_exists('inNav', $data)
            ? $data['inNav'] : $this->inNav;
        $this->template = array_key_exists('template', $data)
            ? $data['template'] : $this->template;
        $this->timings = array_key_exists('timings', $data)
            ? $data['timings'] : $this->timings;
        $this->tags = array_key_exists('tags', $data)
            ? $data['tags'] : $this->tags;
        $this->settings = array_key_exists('settings', $data)
            ? $data['settings'] : $this->settings;
        $this->info = array_key_exists('info', $data)
            ? $data['info'] : $this->info;
        $this->templateContent = array_key_exists('templateContent', $data)
            ? $data['templateContent'] : $this->templateContent;
        $this->timestamp = array_key_exists('@timestamp', $data)
            ? $data['@timestamp'] : $this->timestamp;
    }

    /**
     * Create a new pale page object with only the basic attributes.
     *
     * @param array $idAndPath      assembled ad and path
     * @param array $validatedData  Data from the create form
     *
     * @return Page
     */
    public function createNewPage($idAndPath, $validatedData)
    {
        $this->exchangeArray([
            'id'       => $idAndPath['id'],
            'title'    => $validatedData['title'],
            'inNav'    => false,
            'version'  => 1,
            'timings'  => [],
            'tags'     => [],
            'settings' => [],
            'info'     => [],
            'template' => self::DEFAULT_TEMPLATE,
        ]);
        return $this;
    }
}
