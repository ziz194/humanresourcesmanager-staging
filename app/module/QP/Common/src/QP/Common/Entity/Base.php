<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Entity;

/**
 * Class Base
 * @package QP\Common\Entity
 */
abstract class Base
{
    const ARRAY_COPY_TYPE_DEFAULT = 'default';
    const ARRAY_COPY_TYPE_LOG = 'log';
    const ARRAY_COPY_TYPE_ES = 'es';

    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';
    const STATUS_REMOVED = 'removed';

    /**
     * Factory for creating new instance of class
     *
     * @return mixed New instance of current class
     */
    public static function getNewInstance()
    {
        return new static();
    }

    /**
     * Name of the called class
     *
     * @return string
     */
    public static function getEntityName()
    {
        return get_called_class();
    }
}
