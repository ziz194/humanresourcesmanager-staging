<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\View\Helper;

use QP\Common\Service\CryptService;
use Zend\View\Helper\AbstractHelper;

class GetAggList extends AbstractHelper
{
    /**
     * @param  array  $aggList
     * @param  string $aggName
     *
     * @return string
     */
    public function __invoke($aggList, $aggName)
    {
        $dataSet = [];
        //\FirePHP::getInstance(true)->info($aggList);
        if (isset($aggList[$aggName]['buckets'])) {
            foreach ($aggList[$aggName]['buckets'] as $agg) {
                $dataSet[CryptService::makeUrlWorthy($agg['key'])] = $agg['key'];
            }
        }

        return htmlspecialchars(json_encode($dataSet), ENT_QUOTES, 'UTF-8');
    }
}