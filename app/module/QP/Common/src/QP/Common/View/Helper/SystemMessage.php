<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\View\Helper;

use Zend\View\Helper\AbstractHelper;

class SystemMessage extends AbstractHelper
{
    /**
     * css classed to determine the type of the system message
     */
    const STATUS_GOOD = 'system_message_good';
    const STATUS_WARN = 'system_message_warn';
    const STATUS_FAIL = 'system_message_fail';

    ////////////////////////////////////////////////////////////////////////////
    // public function __invoke() {{{
    ////////////////////////////////////////////////////////////////////////////
    /**
     * @return  string
     */
    public function __invoke($messages)
    {
        $markup = '';

        foreach ($messages as $message) {
            $markup .= '<div class="system_message ' . $message['typ'] . '" data-module="SystemMessage">'
                    .  '<p>' . $this->getView()->translate($message['msg']) . '</p>'
                    .  '</div>';
        }

        return $markup;
    }
    // }}} end of '__invoke()' method
    ////////////////////////////////////////////////////////////////////////////
}
// end of 'SystemMessage' class
////////////////////////////////////////////////////////////////////////////////