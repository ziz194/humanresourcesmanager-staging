<?php
/**
 * Quadriga Plattform WEB
 *
 * Takes the validators messages of the form and
 * displays them in my custom Layout.
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\View\Helper;

use Zend\View\Helper\AbstractHelper;


class FormErrors extends AbstractHelper
{
    /**
     * See class description :)
     *
     * @param   array|string $messages
     * @param   \Zend\Form\Form $form
     *
     * @return  string
     */
    public function __invoke($messages, $form = null)
    {
        $markup = '<div class="form-error">';

        // if the message is only a string there is no need for a form
        // object to get additional info to the message
        if (!is_array($messages)) {
            $markup .= '<p class="single_error_message">' . $this->getView()->translate($messages) . '</p>';
        } else {
            // if no form is given just display the messages within the array
            if (is_null($form)) {
                $msg_count = count($messages);

                for ($i = 0; $i < $msg_count; $i++) {
                    $markup .= '<p class="multiple_error_messages">' . $this->getView()->translate($messages[$i]) . '</p>';
                }
            } else {
                // start table
                $markup .= '<table class="form_element_errors">';
                // to remember last element
                $last_element = '';

                // run over all elements with messages
                foreach ($messages as $element => $el_messages) {
                    // run over all messages to one element
                    foreach ($el_messages as $messages) {
                        // if the element stays the same print an empty first td
                        if ($last_element === $element) {
                            $markup .= '<tr>'
                                    .  '<td></td>'
                                    .  '<td>' . $messages . '</td>'
                                    .  '</tr>';
                        } else {
                            $markup .= '<tr class="form_errors_new_row">'
                                    .  '<td class="form_errors_element_name">'
                                    .  $this->getView()->translate($form->get($element)->getLabel()) . ':</td>'
                                    .  '<td class="form_errors_message">'
                                    .  $messages . '</td>'
                                    .  '</tr>';
                        }

                        // remember last element
                        $last_element = $element;
                    }
                }
                // finish the table
                $markup .= '</table>';
            }
        }

        return $markup . '</div>';
    }
    // }}} end of '__invoke()' method
    ////////////////////////////////////////////////////////////////////////////
}
// end of 'FormErrors' class
////////////////////////////////////////////////////////////////////////////////