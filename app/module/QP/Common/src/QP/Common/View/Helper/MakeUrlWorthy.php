<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\View\Helper;

use QP\Common\Service\CryptService;
use Zend\View\Helper\AbstractHelper;

class MakeUrlWorthy extends AbstractHelper
{
    /**
     * @param  string $str
     * @return string
     */
    public function __invoke($str)
    {
        return CryptService::makeUrlWorthy($str);
    }
}