<?php
/**
 * Quadriga Plattform WEB
 *
 * Override
 *
 * Render the captcha
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\View\Helper\Custom\Captcha;

use Zend\Form\View\Helper\Captcha\AbstractWord;

use Zend\Form\Exception;
use Zend\Form\ElementInterface;
use QP\Common\View\Helper\Custom\Captcha\CustomCaptcha as CaptchaAdapter;

class ViewHelperCaptcha extends AbstractWord
{
    /**
     * @param  ElementInterface $element
     * @throws Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element)
    {
        // check if the given element is instance of the custom adapter
        // to make sure the custom markup will be loaded
        $captcha = $element->getCaptcha();
        if (is_null($captcha) || !$captcha instanceof CaptchaAdapter) {
            throw new Exception\DomainException(sprintf(
                '%s requires that the element has a "captcha" attribute of type Zend\Captcha\Image; none found',
                __METHOD__
            ));
        }

        // create the image and the hash code
        $captcha->setWordlen(5);
        $captcha->generate();

        // get the attributes set within the form
        $imgAttributes = array(
            'width'  => $captcha->getWidth(),
            'height' => $captcha->getHeight(),
            'alt'    => $captcha->getImgAlt(),
            'src'    => $captcha->getImgUrl() . $captcha->getId() . $captcha->getSuffix(),
        );

        // create image tag
        $closingBracket = $this->getInlineClosingBracket();
        $img = sprintf('<img %s%s', $this->createAttributesString($imgAttributes), $closingBracket);

        // get markup options
        $position     = $this->getCaptchaPosition();
        $separator    = $this->getSeparator();
        $captchaInput = $this->renderCaptchaInputs($element);

        // set the pattern
        $pattern = '<div class="captcha_image">%s</div>%s<div class="captcha_input">%s<div class="reload-captcha glyphicon glyphicon-refresh"></div></div>';

        // depending on the position fill the pattern
        if ($position === self::CAPTCHA_PREPEND) {
            return sprintf($pattern, $captchaInput, $separator, $img);
        }

        return sprintf($pattern, $img, $separator, $captchaInput);
    }
}