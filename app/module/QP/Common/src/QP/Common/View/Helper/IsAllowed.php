<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Permissions\Acl\AclInterface;
use Zend\Permissions\Acl\Role\RoleInterface;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Permissions\Acl\Resource\ResourceInterface;

/**
 * Class IsAllowed
 * @package View\Helper
 */
class IsAllowed extends AbstractHelper
{
    /**
     * @var AclInterface
     */
    protected $acl;

    /**
     * @var RoleInterface
     */
    protected $role;

    /**
     * @var AclInterface
     */
    protected static $defaultAcl;

    /**
     * @var RoleInterface
     */
    protected static $defaultRole;

    /**
     * Create a view helper instance
     *
     * @param string $resource
     * @param string $privilege
     *
     * @return bool|IsAllowed
     */
    public function __invoke($resource = null, $privilege = null)
    {
        if (is_null($resource)) {
            return $this;
        }

        return $this->isAllowed($resource, $privilege);
    }

    /**
     * Check if the resource and privilege is available for the user.
     *
     * @param string|ResourceInterface $resource
     * @param string $privilege
     *
     * @return bool
     * @throws \RuntimeException
     */
    public function isAllowed($resource, $privilege = null)
    {
        $acl = $this->getAcl();
        if (!$acl instanceof AclInterface) {
            throw new \RuntimeException('No ACL provided');
        } elseif (!$resource instanceof ResourceInterface) {
            $resource = new GenericResource($resource);
        }

        return $acl->isAllowed($this->getRole(), $resource, $privilege);
    }

    /**
     * Set the default acl
     *
     * @param \Zend\Permissions\Acl\AclInterface|null $acl
     */
    public static function setDefaultAcl(AclInterface $acl = null)
    {
        self::$defaultAcl = $acl;
    }

    /**
     * Get the acl
     *
     * @return \Zend\Permissions\Acl\AclInterface
     */
    protected function getAcl()
    {
        if ($this->acl instanceof AclInterface) {
            return $this->acl;
        }

        return self::$defaultAcl;
    }

    /**
     * Set the acl
     *
     * @param \Zend\Permissions\Acl\AclInterface $acl
     * @return $this
     */
    public function setAcl(AclInterface $acl = null)
    {
        $this->acl = $acl;
        return $this;
    }


    /**
     * Set the default role
     *
     * @param \Zend\Permissions\Acl\Role\RoleInterface $role
     */
    public static function setDefaultRole(RoleInterface $role = null)
    {
        self::$defaultRole = $role;
    }

    /**
     * Get the role
     *
     * @return \Zend\Permissions\Acl\Role\RoleInterface
     */
    protected function getRole()
    {
        if ($this->role instanceof RoleInterface) {
            return $this->role;
        }

        return self::$defaultRole;
    }

    /**
     * Set the role
     *
     * @param \Zend\Permissions\Acl\Role\RoleInterface $role
     * @return $this
     */
    public function setRole(RoleInterface $role = null)
    {
        $this->role = $role;
        return $this;
    }
}
