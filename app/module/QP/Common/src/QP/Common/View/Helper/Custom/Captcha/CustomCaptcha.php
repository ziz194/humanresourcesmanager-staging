<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace  QP\Common\View\Helper\Custom\Captcha;

use Zend\Captcha\Image as CaptchaImage;

class CustomCaptcha extends CaptchaImage
{
    /**
     * !!! Override this function to point to the new helper.
     * Get helper name used to render captcha
     *
     * @return string
     */
    public function getHelperName()
    {
        return 'myCaptcha';
    }


}