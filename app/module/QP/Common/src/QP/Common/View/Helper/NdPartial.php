<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class NdPartial extends AbstractHelper implements ServiceLocatorAwareInterface
{
    private $serviceLocator;

    /*
    * @return \Zend\ServiceManager\ServiceLocatorInterface
    */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    /**
     * @param  string $path
     * @param  array  $args
     *
     * @return void
     */
    public function __invoke($path, $args = [])
    {
        $config = $this->getServiceLocator()->getServiceLocator()->get('Config');

        if ($config['debugging']['showHTMLCommentsOfStartingAndEndingPartials']) {
            echo '<!-- Start: ' . $path . '-->';
        }
        echo $this->getView()->partial($path, $args);
        if ($config['debugging']['showHTMLCommentsOfStartingAndEndingPartials']) {
            echo '<!-- End: ' . $path . '-->';
        }
    }
}