<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Exception;

/**
 * Exception permission denied
 *
 * Class PermissionDeniedException
 * @package QP\Common\Exception
 */
class PermissionDeniedException extends \Exception
{

}
