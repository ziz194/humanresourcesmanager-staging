<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Exception;

/**
 * Exception not found
 *
 * Class NotFoundException
 * @package QP\Common\Exception
 */
class NotFoundException extends \Exception
{

}
