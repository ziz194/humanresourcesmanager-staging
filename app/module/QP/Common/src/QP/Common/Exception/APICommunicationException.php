<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Exception;

/**
 * Exception not found
 *
 * Class NotFoundException
 * @package QP\Common\Exception
 */
class APICommunicationException extends \Exception
{
    const NO_RESPONSE_CODE = "101";

    function __construct($code, \Exception $previous)
    {
        $message = $this->setMessage($code);


        parent::__construct($message, $code, $previous);
    }

    function setMessage( $code ) {
        switch($code) {
            case self::NO_RESPONSE_CODE:
                return "API does not response.";

        }
        return "TODO: No message defined!!!!";
    }
}
