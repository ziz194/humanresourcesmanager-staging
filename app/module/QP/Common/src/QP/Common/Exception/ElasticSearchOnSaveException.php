<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Exception;

/**
 * Exception class for Elastic Search on save event
 *
 * Class ElasticSearchOnSaveException
 * @package QP\Common\Exception
 */
class ElasticSearchOnSaveException extends \Exception
{
    /**
     * @var string
     */
    private $data;

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * ElasticSearchOnSaveException constructor.
     * @param string $data
     */
    public function __construct($data)
    {
        $this->data = $data;

        parent::__construct();
    }
}
