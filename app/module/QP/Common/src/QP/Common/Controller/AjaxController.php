<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Controller;

/**
 * Class AjaxController
 * @package QP\Common\Controller
 */
class AjaxController extends AbstractController
{

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function followAction()
    {
        $this->layout('layout/empty');
        $viewLogic = $this->getViewLogicFactory()->build(
            'Common\ViewLogic\Ajax\FollowLogic',
            [
                $this->getRequest(),
                $this->getServiceLocator()->get('Config'),
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function bookmarkAction()
    {
        $this->layout('layout/empty');
        $viewLogic = $this->getViewLogicFactory()->build(
            'Common\ViewLogic\Ajax\BookmarkLogic',
            [
                $this->getRequest(),
                $this->getServiceLocator()->get('Config'),
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }
}
