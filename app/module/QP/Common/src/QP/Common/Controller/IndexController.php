<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Controller;

/**
 * Class IndexController
 * @package QP\Common\Controller
 */
class IndexController extends AbstractController
{
    /**
     * Default error action.
     */
    public function errorAction()
    {

    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function homeAction()
    {
        $viewLogic = $this->getViewLogicFactory()->build(
            'Common\ViewLogic\Index\HomeLogic',
            []
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function lookingGlassAction()
    {
        $this->layout('layout/empty');
        $viewLogic = $this->getViewLogicFactory()->build(
            'Common\ViewLogic\Index\LookingGlassLogic',
            [
                $this->params()->fromRoute('scope'),
                $this->params()->fromRoute('date'),
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }
}
