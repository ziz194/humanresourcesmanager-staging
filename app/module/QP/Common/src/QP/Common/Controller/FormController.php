<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Controller;

/**
 * Class FormController
 * @package QP\Common\Controller
 */
class FormController extends AbstractController
{

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function newsletterAction()
    {
        $this->layout('layout/empty');
        $viewLogic = $this->getViewLogicFactory()->build(
            'Common\ViewLogic\Form\NewsletterLogic',
            [
                $this->getRequest(),
                $this->getServiceLocator()->get('Config'),
                $this->redirect(),
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function kontaktAction()
    {
        $this->layout('layout/empty');
        $translator = $this->getServiceLocator()->get('translator');
        $viewLogic  = $this->getViewLogicFactory()->build(
            'Common\ViewLogic\Form\KontaktLogic',
            [
                $this->getRequest(),
                $this->getServiceLocator()->get('Config'),
                $this->redirect(),
                $translator,
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function eventSubmitAction()
    {
        $this->layout('layout/empty');
        $translator = $this->getServiceLocator()->get('translator');
        $viewLogic  = $this->getViewLogicFactory()->build(
            'Common\ViewLogic\Form\EventSubmitLogic',
            [
                $this->getRequest(),
                $this->getServiceLocator()->get('Config'),
                $this->redirect(),
                $translator,
            ]
        );
        $response = $viewLogic->getResponse();

        return $this->processResponse($response);
    }
}
