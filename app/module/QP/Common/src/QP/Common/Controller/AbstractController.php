<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */

namespace QP\Common\Controller;

use Zend\View\Model;
use Zend\Mvc\MvcEvent;
use QP\Common\ViewLogic\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\PhpEnvironment\Response as ImageResponse;

/**
 * Class AbstractController
 * @package QP\Common\Controller
 */
abstract class AbstractController extends AbstractActionController
{

    /**
     */
    public function onDispatch(MvcEvent $e)
    {
        // take a given flash message and pipe it through to the layout
        if ($this->flashMessenger()->hasMessages()) {
            $e->getViewModel()->setVariable('sysMsg', $this->flashMessenger()->getMessages());
        }

        parent::onDispatch($e);
    }

    /**
     * Get the entity manager
     *
     * @return array|\Doctrine\ORM\EntityManager|object
     */
    public function getEntityManager()
    {
        return $this->getServiceLocator()->get('EntityManager');
    }

    /**
     * @return  mixed
     */
    public function getBaseUrl()
    {
        $serverUrl = $this->getServiceLocator()->get('ViewHelperManager')->get('ServerUrl');
        return $serverUrl->__invoke();
    }

    /**
     * Handle the response when an action has finished
     *
     * @param \QP\Common\ViewLogic\Response\AbstractResponse $response
     *
     * @return \Zend\Http\Response|\Zend\View\Model\ViewModel
     */
    public function processResponse($response)
    {
        $params = [];
        if (method_exists($response, 'getParams')) {
            $params = $response->getParams();
        }

        if (isset($params['httpStatus'])) {
            $this->getResponse()->setStatusCode($params['httpStatus']);
        }

        if ($response instanceof Response\TemplateResponse) {
            $viewModel = new Model\ViewModel($params);
            $viewModel->setTemplate($response->getTemplate());

            return $viewModel;

        } elseif ($response instanceof Response\JsonResponse) {
            $this->getServiceLocator()->get('Application')->getEventManager()->attach(
                \Zend\Mvc\MvcEvent::EVENT_RENDER,
                function ($event) {
                    $event->getResponse()->getHeaders()->addHeaderLine('Access-Control-Allow-Origin', '*');
                },
                -10000
            );
            return new Model\JsonModel($params);

        } elseif ($response instanceof Response\RedirectResponse) {
            $url = $this->url()->fromRoute($response->getRoute(), $params) . $response->getHashtag();
            return $this->redirect()->toUrl($url);

        } elseif ($response instanceof Response\HttpResponse) {
            return $response;
        } elseif ($response instanceof ImageResponse) {
            return $response;
        } elseif ($response instanceof Response\ConsoleResponse) {
            return $response->printMessage();
        }

        return $this->redirect()->toUrl($this->url()->fromRoute('home'));
    }

    /**
     * Return the ViewLogicFactory
     *
     * @return \QP\Common\ViewLogic\ViewLogicFactory
     */
    public function getViewLogicFactory()
    {
        /** @var \QP\Common\ViewLogic\ViewLogicFactory $vlf */
        $vlf = $this->getServiceLocator()->get('ViewLogicFactory');
        $vlf->setLayout($this->layout());
        return $vlf;
    }
}
