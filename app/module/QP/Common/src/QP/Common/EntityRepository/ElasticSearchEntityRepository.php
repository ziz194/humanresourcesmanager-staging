<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\EntityRepository;

/**
 * Class ElasticSearchEntityRepository
 * @package QP\Common\EntityRepository
 */
abstract class ElasticSearchEntityRepository
{
    const DOC_NOT_FOUND = 'DOC_NOT_FOUND';
    const DOC_NOT_SAVED = 'DOC_NOT_SAVED';

    /**
     * @var \QP\Common\Service\ApiClientService
     */
    private $apiClient;

    /**
     * @var \QP\Common\Service\ErrorLogger
     */
    private $errorLogger;

    /**
     * ElasticSearchEntityRepository constructor.
     *
     * @param \QP\Common\Service\ApiClientService $apiClient
     * @param \QP\Common\Service\ErrorLogger      $errorLogger
     */
    public function __construct($apiClient, $errorLogger)
    {
        $this->apiClient   = $apiClient;
        $this->errorLogger = $errorLogger;
    }

    /**
     * Get the api client.
     *
     * @return \QP\Common\Service\ApiClientService
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * @return \QP\Common\Service\ErrorLogger
     */
    public function getErrorLogger() {
        return $this->errorLogger;
    }
}
