<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\EntityRepository;

use QP\Common\Entity\Nav;
use QP\Common\Service\ApiClientService;
use QP\Common\Exception\APICommunicationException;

/**
 * A repository for capsulating custom nav queries.
 *
 * @author Andy Killat
 */
class NavRepository extends ElasticSearchEntityRepository
{
    /**
     * Read a nav by Id.
     *
     * @param  string $navId
     *
     * @return Object|\QP\Common\Entity\Nav A single nav.
     * @throws \Exception
     */
    public function readOne($navId)
    {
        try {
            /** @var $nav \QP\Common\Entity\Nav */
            $nav = $this->getApiClient()->get(ApiClientService::ENTITY_NAV, $navId);

            //\FirePHP::getInstance(true)->info( $nav );
            if (!isset($nav['success']) || $nav['success'] === false) {
                $e = new \Exception($nav['data'][0]);
                $this->getErrorLogger()->logException($e);
                throw $e;
            }
            $navObj = new Nav();
            $navObj->exchangeArray($nav['data']);
            return $navObj;
        } catch (\Exception $e) {
            $e = new APICommunicationException(APICommunicationException::NO_RESPONSE_CODE, $e);
            $this->getErrorLogger()->logException($e);
            throw $e;
        }
    }

    /**
     * Update a nav by Id.
     *
     * @param  string $navId
     * @param  array  $updatedNav
     *
     * @return array
     * @throws \Exception
     */
    public function update($navId, $updatedNav)
    {
        $returnNav = $this->getApiClient()->update(ApiClientService::ENTITY_NAV, $navId, $updatedNav);
        if (!isset($returnNav['success']) || $returnNav['success'] === false) {
            $e = new \Exception($returnNav['data'][0]);
            $this->getErrorLogger()->logException($e);
            throw $e;
        }
        return $returnNav;
    }
}

