<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\EntityRepository;

/**
 * A repository for capsuling custom content queries.
 *
 * @author Andy Killat
 */
class ContentRepository extends ElasticSearchEntityRepository
{

    /**
     * Read mixed content by query.
     *
     * @param  string       $entity
     * @param  string|array $filter
     *
     * @return array
     * @throws \Exception
     */
    public function query($entity, $filter = [])
    {
        $contents = $this->getApiClient()->query($entity, $filter);

        if (!isset($contents['success']) || $contents['success'] === false) {
            $msg = 'Data could not be fetched. Bad JSON format or no connection at all ('.$entity.print_r($filter, true).').';
            if(isset($contents['data'])) {
                $msg = print_r($contents['data'], true);
            }
            $e = new \Exception($msg."\n\n".print_r($contents, true));
            $this->getErrorLogger()->logException($e);
            throw $e;
        }
        return $contents;
    }

    public function getInitCount()
    {
        $initCount = $this->getApiClient()->getInitCount();
        if (isset($initCount[0])) {
            return $initCount[0];
        }
        return 0;
    }
}
