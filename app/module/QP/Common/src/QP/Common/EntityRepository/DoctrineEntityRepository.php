<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\EntityRepository;

use QP\Common\Entity\Base;

/**
 * Class DoctrineEntityRepository
 * @package QP\Common\EntityRepository
 */
abstract class DoctrineEntityRepository extends \Doctrine\ORM\EntityRepository
{

    const QUERY_TYPE_COUNT = 'count';
    const QUERY_TYPE_SELECT = 'select';

    const OPERATOR_AND = 'AND';
    const OPERATOR_OR = 'OR';

    /**
     * Creates or updates an object in the database
     *
     * @param \QP\Common\Entity\Base $object
     */
    public function save(Base $object)
    {
        $this->getEntityManager()->persist($object);
    }

    /**
     * Deletes an object from database
     *
     * @param \QP\Common\Entity\Base $object
     */
    public function remove(Base $object)
    {
        $this->getEntityManager()->remove($object);
    }

    /**
     * Reads an  object from database by ID
     *
     * @param integer $id The ID.
     * @return \QP\Common\Entity\Base
     */
    public function readOneById($id)
    {
        $conditions = [
            'id' => $id
        ];

        $obj = $this->findOneBy($conditions);

        return $obj;
    }

    /**
     * Reads all objects from database
     *
     * @return
     */
    public function readAll()
    {
        return $this->findAll();
    }
}
