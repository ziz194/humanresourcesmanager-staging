<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\EntityRepository;

use QP\Common\Service\ApiClientService;

/**
 * A repository for capsulating custom page queries.
 *
 * @author Andy Killat
 */
class JobRepository extends ElasticSearchEntityRepository
{
    /**
     * Read one job by id.
     *
     * @param  string $jobId
     *
     * @return array
     * @throws \Exception
     */
    public function readOne($jobId)
    {
        $job = $this->getApiClient()->get(ApiClientService::ENTITY_JOB, $jobId);

        if (!isset($job['success']) || $job['success'] === false) {
            $e = new \Exception($job['data'][0]);
            $this->getErrorLogger()->logException($e);
            throw $e;
        }

        return $job;
    }

    /**
     * Read all jobs.
     *
     * @return array
     * @throws \Exception
     */
    public function readAll($filter = [])
    {
        $jobs = $this->getApiClient()->listing(ApiClientService::ENTITY_JOB, $filter);

        if ($jobs['success'] === false) {
            $e = new \Exception($jobs['data'][0]);
            $this->getErrorLogger()->logException($e);
            throw $e;
        }
        return $jobs;
    }

    /**
     * Searching for jobs.
     *
     * @return array
     * @throws \Exception
     */
    public function search($filter = [])
    {
        $jobs = $this->getApiClient()->search(ApiClientService::ENTITY_JOB, $filter);

        if ($jobs['success'] === false) {
            $e = new \Exception($jobs['data'][0]);
            $this->getErrorLogger()->logException($e);
            throw $e;
        }
        return $jobs;
    }
}
