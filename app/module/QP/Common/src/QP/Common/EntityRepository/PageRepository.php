<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\EntityRepository;

use QP\Common\Entity\Page;
use QP\Common\Service\ApiClientService;

/**
 * A repository for capsulating custom page queries.
 *
 * @author Andy Killat
 */
class PageRepository extends ElasticSearchEntityRepository
{
    /**
     * Read a page by ES Id.
     *
     * @param  string $pageEsId
     *
     * @return \QP\Common\Entity\Page A single page.
     * @throws \Exception
     */
    public function readOne($pageEsId)
    {
        $page = $this->getApiClient()->get(ApiClientService::ENTITY_PAGE, $pageEsId);

        if (!isset($page['success']) || $page['success'] === false) {
            $e = new \Exception($page['data'][0]);
            $this->getErrorLogger()->logException($e);
            throw $e;
        }
        $pageObj = new Page();
        $pageObj->exchangeArray($page['data']);
        return $pageObj;
    }
}
