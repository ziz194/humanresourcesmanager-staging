<?php
/**
 * Quadriga Plattform Webfrontend
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class FormBaseAbstract
 * @package QP\Common\Form
 */
abstract class FormBaseAbstract extends Form
{
    /**
     * @var \QP\User\Service\AclService
     */
    protected $aclService;

    /**
     * @var \Zend\Permissions\Acl\Role\GenericRole $aclRole
     */
    protected $aclRole;

    /**
     * Set the acl service.
     *
     * @param \QP\User\Service\AclService $aclService
     *
     * @return \QP\Common\Form\FormBaseAbstract
     */
    public function setAclService($aclService)
    {
        $this->aclService = $aclService;
        return $this;
    }

    /**
     * Get the acl service.
     *
     * @return \QP\User\Service\AclService $aclService
     */
    public function getAclService()
    {
        return $this->aclService;
    }

    /**
     * Set the acl role.
     *
     * @param \Zend\Permissions\Acl\Role\GenericRole $aclRole
     *
     * @return \QP\Common\Form\FormBaseAbstract
     */
    public function setAclRole($aclRole)
    {
        $this->aclRole = $aclRole;
        return $this;
    }

    /**
     * Process the acl permissions.
     *
     * @return \QP\Common\Form\FormBaseAbstract
     */
    public function processAcl()
    {
        if ($this->aclService) {
            foreach ($this->getElements() as $element) {
                $this->processAclOnElement($element);
            }

            // process acl on fieldset element as well
            foreach ($this->getFieldsets() as $fieldset) {
                foreach ($fieldset->getTargetElement()->getElements() as $element) {
                    $this->processAclOnElement($element);
                }
            }
        }
        return $this;
    }

    /**
     * Process acl permissions on a single element.
     *
     * @param $element
     */
    private function processAclOnElement($element)
    {
        if (isset($element->getOptions()['acl'])) {
            $allowed = $this->aclService->isAllowed(
                $this->aclRole,
                $element->getOptions()['acl']['resource'],
                $element->getOptions()['acl']['privilege']
            );
            if (!$allowed) {
                if (array_key_exists('on_denial', $element->getOptions()['acl']) &&
                    $element->getOptions()['acl']['on_denial'] == 'disable'
                ) {
                    $element->setAttribute('disabled', 'disabled');
                } else {
                    $this->remove($element->getName());
                }
            }
        }
    }

    /**
     * Remove filters from the input filter that have no fields in form
     *
     * @param \Zend\InputFilter\InputFilterInterface $inputFilter
     *
     * @return \Zend\Form\Form
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        if ($this->aclService) {
            foreach ($inputFilter->getInputs() as $filter) {
                if (method_exists($filter, 'getName')) {
                    if (!$this->has($filter->getName())) {
                        $inputFilter->remove($filter->getName());
                    }
                }
            }
        }

        return parent::setInputFilter($inputFilter);
    }

    /**
     * Bind data to the form and keep previous data if acl does not allow chaning fields.
     *
     * @param array|\ArrayAccess|\Traversable $data
     *
     * @return \QP\Common\Form\FormBaseAbstract
     */
    public function setData($data)
    {
        if ($this->aclService) {
            foreach ($this->getElements() as $element) {
                $data = $this->processDataOnElement(
                    $element,
                    $data,
                    $this->get($element->getName())->getValue()
                );
            }
        }

        parent::setData($data);

        return $this;
    }

    /**
     * Process data on a single element.
     *
     * @param $element
     * @param $data
     * @param $prevValue
     *
     */
    private function processDataOnElement($element, $data, $prevValue)
    {
        if (isset($element->getOptions()['acl'])) {
            $allowed = $this->aclService->isAllowed(
                $this->aclRole,
                $element->getOptions()['acl']['resource'],
                $element->getOptions()['acl']['privilege']
            );
            if (!$allowed) {
                $data[$element->getName()] = $prevValue;
            }
        }

        return $data;
    }
}
