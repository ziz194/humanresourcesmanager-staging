<?php
/**
 * Quadriga Plattform Webfrontend
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Form;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ViewLogicFactory
 * @package Sfp\Common\ViewLogic
 */
class FormFactory implements ServiceLocatorAwareInterface
{
    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    protected $serviceLocator;

    /**
     * Get the service locator
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * Set the service locator
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Create an instance of a form class
     *
     * @param string $formName
     * @param string $params
     *
     * @return Form
     */
    public function build($formName, $params)
    {
        $reflect = new \ReflectionClass('QP\\' . $formName);

        /** @var \QP\Common\Form\FormBaseAbstract $instance */
        $instance = $reflect->newInstanceArgs($params);
        $instance->setAclService($this->getServiceLocator()->get('AclService'));
        $instance->setAclRole(
            $instance->getAclService()->createRole(
                $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService')
            )
        );
        $instance->processAcl();
        if (!$instance->hasAttribute('action')) {
            $instance->setAttribute('action', $_SERVER['REQUEST_URI']);
        }
        return $instance;
    }
}
