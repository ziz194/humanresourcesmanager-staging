<?php
/**
 * Quadriga Plattform Webfrontend
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Service;

use Zend\Http\Client;
use Zend\Http\Headers;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;

/**
 * API Client
 *
 * Class ApiClientService
 * @package QP\Common\Service
 */
class ApiClientService
{
    Const ENTITY_EVENT    = 'event';
    Const ENTITY_JOB      = 'job';
    Const ENTITY_LOG      = 'log';
    Const ENTITY_NAV      = 'nav';
    Const ENTITY_PAGE     = 'page';
    Const ENTITY_TEMPLATE = 'template';
    Const ENTITY_CONTENT  = 'content';
    Const ENTITY_PERSON   = 'person';

    /**
     * @var string
     */
    private $apiUrl;

    /**
     * @var \Zend\Http\Request
     */
    private $request;

    /**
     * @var \Zend\Http\Client
     */
    private $client;

    /**
     * Return an API Client instance.
     *
     * @param  array $services
     * @return ApiClientService
     */
    public function __construct($services)
    {
        $config        = $services->get('Config');
        $this->apiUrl  = $config['api']['connection'];
        $this->client  = new Client(null, [
            'maxredirects' => 0,
//            'timeout'      => 30
        ]);
        $this->request = new Request();
        $this->request->getHeaders()->addHeaders(
            ['Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8']
        );

        return $this;
    }

    /**
     * @param  string $entity
     * @param  string $id
     *
     * @return array
     */
    public function get($entity, $id)
    {
        $this->request->setUri($this->apiUrl.'/'.$entity.'/'.$id);
        $this->request->setMethod('GET');
        $response = $this->client->dispatch($this->request);

        return json_decode($response->getBody(), true);
    }

    /**
     * @param  string $entity
     * @param  array  $data
     *
     * @return mixed
     */
    public function create($entity, $data)
    {
        $this->request->setUri($this->apiUrl.'/'.$entity);
        $this->request->setMethod('POST');
        $this->request->setPost(new Parameters($data));
        $response = $this->client->dispatch($this->request);
        return json_decode($response->getBody(), true);
    }

    /**
     * @param  string $entity
     * @param  string $id
     * @param  array  $data
     *
     * @return mixed
     */
    public function update($entity, $id, $data)
    {
        $this->request->setUri($this->apiUrl.'/'.$entity.'/'.$id);
        $this->request->setMethod('PUT');
        $headers = new Headers();
        $headers->addHeaderLine('Content-Type', 'application/json');
        $this->request->setHeaders($headers);
        $this->request->setContent(json_encode($data));
        $response = $this->client->dispatch($this->request);

        return json_decode($response->getBody(), true);
    }

    /**
     * @param  string $entity
     * @param  string $id
     *
     * @return array
     */
    public function delete($entity, $id)
    {
        $this->request->setUri($this->apiUrl.'/'.$entity.'/'.$id);
        $this->request->setMethod('DELETE');
        $response = $this->client->dispatch($this->request);

        return json_decode($response->getBody(), true);
    }


    // -------------------------------------------------- not rest functions ---
    /**
     * @param  string $entity
     * @param  array  $filter
     *
     * @return array
     */
    public function search($entity, $filter = [])
    {
        $url = $this->apiUrl.'/search/'.$entity;
        if (count($filter)) {
            $params = '';
            foreach ($filter as $key => $value) {
                $params .= urlencode($key).'='.urlencode($value).'&';
            }
            $url .= '?'.substr($params, 0, -1);
        }

        $this->request->setUri($url);
        $this->request->setMethod('GET');
        $response = $this->client->dispatch($this->request);

        return json_decode($response->getBody(), true);
    }

    /**
     * @param  string $entity
     * @param  array  $filter
     *
     * @return array
     */
    public function listing($entity, $filter = [])
    {
        $url = $this->apiUrl.'/'.$entity;
        if (count($filter)) {
            $params = '';
            foreach ($filter as $key => $value) {
                $params .= urlencode($key).'='.urlencode($value).'&';
            }
            $url .= '?'.substr($params, 0, -1);
        }

        $this->request->setUri($url);
        $this->request->setMethod('GET');
        $response = $this->client->dispatch($this->request);

        return json_decode($response->getBody(), true);
    }

    /**
     * @param  string $entity
     * @param  string $query
     *
     * @return array
     */
    public function query($entity, $query = '')
    {
        $url = $this->apiUrl.'/'.$entity.$query;
        $this->request->setUri($url);
        $this->request->setMethod('GET');
        $response = $this->client->dispatch($this->request);

        return json_decode($response->getBody(), true);
    }

    // ---------------------------------------------- pure service functions ---
    /**
     * @param  array $data
     *
     * @return mixed
     */
    public function mail($data)
    {
        $this->request->setUri($this->apiUrl.'/mail');
        $this->request->setMethod('POST');
        $this->request->setPost(new Parameters($data));
        $response = $this->client->dispatch($this->request);
        return json_decode($response->getBody(), true);
    }

    /**
     * @param  array $data
     *
     * @return mixed
     */
    public function newsletter($data)
    {
        $this->request->setUri($this->apiUrl.'/newsletter');
        $this->request->setMethod('POST');
        $this->request->setPost(new Parameters($data));
        $response = $this->client->dispatch($this->request);
        return json_decode($response->getBody(), true);
    }

    public function getInitCount()
    {
        $this->request->setUri($this->apiUrl.'/init/count');
        $this->request->setMethod('GET');
        $response = $this->client->dispatch($this->request);
        return json_decode($response->getBody(), true);
    }
}
