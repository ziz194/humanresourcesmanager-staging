<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Service;

use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

/**
 * Class Tracker
 * @package QP\Common\Service
 */
class Tracker
{

    /**
     * @var Logger
     */
    private $tracker;

    /**
     * @var array
     */
    private $config;

    /**
     * Return a tracker instance.
     *
     * @param  array $services
     *
     * @return Tracker
     */
    public function __invoke($services)
    {
        $date         = new \DateTime();
        $trackDir     = './data/tracking/';
        $this->config = $services->get('Config');

        if (!file_exists($trackDir) && !is_dir($trackDir)) {
            mkdir($trackDir);
        }

        $filename = 'track_' . $date->format('Y-m-d') . '.txt';
        if (!is_readable($trackDir.$filename)) {
            fopen($trackDir.$filename, "w") ;
        }

        // if the day of the month is at least the fifth
        // remove all the old stuff but the current month
        if ((int)$date->format('d') > 5) {
            $months = ['01'=>1,'02'=>1,'03'=>1,'04'=>1,'05'=>1,'06'=>1,'07'=>1,'08'=>1,'09'=>1,'10'=>1,'11'=>1,'12'=>1];
            $months[$date->format('m')] = 0;
            foreach ($months as $month => $allowed) {
                if ($allowed) {
                    array_map('unlink', glob($trackDir.'track_'.$date->format('Y').'-'.$month.'*.txt'));
                }
            }
        }

        $writer = new Stream($trackDir . $filename);
        $this->tracker = new Logger();
        $this->tracker->addWriter($writer);

        return $this;
    }

    /**
     * @param \QP\User\Entity\User|null     $user
     * @param string                        $trackTime
     * @param array                         $customParams
     */
    public function trackPageLoad($user, $trackTime, $customParams = []) {

        $track      = $this->config['tracker']['params'];
        $osPlatform = $this->getOsPlatform($_SERVER['HTTP_USER_AGENT']);

        $track['web_id_account']        = 'no_account';
        $track['session']               = $user->getIdHash();
        $track['event']                 = 'page_load';
        $track['channel']               = 'direct';
        $track['device']                = $osPlatform[1];
        $track['os']                    = $osPlatform[0];
        $track['browser']               = $this->getBrowser($_SERVER['HTTP_USER_AGENT']);
        $track['product_id']            = 'no_product';
        $track['product_options']       = 'no_product_options';
        $track['url']                   = $_SERVER['REQUEST_URI'];
        $track['ipv4']                  = CryptService::getAnonymousIPv4();
        $track['ipv6']                  = CryptService::getAnonymousIPv6();
        $track['page_loaded']           = 1;
        $track['page_load_time']        = $trackTime;

        // JS
//        $track['region']                = '';
//        $track['timezone']              = '';
//        $track['view_port']             = '';
//        $track['event_object_position'] = '';
//        $track['js_click']              = '';
//        $track['js_click_params']       = '';
//        $track['js_click_time']         = '';
//        $track['js_exit_page']          = '';
//        $track['js_exit_time']          = '';

        // apply custom params
        foreach ($track as $param => $val) {
            if (isset($customParams[$param])) {
                $track[$param] = $customParams[$param];
            }
        }
        // apply get params
        foreach ($_GET as $param => $val) {
            if (isset($_GET[$param])) {
                $track[$param] = $val;
            }
        }

//        \FirePHP::getInstance(true)->info($track);
        $track = implode(',',$track);
//        \FirePHP::getInstance(true)->info($_COOKIE);
//        \FirePHP::getInstance(true)->info($_SESSION);
//        \FirePHP::getInstance(true)->info($_POST);
//        \FirePHP::getInstance(true)->info($_GET);
//        \FirePHP::getInstance(true)->info($_SERVER);
        // put params in here

        $this->tracker->info($track);
    }

    /**
     * @param  string $userAgent
     *
     * @return string
     */
    function getOsPlatform($userAgent)
    {
        $osPlatform = "Unknown OS Platform";
        $osArray    = [
            '/windows nt 10/i'      => ['Windows 10', 'desktop'],
            '/windows nt 6.3/i'     => ['Windows 8.1', 'desktop'],
            '/windows nt 6.2/i'     => ['Windows 8', 'desktop'],
            '/windows nt 6.1/i'     => ['Windows 7', 'desktop'],
            '/windows nt 6.0/i'     => ['Windows Vista', 'desktop'],
            '/windows nt 5.2/i'     => ['Windows Server 2003/XP x64', 'desktop'],
            '/windows nt 5.1/i'     => ['Windows XP', 'desktop'],
            '/windows xp/i'         => ['Windows XP', 'desktop'],
            '/windows nt 5.0/i'     => ['Windows 2000', 'desktop'],
            '/windows me/i'         => ['Windows ME', 'desktop'],
            '/win98/i'              => ['Windows 98', 'desktop'],
            '/win95/i'              => ['Windows 95', 'desktop'],
            '/win16/i'              => ['Windows 3.11', 'desktop'],
            '/macintosh|mac os x/i' => ['Mac OS X', 'desktop'],
            '/mac_powerpc/i'        => ['Mac OS 9', 'desktop'],
            '/linux/i'              => ['Linux', 'desktop'],
            '/ubuntu/i'             => ['Ubuntu', 'desktop'],
            '/iphone/i'             => ['iPhone', 'iPhone'],
            '/ipod/i'               => ['iPod', 'iPod'],
            '/ipad/i'               => ['iPad', 'iPad'],
            '/android/i'            => ['Android', 'Android'],
            '/blackberry/i'         => ['BlackBerry', 'BlackBerry'],
            '/webos/i'              => ['Mobile', 'Mobile'],
        ];

        foreach ($osArray as $regex => $value) {
            if (preg_match($regex, $userAgent)) {
                $osPlatform    =   $value;
            }
        }
        return $osPlatform;
    }

    /**
     * @param  string $userAgent
     *
     * @return string
     */
    function getBrowser($userAgent)
    {
        $browser      = "Unknown Browser";
        $browserArray = [
            '/msie/i'      => 'Internet Explorer',
            '/firefox/i'   => 'Firefox',
            '/safari/i'    => 'Safari',
            '/chrome/i'    => 'Chrome',
            '/edge/i'      => 'Edge',
            '/opera/i'     => 'Opera',
            '/netscape/i'  => 'Netscape',
            '/maxthon/i'   => 'Maxthon',
            '/konqueror/i' => 'Konqueror',
            '/vivaldi/i'   => 'Vivaldi',
            '/mobile/i'    => 'Handheld Browser',
        ];

        foreach ($browserArray as $regex => $value) {
            if (preg_match($regex, $userAgent)) {
                $browser = $value;
            }
        }

        return $browser;
    }
}
