<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Service;

use Monolog;
use Zend\Log;
use QP\Common\Log\Writer;
use Elasticsearch;

/**
 * Class ElasticSearchManagerService
 * @package QP\Common\Service
 */
class ElasticSearchManagerService
{

    /**
     * @var \Elasticsearch\Client
     */
    private $elasticSearchClient;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Return a logger instance.
     *
     * @param array $services
     * @return \Zend\Log\Logger
     */
    public function __invoke($services)
    {
        $config = $services->get('Config');

        $this->elasticSearchClient = new Elasticsearch\Client(['hosts' => $config['elasticsearch']['hosts']]);
        $this->entityManager = $services->get('EntityManager');


        return $this;
    }

    /**
     * Get an instance of a repository.
     *
     * @param string $entityName
     * @return \QP\Common\EntityRepository\ElasticSearchEntityRepository
     */
    public function getRepository($entityName)
    {

        $repositoryName = str_replace('\Entity', '\EntityRepository', $entityName) . 'Repository';
        $reflect = new \ReflectionClass($repositoryName);

        return $reflect->newInstance($this, $this->entityManager);
    }

    /**
     * Get the elastic search client
     *
     * @return \Elasticsearch\Client
     */
    public function getElasticSearchClient()
    {
        return $this->elasticSearchClient;
    }
}
