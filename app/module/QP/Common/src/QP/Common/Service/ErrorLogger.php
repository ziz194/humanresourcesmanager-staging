<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Service;

use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;

/**
 * Class ElasticSearchManagerService
 * @package QP\Common\Service
 */
class ErrorLogger
{

    /**
     * @var Logger
     */
    private $errorLogger;

    /**
     * @var array
     */
    private $config;

    /**
     * Return a logger instance.
     *
     * @param  array $services
     * @return ErrorLogger
     */
    public function __invoke($services)
    {
        $date         = new \DateTime();
        $logDir       = './data/logs/';
        $this->config = $services->get('Config');

        if (!file_exists($logDir) && !is_dir($logDir)) {
            mkdir($logDir);
        }

        $filename = 'log_' . $date->format('Y-m-d') . '.txt';
        if (!is_readable($logDir.$filename)) {
            fopen($logDir.$filename, "w") ;
        }

        // if the day of the month is at least the 20th
        // remove all the old stuff but the current month
        if ((int)$date->format('d') > 20) {
            $months = ['01'=>1,'02'=>1,'03'=>1,'04'=>1,'05'=>1,'06'=>1,'07'=>1,'08'=>1,'09'=>1,'10'=>1,'11'=>1,'12'=>1];
            $months[$date->format('m')] = 0;
            foreach ($months as $month => $allowed) {
                if ($allowed) {
                    array_map('unlink', glob($logDir.'log_'.$date->format('Y').'-'.$month.'*.txt'));
                }
            }
        }

        $writer = new Stream($logDir . $filename);
        $this->errorLogger = new Logger();
        $this->errorLogger->addWriter($writer);

        return $this;
    }

    /**
     * @param \Exception $e
     */
    public function logException(\Exception $e) {
        $trace = $e->getTraceAsString();

        $i = 1;
        do {
            $messages[] = $i++ . ": " . $e->getMessage();
        } while ($e = $e->getPrevious());

        $log  = "Exception:\n" . implode("\n", $messages);
        $log .= "\nTrace:\n" . $trace;
        $log .= "\n\n--------------------------------------------\n\n";

        $this->errorLogger->err($log);

        if ($this->config['logger']['isAllowedToSendMails']) {
            // Psssst .. please don't tell the API we send mails without her
            // but if the the exception here is because the API is down
            // how could find out about it :)
            $message   = new Message();
            $transport = new Smtp();
            $transport->setOptions(new SmtpOptions($this->config['mail']['transport']['options']));
            $message->addTo($this->config['mail']['my_mail'], 'QP-Dev')
                ->addFrom($this->config['mail']['system_sender_email'], 'DEV_HRM ErrorLog')
                ->setEncoding('UTF-8')
                ->setSubject('ErrorLog')
                ->setBody($log);
            // go for it little dove :)
            $transport->send($message);
        }
    }
}
