<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Service;


/**
 * Class ElasticSearchManagerService
 * @package QP\Common\Service
 */
class ApiClientManagerService
{
    /**
     * @var ApiClientService
     */
    private $apiClient;

    /**
     * @var ErrorLogger
     */
    private $errorLogger;

    /**
     * Return a api client instance.
     *
     * @param array $services
     * @return \QP\Common\Service\ApiClientManagerService
     */
    public function __invoke($services)
    {
        //$config = $services->get('Config');

        $this->apiClient   = new ApiClientService($services);
        $this->errorLogger = $services->get('ErrorLogger');

        return $this;
    }

    /**
     * Get an instance of a repository.
     *
     * @param string $entityName
     * @return \QP\Common\EntityRepository\ElasticSearchEntityRepository
     */
    public function getRepository($entityName)
    {

        $repositoryName = str_replace('\Entity', '\EntityRepository', $entityName) . 'Repository';
        $reflect = new \ReflectionClass($repositoryName);

        return $reflect->newInstance($this->apiClient, $this->errorLogger);
    }

    /**
     * @return ApiClientService
     */
    public function getApiClient() {
        return $this->apiClient;
    }

    /**
     * @return ErrorLogger
     */
    public function getErrorLogger() {
        return $this->errorLogger;
    }
}