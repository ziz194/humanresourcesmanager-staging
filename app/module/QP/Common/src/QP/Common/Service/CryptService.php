<?php
/**
 * Quadriga Plattform Webfrontend
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Service;

/**
 * API Client
 *
 * Class CryptService
 * @package QP\Common\Service
 */
class CryptService
{

    /**
     * @var array
     */
    private $config;

    /**
     * Return an API Client instance.
     *
     * @param array $services
     * @return CryptService
     */
    public function __invoke($services)
    {
        $this->config = $services->get('Config');

        return $this;
    }

    /**
     * @Link    http://php.net/manual/de/function.mcrypt-encrypt.php
     * @param   string      $string
     *
     * @return  string
     */
    public function encrypt($string)
    {
        $urlEncodePhrase = md5($this->config['url_encode_phrase']);

        // create a random IV to use with CBC encoding
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv     = mcrypt_create_iv($ivSize, MCRYPT_RAND);

        // creates a cipher text compatible with AES (Rijndael block size = 128)
        $cipherText = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $urlEncodePhrase, $string, MCRYPT_MODE_CBC, $iv);

        // prepend the IV for it to be available for decryption
        $cipherText = $iv . $cipherText;

        // encode the resulting cipher text and make it url safe
        return strtr(base64_encode($cipherText ), '+/=', '-_,' );
    }

    /**
     * @Link    http://php.net/manual/de/function.mcrypt-encrypt.php
     * @param   string      $string
     *
     * @return  string
     */
    public function decrypt($string)
    {
        // check that the string as at least as long as the block size (nearly)
        if (strlen($string) < 30) {
            return '';
        }

        $urlEncodePhrase = md5( $this->config['url_encode_phrase'] );
        $cipherTextDec   = base64_decode(strtr($string, '-_,', '+/='));

        // create a random IV to use with CBC encoding
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $ivDec  = substr($cipherTextDec, 0, $ivSize);

        // retrieves the cipher text (everything except the $iv_size in the front)
        $cipherTextDec = substr($cipherTextDec, $ivSize);

        // may remove 00h valued characters from end of plain text
        return mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $urlEncodePhrase, $cipherTextDec, MCRYPT_MODE_CBC, $ivDec);
    }

    /**
     * @param  string $str
     *
     * @return string
     */
    public static function makeUrlWorthy($str)
    {
        $str = mb_strtolower($str);
        $str = str_replace('ä','ae',$str);
        $str = str_replace('ö','oe',$str);
        $str = str_replace('ü','ue',$str);
        $str = str_replace('ß','ss',$str);
        $str = preg_replace('/[^A-Za-z0-9]/','-',$str);
        $str = str_replace('--','-',$str);
        $str = str_replace('--','-',$str);
        $str = ltrim($str, '-');
        $str = rtrim($str, '-');
        return $str;
    }



    /**
     * @return string
     */
    public static function getAnonymousIPv4()
    {
        $ipAddressV4 = '';
        if ($_SERVER['REMOTE_ADDR']) {
            if(strpos($_SERVER['REMOTE_ADDR'], '.') !== false) {
                $ipAddressV4 = substr($_SERVER['REMOTE_ADDR'], 0, strrpos($_SERVER['REMOTE_ADDR'], ".")).'.XXX';
            }
        }
        return $ipAddressV4;
    }

    /**
     * @return string
     */
    public static function getAnonymousIPv6()
    {
        $ipAddressV6 = '';
        if ($_SERVER['REMOTE_ADDR']) {
            if(strpos($_SERVER['REMOTE_ADDR'], ':') !== false) {
                $ipAddressV6 = substr($_SERVER['REMOTE_ADDR'], 0, strrpos($_SERVER['REMOTE_ADDR'], ":")).':XX';
            }
        }
        return $ipAddressV6;
    }
}
