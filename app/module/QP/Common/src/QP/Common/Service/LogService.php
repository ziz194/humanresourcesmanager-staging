<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Service;

use Zend\Log;
use QP\Common\Log\Writer;

/**
 * Class LogService
 * @package QP\Common\Service
 */
class LogService
{

    /**
     * Return a logger instance.
     *
     * @param  array $services
     * @return \Zend\Log\Logger
     */
    public function __invoke($services)
    {
        $config                = $services->get('Config');
        $mvcEvent              = $services->get('Application')->getMvcEvent();
        $apiClient             = $services->get('ApiClientManager')->getApiClient();
        $translator            = $services->get('translator');
        $entityManager         = $services->get('EntityManager');
        $authenticationService = $services->get('Zend\Authentication\AuthenticationService');

        $routeName = $mvcEvent->getRouteMatch()->getMatchedRouteName();
        $user      = $entityManager->getRepository('QP\User\Entity\User')
                                   ->readOneById($authenticationService->getIdentity()['id']);

        $writer = new Writer\ApiClient(
            $apiClient,
            $translator,
            $user,
            $routeName,
            $config['application']['hostname']
        );
        $logger = new Log\Logger();
        $logger->addWriter($writer);

        return $logger;
    }
}
