<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Service;

use \Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Entity manager
 *
 * Class EntityManagerService
 * @package QP\Common\Service
 */
class EntityManagerService extends \DoctrineORMModule\Service\EntityManagerFactory
{
    /**
     * EntityManagerService constructor.
     */
    public function __construct()
    {
        parent::__construct('orm_default');
    }

    private static $entityManager = null;

    /**
     * Create the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return \Doctrine\ORM\EntityManager|null
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        if (!self::$entityManager) {
            self::$entityManager = parent::createService($serviceLocator);
        }

        return self::$entityManager;
    }
}
