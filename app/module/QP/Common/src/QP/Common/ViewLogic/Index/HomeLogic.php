<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Index;

use QP\Common\ViewLogic;
use QP\Common\ViewLogic\Response;

/**
 * Class HomeLogic
 * @package QP\Common\ViewLogic\Index
 */
class HomeLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{
    /**
     * Generate the view response.
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse()
    {
        // \FirePHP::getInstance(true)->info($this->getAuthenticationService()->getIdentity());
        if (!$this->getAuthenticationService()->hasIdentity()) {
            return new Response\RedirectResponse('user-login');
        }

        return new Response\TemplateResponse(
            'qp/common/index/home',
            []
        );
    }
}
