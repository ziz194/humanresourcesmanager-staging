<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Ajax;

use Zend\Http\Request;
use QP\Common\ViewLogic;
use QP\User\Entity\Follow;
use QP\Common\ViewLogic\Response;
use QP\Common\Service\CryptService;

/**
 * Class FollowLogic
 * @package QP\Common\ViewLogic\Ajax
 */
class FollowLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{

    /**
     * @var Request $request
     */
    private $request;

    /**
     * @var array $post
     */
    private $post;

    /**
     * @var array $config
     */
    private $config;

    /**
     * FollowLogic constructor.
     *
     * @param \Zend\Http\Request $request
     * @param array              $config
     */
    public function __construct(Request $request, $config)
    {
        $this->request = $request;
        $this->config  = $config;
        $this->post    = $this->request->getPost();
    }

    /**
     * Generate the view response.
     */
    public function getResponse()
    {
        try {
            // find via session and update or add the user
            $user = $this->handleUser([], true);

            $ipAddressV4 = CryptService::getAnonymousIPv4();
            $ipAddressV6 = CryptService::getAnonymousIPv6();

            setcookie(
                "followMeId",
                $user->getIdHash(),
                $this->config['session_config']['cookie_lifetime'],
                "/",
                $this->request->getUri()->getHost(),
                $this->config['session_config']['cookie_secure'],
                $this->config['session_config']['cookie_httponly']
            );

            $followDataSet = [
                'userId'                  => $user,
                'language'                => $this->post['navigator_language'],
                'platform'                => $this->post['navigator_platform'],
                'browser'                 => $this->post['navigator_appCodeName'],
                'userAgent'               => $_SERVER['HTTP_USER_AGENT'],
                'screenWidth'             => $this->post['screen_width'],
                'screen_height'           => $this->post['screen_height'],
                'screen_available_width'  => $this->post['screen_avail_width'],
                'screen_available_height' => $this->post['screen_avail_height'],
                'colorDepth'              => $this->post['color_depth'],
                'dpiX'                    => $this->post['dpi_x'],
                'dpiY'                    => $this->post['dpi_y'],
                'devicePixelRatio'        => $this->post['devicePixelRatio'],
                'timezoneOffset'          => $this->post['timezone_offset'],
                'javaEnabled'             => $this->post['java_enabled'],
                'acrobat'                 => $this->post['plugin_adobe_acrobat'],
                'mimeTypes'               => implode(',', $this->post['mimetypes']),
                'browserPlugins'          => implode(',', $this->post['plugins']),
                'systemColors'            => implode(',', $this->post['systemColors']),
                'ipAddressV4'             => $ipAddressV4,
                'ipAddressV6'             => $ipAddressV6,
                'httpReferrer'            => $_SERVER['HTTP_REFERER'],
                'createDate'              => new \DateTime(),
            ];
            //\FirePHP::getInstance(true)->info($followDataSet);

            $userFollowDataSet = new Follow();
            $userFollowDataSet->exchangeArray($followDataSet);
            $this->getEntityManager()->getRepository(\QP\User\Entity\Follow::getEntityName())->save($userFollowDataSet);
            $this->getEntityManager()->flush();

            $this->getTracker()->trackPageLoad($user, $this->trackTime, ['event'=>'cookie_accept']);

        } catch (\Exception $e) {
            \FirePHP::getInstance(true)->warn('ajax - follow - fail');
            \FirePHP::getInstance(true)->warn($e->getMessage());
        }
        return new Response\HttpResponse();
    }
}

