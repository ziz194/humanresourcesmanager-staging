<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Ajax;

use QP\User\Entity\Bookmark;
use Zend\Http\Request;
use QP\Common\ViewLogic;
use QP\Common\ViewLogic\Response;

/**
 * Class BookmarkLogic
 * @package QP\Common\ViewLogic\Ajax
 */
class BookmarkLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{

    /**
     * @var Request $request
     */
    private $request;

    /**
     * @var array $post
     */
    private $post;

    /**
     * @var array $config
     */
    private $config;

    /**
     * FollowLogic constructor.
     *
     * @param \Zend\Http\Request $request
     * @param array              $config
     */
    public function __construct(Request $request, $config)
    {
        $this->request = $request;
        $this->config  = $config;
        $this->post    = $this->request->getPost();
    }

    /**
     * Generate the view response.
     */
    public function getResponse()
    {
        try {
            //\FirePHP::getInstance(true)->info($this->post);

            // find via cookie, session or email and update or add the user
            $user = $this->handleUser($this->post);

            if ($this->post->addBookmark === 'true') {
                $newBookmark = new Bookmark();
                $newBookmark->exchangeArray([
                    'userId'         => $user,
                    'portalId'       => $this->post->portal,
                    'entity'         => $this->post->entity,
                    'esId'           => $this->post->esId,
                    'createDate'     => new \DateTime(),
                ]);
                $this->getEntityManager()->getRepository(Bookmark::getEntityName())->save($newBookmark);
                $this->getEntityManager()->flush();
            } else {
                $this->getEntityManager()->getRepository(Bookmark::getEntityName())->deleteOneByUserIdAndEsId($user->getId(), $this->post->esId);
            }

            $this->getTracker()->trackPageLoad($user, $this->trackTime, ['event'=>'bookmarked']);

        } catch (\Exception $e) {
            \FirePHP::getInstance(true)->warn('ajax - bookmark - fail');
            \FirePHP::getInstance(true)->warn($e->getMessage());
        }
        return new Response\HttpResponse();
    }
}

