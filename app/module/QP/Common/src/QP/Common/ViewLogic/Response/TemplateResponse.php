<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Response;

/**
 * A view logic response representing a template view model.
 *
 * @package QP\Common\ViewLogic\Response
 */
class TemplateResponse extends AbstractResponse
{
    /**
     * The template to render.
     * @var string
     */
    private $template;

    /**
     * Constructor
     *
     * @param string $template The parameters sent to the view model.
     * @param array $params
     */
    public function __construct($template, $params)
    {
        if (empty($template)) {
            throw new Exception\InvalidArgumentException("Invalid parameter. 'template' can not be empty");
        }

        $this->template = $template;
        parent::__construct($params);
    }

    /**
     * Get the template to render.
     * @return string The template name to render.
     */
    public function getTemplate()
    {
        return $this->template;
    }
}
