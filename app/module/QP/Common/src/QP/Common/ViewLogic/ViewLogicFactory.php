<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 * Class ViewLogicFactory
 * @package QP\Common\ViewLogic
 */
class ViewLogicFactory implements ServiceLocatorAwareInterface
{
    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    protected $serviceLocator;

    /**
     * @var $layout
     */
    protected $layout;

    /**
     * Get the service locator
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * Set the service locator
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @param $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * Create an instance of a view logic class
     *
     * @param string $viewLogicName
     * @param array $params
     *
     * @return \QP\Common\ViewLogic\ViewLogicInterface
     */
    public function build($viewLogicName, $params)
    {
        $reflect = new \ReflectionClass('QP\\' . $viewLogicName);

        /** @var \QP\Common\ViewLogic\ViewLogicAbstract $instance */
        $instance = $reflect->newInstanceArgs($params);

        //$instance->setShopConnector( new \TanteEmma\Connector() );

        $instance->setLayout($this->layout);
        $instance->setCacheAdapter($this->getServiceLocator()->get('Zend\Cache\Storage\Factory'));
        $instance->setViewRenderer($this->getServiceLocator()->get('ViewRenderer'));
        $instance->setCrypt($this->getServiceLocator()->get('Crypt'));
        $instance->setErrorLogger($this->getServiceLocator()->get('ErrorLogger'));
        $instance->setTracker($this->getServiceLocator()->get('Tracker'));
        $instance->setApiClient($this->getServiceLocator()->get('ApiClientManager'));

        $instance->setFlashMessenger( new \Zend\Mvc\Controller\Plugin\FlashMessenger());
        $instance->setEntityManager($this->getServiceLocator()->get('EntityManager'));
        $instance->setAuthenticationService(
            $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService')
        );
        $instance->setSessionManager($this->getServiceLocator()->get('Zend\Session\SessionManager'));
        $instance->getBookmarkIds();

        $userSessionId = '';
        $user = $instance->handleUser([]);
        if ($user) {
            $userSessionId = $user->getIdHash();
        }
        $this->layout->setVariable('userSessionId', $userSessionId);

        return $instance;
    }
}
