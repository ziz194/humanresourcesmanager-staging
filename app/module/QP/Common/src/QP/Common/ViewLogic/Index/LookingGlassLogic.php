<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Index;

use QP\Common\ViewLogic;
use QP\Common\ViewLogic\Response;

/**
 * Class LookingGlass
 * @package QP\Common\ViewLogic\Index
 */
class LookingGlassLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{

    /**
     * @var string $scope
     */
    private $scope;

    /**
     * @var string $date
     */
    private $date;

    /**
     * LookingGlass constructor.
     *
     * @param string $scope
     * @param string $date
     */
    public function __construct($scope, $date)
    {
        $this->scope = $scope;
        $this->date  = $date;
    }

    /**
     * Generate the view response.
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse()
    {
        $user = $_SERVER['PHP_AUTH_USER'];
        $pass = $_SERVER['PHP_AUTH_PW'];
        if ($user !== 'QP_ND' || $pass !== '>#ko0c4uqMl/(S/`B>loHFIbP') {

            header('WWW-Authenticate: Basic realm="Looking Glass"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'Dann eben nicht :(';
            exit;

        } else {
            $date = date('Y-m-d');
            if ($this->date) {
                $date = $this->date;
            }
            
            switch ($this->scope) {
                case 'log':
                    $file = './data/logs/log_';
                    break;
                case 'track':
                    $file = './data/tracking/track_';
                    break;
                default:
                    $file = '';
                    break;
            }

            if (is_readable($file.$date.'.txt')) {
                $file = file_get_contents($file.$date.'.txt');
            } else {
                $file = 'nope';
            }

            return new Response\TemplateResponse(
                'qp/common/index/lookingGlass',
                [
                    'file' => nl2br($file),
                ]
            );
        }
    }
}
