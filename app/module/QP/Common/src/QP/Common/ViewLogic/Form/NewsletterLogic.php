<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Form;

use Zend\Http\Request;
use QP\Common\ViewLogic;
use QP\User\Entity\Newsletter;

/**
 * Class NewsletterLogic
 * @package QP\Common\ViewLogic\Form
 */
class NewsletterLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{

    /**
     * @var Request $request
     */
    private $request;

    /**
     * @var array $post
     */
    private $post;

    /**
     * @var array $config
     */
    private $config;

    /**
     * @var \Zend\Mvc\Controller\Plugin\Redirect $redirect
     */
    private $redirect;

    /**
     * FollowLogic constructor.
     *
     * @param \Zend\Http\Request                    $request
     * @param array                                 $config
     * @param \Zend\Mvc\Controller\Plugin\Redirect  $redirect
     */
    public function __construct(Request $request, $config, $redirect)
    {
        $this->request  = $request;
        $this->config   = $config;
        $this->redirect = $redirect;
        $this->post     = $this->request->getPost();
    }

    /**
     * Generate the view response.
     */
    public function getResponse()
    {
        if ($this->request->isPost() &&
            isset($this->post['firstname']) && $this->post['firstname'] &&
            isset($this->post['lastname']) && $this->post['lastname'] &&
            isset($this->post['email']) && $this->post['email'] &&
            isset($this->post['nl']) && count($this->post['nl']))
        {
            try {
                //\FirePHP::getInstance(true)->info($this->post);
                $opiIds   = strip_tags(implode(',', $this->post['nl']));
                $nlFields = array(
                    'lastname'  => $this->post['lastname'],
                    'firstname' => $this->post['firstname'],
                    'email'     => $this->post['email'],
                    'nids'      => $opiIds,
                    'ip'        => $_SERVER['REMOTE_ADDR'],
                );

                // find via cookie or email and update or add the user
                $user = $this->handleUser($this->post);

                $newNewsletter = new Newsletter();
                $newNewsletter->exchangeArray([
                    'userId'         => $user,
                    'opiId'          => $opiIds,
                    'newsletterName' => 'HRM Portal',
                    'createDate'     => new \DateTime(),
                ]);
                $this->getEntityManager()->getRepository(Newsletter::getEntityName())->save($newNewsletter);
                $this->getEntityManager()->flush();

                $apiResponse = $this->getApiClient()->getApiClient()->newsletter($nlFields);
                //\FirePHP::getInstance(true)->info($apiResponse);

                $this->getTracker()->trackPageLoad($user, $this->trackTime, ['event' => 'newsletter']);

            } catch (\Exception $e) {
                \FirePHP::getInstance(true)->warn('form - newsletter - fail');
                \FirePHP::getInstance(true)->warn($e->getMessage());
            }
        }
//        return new Response\HttpResponse();
        return $this->redirect->toUrl($_SERVER['HTTP_REFERER']);
    }
}

