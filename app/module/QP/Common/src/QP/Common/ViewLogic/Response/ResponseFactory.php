<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Response;

/**
 * Class ResponseFactory
 * @package QP\Common\ViewLogic\Response
 */
class ResponseFactory
{
    const TYPE_JSON_RESPONSE = 'json';
    const TYPE_TEMPLATE_RESPONSE = 'template';
    const TYPE_REDIRECT_RESPONSE = 'redirect';
}
