<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Response;

/**
 * A view logic response representing a json model.
 *
 * @package QP\Common\ViewLogic\Response
 */
class ConsoleResponse extends AbstractResponse
{
    private $output;

    /**
     * ConsoleResponse constructor.
     * @param array $output
     */
    public function __construct($output)
    {
        $this->output = $output;
    }

    /**
     * Print message
     */
    public function printMessage()
    {
        if (is_array($this->output)) {
            foreach ($this->output as $key => $value) {
                if (is_string($value) || is_int($value)) {
                    echo "\n". $key . ': ' . $value . "\n";
                }
            }
        } elseif (is_string($this->output) || is_int($this->output)) {
            echo "\n". $this->output . "\n";
        }
    }
}
