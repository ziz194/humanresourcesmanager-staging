<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Response;

use Zend\Http\PhpEnvironment\Response;

/**
 * A view logic response representing a http model.
 *
 * @package QP\Common\ViewLogic\Response
 */
class HttpResponse extends Response
{
}
