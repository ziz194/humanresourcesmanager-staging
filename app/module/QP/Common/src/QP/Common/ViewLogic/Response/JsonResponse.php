<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Response;

/**
 * A view logic response representing a json model.
 *
 * @package QP\Common\ViewLogic\Response
 */
class JsonResponse extends AbstractResponse
{
}
