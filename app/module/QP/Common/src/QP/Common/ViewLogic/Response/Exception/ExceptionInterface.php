<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Response\Exception;

/**
 * Interface ExceptionInterface
 * @package QP\Common\Driver\Exception
 */
interface ExceptionInterface
{

}
