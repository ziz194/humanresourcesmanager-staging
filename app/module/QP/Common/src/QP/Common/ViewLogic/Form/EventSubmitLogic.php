<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Form;

use Zend\Http\Request;
use QP\Common\ViewLogic;
use QP\User\Entity\EventSubmit;
use QP\Common\ViewLogic\Response;

/**
 * Class EventSubmitLogic
 * @package QP\Common\ViewLogic\Form
 */
class EventSubmitLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{

    /**
     * @var Request $request
     */
    private $request;

    /**
     * @var array $post
     */
    private $post;

    /**
     * @var array $config
     */
    private $config;

    /**
     * @var \Zend\Mvc\Controller\Plugin\Redirect $redirect
     */
    private $redirect;

    /**
     * @var \Zend\I18n\Translator\Translator
     */
    private $translator;

    /**
     * FollowLogic constructor.
     *
     * @param \Zend\Http\Request                    $request
     * @param array                                 $config
     * @param \Zend\Mvc\Controller\Plugin\Redirect  $redirect
     * @param \Zend\I18n\Translator\Translator      $translator
     */
    public function __construct(Request $request, $config, $redirect, $translator)
    {
        $this->request    = $request;
        $this->config     = $config;
        $this->redirect   = $redirect;
        $this->translator = $translator;
        $this->post       = $this->request->getPost()->toArray();
    }

    /**
     * Generate the view response.
     */
    public function getResponse()
    {
        //\FirePHP::getInstance(true)->info($this->post);
        if ($this->request->isPost() &&
            isset($this->post['additionalType']) && $this->post['additionalType'] &&
            isset($this->post['title'])          && $this->post['title'] &&
            isset($this->post['from_date'])      && $this->post['from_date'] &&
            isset($this->post['until_date'])     && $this->post['until_date'] &&
            isset($this->post['message'])        && $this->post['message'] &&
            isset($this->post['host'])           && $this->post['host'] &&
            isset($this->post['location'])       && $this->post['location'] &&
            isset($this->post['partner'])        && $this->post['partner'] &&
            isset($this->post['email'])          && $this->post['email'])
        {
            try {
                // find via cookie or email and update or add the user
                $user = $this->handleUser($this->post);

                $data                 = $this->post;
                $data['userId']       = $user;
                $data['fromDate']     = new \DateTime($this->post['from_date']);
                $data['fromTime']     = $this->post['from_date_hours'].':'.$this->post['from_date_minutes'];
                $data['fromFullDay']  = 0;
                $data['untilDate']    = new \DateTime($this->post['until_date']);
                $data['untilTime']    = $this->post['until_date_hours'].':'.$this->post['until_date_minutes'];
                $data['untilFullDay'] = 0;
                $data['createDate']   = new \DateTime();

                $guestTypes = [];
                foreach ($this->post['guest'] as $guest) {
                    if ($guest === 'register') {
                        $data['guestRegister'] = $guest;
                        $guestTypes[] = 'Nur mit Anmeldung';
                    }
                    if ($guest === 'costs') {
                        $data['guestCosts'] = $guest;
                        $guestTypes[] = 'Mit Kostenbeitrag';
                    }
                    if ($guest === 'members') {
                        $data['guestMember'] = $guest;
                        $guestTypes[] = 'Nur für Mitglieder/geladene Gäste';
                    }
                }

                $from = $this->post['from_date'];
                if (isset($this->post['from_date_full_day']) && $this->post['from_date_full_day']) {
                    $data['fromFullDay'] = 1;
                    $from .= ' (ganztägig)';
                } else {
                    $from .= ' '.$data['fromTime'];
                }
                $until = $this->post['until_date'];
                if (isset($this->post['until_date_full_day']) && $this->post['until_date_full_day']) {
                    $data['untilFullDay'] = 1;
                    $until .= ' (ganztägig)';
                } else {
                    $until .= ' '.$data['untilTime'];
                }

                $newEventSubmit = new EventSubmit();
                $newEventSubmit->exchangeArray($data);
                $this->getEntityManager()->getRepository(EventSubmit::getEntityName())->save($newEventSubmit);
                $this->getEntityManager()->flush();

                $text = 'Hallo<br><br>'
                      . $data['partner']. '/' .$data['host']. ' hat ein Event angemeldet.<br>'
                      . 'Email: '        . $data['email'].'<br>'
                      . 'Telefon: '      . $data['tel'].'<br><br><br><table>'
                      . '<tr><td>Name:</td><td>'  . $data['title'].'</td></tr>'
                      . '<tr><td>Von:</td><td>'   . $from.'</td></tr>'
                      . '<tr><td>Bis:</td><td>'   . $until.'</td></tr>'
                      . '<tr><td>Ort:</td><td>'   . $data['location']. ', ' . $data['plz']. ', '.$data['street'].'</td></tr>'
                      . '<tr><td>Typ:</td><td>'   . $data['additionalType'].'</td></tr>'
                      . '<tr><td>Gäste:</td><td>' . implode(',',$guestTypes).'</td></tr></table>'
                      . 'Nachricht:<br>'          . $data['message'].'<br><br><br><br>';

                $apiResponse = $this->getApiClient()->getApiClient()->mail([
                    'to'      => $this->config['mail']['my_mail'], //TODO: replace dummy by contact mail
                    'from'    => $this->config['mail']['system_sender_email'],
                    'subject' => $this->translator->translate('EVENT_SUBMIT_MAIL_SUBJECT').' - '.strip_tags(trim($data['title'])),
                    'text'    => $text,
                ]);

                $this->getTracker()->trackPageLoad($user, $this->trackTime, ['event' => 'event_submit']);
//                \FirePHP::getInstance(true)->info($apiResponse);

            } catch (\Exception $e) {
                \FirePHP::getInstance(true)->warn('form - contact - fail');
                \FirePHP::getInstance(true)->warn($e->getMessage());
            }
        }
//        return new Response\HttpResponse();
        return $this->redirect->toUrl($_SERVER['HTTP_REFERER']);
    }
}

