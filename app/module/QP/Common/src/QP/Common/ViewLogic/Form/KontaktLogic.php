<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Form;

use Zend\Http\Request;
use QP\Common\ViewLogic;
use QP\User\Entity\Contact;
use QP\Common\ViewLogic\Response;

/**
 * Class KontaktLogic
 * @package QP\Common\ViewLogic\Form
 */
class KontaktLogic extends ViewLogic\ViewLogicAbstract implements ViewLogic\ViewLogicInterface
{

    /**
     * @var Request $request
     */
    private $request;

    /**
     * @var array $post
     */
    private $post;

    /**
     * @var array $config
     */
    private $config;

    /**
     * @var \Zend\Mvc\Controller\Plugin\Redirect $redirect
     */
    private $redirect;

    /**
     * @var \Zend\I18n\Translator\Translator
     */
    private $translator;

    /**
     * FollowLogic constructor.
     *
     * @param \Zend\Http\Request                    $request
     * @param array                                 $config
     * @param \Zend\Mvc\Controller\Plugin\Redirect  $redirect
     * @param \Zend\I18n\Translator\Translator      $translator
     */
    public function __construct(Request $request, $config, $redirect, $translator)
    {
        $this->request    = $request;
        $this->config     = $config;
        $this->redirect   = $redirect;
        $this->translator = $translator;
        $this->post       = $this->request->getPost();
    }

    /**
     * Generate the view response.
     */
    public function getResponse()
    {
        //\FirePHP::getInstance(true)->info($this->post);
        if ($this->request->isPost() &&
            isset($this->post['firstname']) && $this->post['firstname'] &&
            isset($this->post['lastname']) && $this->post['lastname'] &&
            isset($this->post['email']) && $this->post['email'])
        {
            try {
                // find via cookie or email and update or add the user
                $user = $this->handleUser($this->post->toArray());

                $newContact = new Contact();
                $newContact->exchangeArray([
                    'userId'     => $user,
                    'request'    => $this->post['request'],
                    'message'    => $this->post['message'],
                    'createDate' => new \DateTime(),
                ]);
                $this->getEntityManager()->getRepository(Contact::getEntityName())->save($newContact);
                $this->getEntityManager()->flush();

                $text = 'Hallo<br><br>'
                      . $this->post['firstname']. ' ' .$this->post['lastname']. ' hat eine Anfrage geststellt.<br>'
                      . 'Email: '        . $this->post['email'].'<br>'
                      . 'Telefon: '      . $this->post['tel'].'<br><br><br>'
                      . 'Anliegen:<br>'  . $this->post['request'].'<br><br>'
                      . 'Nachricht:<br>' . $this->post['message'].'<br><br>';

                $apiResponse = $this->getApiClient()->getApiClient()->mail([
                    'to'      => $this->config['mail']['my_mail'], //TODO: replace dummy by contact mail
                    'from'    => $this->config['mail']['system_sender_email'],
                    'subject' => $this->translator->translate('CONTACT_MAIL_SUBJECT').' - '.strip_tags(trim($this->post['request'])),
                    'text'    => $text,
                ]);

                $this->getTracker()->trackPageLoad($user, $this->trackTime, ['event' => 'kontakt']);
                //\FirePHP::getInstance(true)->info($apiResponse);

            } catch (\Exception $e) {
                \FirePHP::getInstance(true)->warn('form - contact - fail');
                \FirePHP::getInstance(true)->warn($e->getMessage());
            }
        }
//        return new Response\HttpResponse();
        return $this->redirect->toUrl($_SERVER['HTTP_REFERER']);
    }
}

