<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Response;

/**
 * A view logic response representing an http redirect.
 *
 * @package QP\Common\ViewLogic\Response
 */
class RedirectResponse extends AbstractResponse
{
    /**
     * The route to redirect to.
     * @var string
     */
    private $route;

    /**
     * The hashtag appended to the redirect url.
     * string @var
     */
    private $hashtag;

    /**
     * Constructor
     *
     * @param array $route The route to redirect to.
     * @param $params The parameters to replace in the route.
     * @param null $hashtag The hashtag appended to the redirect url.
     */
    public function __construct($route, $params = [], $hashtag = null)
    {
        if (empty($route)) {
            throw new Exception\InvalidArgumentException("Invalid parameter. 'route' can not be empty");
        }

        $this->route = $route;
        if (!is_null($hashtag)) {
            $this->hashtag = $hashtag;
        }
        parent::__construct($params);
    }

    /**
     * Get the route to redirect to.
     * @return string The route to redirect to.
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Get the hashtag appended to the redirect url.
     * @return array The hashtag.
     */
    public function getHashtag()
    {
        return $this->hashtag;
    }
}
