<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Response;

/**
 * A view logic abstract response.
 *
 * @package QP\Common\ViewLogic\Response
 */
abstract class AbstractResponse
{
    /**
     * The parameters of a response.
     * @var array
     */
    private $params = [];

    /**
     * Constructor
     *
     * @param array $params The parameters of a response.
     */
    public function __construct($params)
    {
        if (!is_array($params)) {
            throw new Exception\InvalidArgumentException("Invalid parameter. 'params' has to be an array");
        }
        $this->params = $params;
    }

    /**
     * Get the params of a response.
     *
     * @return array The params.
     */
    public function getParams()
    {
        return $this->params;
    }
}
