<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic\Response\Exception;

/**
 * Class InvalidArgumentException
 * @package QP\Common\ViewLogic\Response\Exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
