<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic;

use QP\User\Entity\User;
use QP\User\Entity\Bookmark;
use Doctrine\ORM\EntityManager;
use QP\Common\Service\CryptService;
use Zend\Authentication\AuthenticationService;

/**
 * Class ViewLogicAbstract
 * @package QP\Common\ViewLogic
 */
abstract class ViewLogicAbstract
{

    /**
     * @var $layout
     */
    protected $layout;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Zend\Log\Logger
     */
    private $logService;

    /**
     * @var \QP\Common\Service\ApiClientManagerService
     */
    private $apiClient;

    /**
     * @var \QP\Common\Service\CryptService
     */
    private $crypt;

    /**
     * @var \QP\Common\Service\ElasticSearchManagerService
     */
    private $elasticSearchManager;

    /**
     * @var \QP\Common\Service\ErrorLogger
     */
    private $errorLogger;

    /**
     * @var string
     */
    protected $trackTime;

    /**
     * @var \QP\Common\Service\Tracker
     */
    private $tracker;

    /**
     * @var \Zend\Mvc\Controller\Plugin\FlashMessenger
     */
    private $flashMessenger;

    /**
     * @var \Zend\Authentication\AuthenticationService $authenticationService
     */
    private $authenticationService;

    /**
     * @var \Zend\Session\SessionManager $sessionManager
     */
    private $sessionManager;

    /**
     * @var int
     */
    protected $confirmation;

    /**
     * @var \Zend\Cache\Storage\Adapter\Filesystem
     */
    protected $cacheAdapter;


    /**
     * @var \Zend\View\Renderer\PhpRenderer
     */
    protected $viewRenderer;

    /**
     * @var
     */
    protected $shopConnector;

    /**
     * @var array
     */
    protected $bookmarkIds = [];

    /**
     * Get the view Rendererr
     *
     * @return \Zend\View\Renderer\PhpRenderer
     */
    public function getViewRenderer()
    {
        return $this->viewRenderer;
    }

    /**
     * Set the view Rendererr
     *
     * @param \Zend\View\Renderer\PhpRenderer $viewRendererr
     */
    public function setViewRenderer($viewRendererr)
    {
        $this->viewRenderer = $viewRendererr;
    }

    /**
     * Get the
     *
     * @return \TanteEmma\Connector
     */
    public function getShopConnector()
    {
        return $this->shopConnector;
    }

    /**
     * Set the
     *
     * @param
     */
    public function setShopConnector($shopConnector)
    {
        $this->shopConnector = $shopConnector;
    }

    /**
     * Get the entity manager.
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * Set the entity manager.
     *
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get the log service.
     *
     * @return \Zend\Log\Logger
     */
    public function getLogService()
    {
        return $this->logService;
    }

    /**
     * Set the log service.
     *
     * @param \Zend\Log\Logger $logService
     */
    public function setLogService($logService)
    {
        $this->logService = $logService;
    }

    /**
     * Get the log cacheAdapter.
     *
     * @return \Zend\Cache\Storage\Adapter\Filesystem
     */
    public function getCacheAdapter()
    {
        return $this->cacheAdapter;
    }

    /**
     * Set the log cacheAdapter.
     *
     * @param \Zend\Cache\Storage\Adapter\Filesystem $cacheAdapter
     */
    public function setCacheAdapter($cacheAdapter)
    {
        $this->cacheAdapter = $cacheAdapter;
    }

    /**
     * Get the authentication service.
     *
     * @return \Zend\Authentication\AuthenticationService
     */
    public function getAuthenticationService()
    {
        return $this->authenticationService;
    }

    /**
     * Set the authentication service.
     *
     * @param \Zend\Authentication\AuthenticationService $authenticationService
     */
    public function setAuthenticationService(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * Get the session manager.
     *
     * @return \Zend\Session\SessionManager
     */
    public function getSessionManager()
    {
        return $this->sessionManager;
    }

    /**
     * Set the session manager.
     *
     * @param \Zend\Session\SessionManager $sessionManager
     */
    public function setSessionManager($sessionManager)
    {
        $this->sessionManager = $sessionManager;
    }

    /**
     * Get the elastic search manager.
     *
     * @return \QP\Common\Service\ElasticSearchManagerService
     */
    public function getElasticSearchManager()
    {
        return $this->elasticSearchManager;
    }

    /**
     * Set the elastic search manager.
     *
     * @param \QP\Common\Service\ElasticSearchManagerService $elasticSearchManager
     */
    public function setElasticSearchManager($elasticSearchManager)
    {
        $this->elasticSearchManager = $elasticSearchManager;
    }

    /**
     * Get the error logger.
     *
     * @return \QP\Common\Service\ErrorLogger
     */
    public function getErrorLogger()
    {
        return $this->errorLogger;
    }

    /**
     * Set the error logger.
     *
     * @param \QP\Common\Service\ErrorLogger $errorLogger
     */
    public function setErrorLogger($errorLogger)
    {
        $this->errorLogger = $errorLogger;
    }

    /**
     * Get the tracker.
     *
     * @return \QP\Common\Service\Tracker
     */
    public function getTracker()
    {
        $this->trackTime = date('Y-m-d H:i:s');
        return $this->tracker;
    }

    /**
     * Set the tracker.
     *
     * @param \QP\Common\Service\Tracker $tracker
     */
    public function setTracker($tracker)
    {
        $this->tracker = $tracker;
    }

    /**
     * Get the api client.
     *
     * @return \QP\Common\Service\ApiClientManagerService
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the api client.
     *
     * @param \QP\Common\Service\ApiClientManagerService $apiClient
     */
    public function setApiClient($apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * Get the elastic search manager.
     *
     * @return \QP\Common\Service\CryptService
     */
    public function getCrypt()
    {
        return $this->crypt;
    }

    /**
     * Set the crypt.
     *
     * @param \QP\Common\Service\CryptService $apiClient
     */
    public function setCrypt($crypt)
    {
        $this->crypt = $crypt;
    }

    /**
     * Get the elastic search manager.
     *
     * @return \Zend\Mvc\Controller\Plugin\FlashMessenger
     */
    public function getFlashMessenger()
    {
        return $this->flashMessenger;
    }

    /**
     * Set the flashMessenger.
     *
     * @param \Zend\Mvc\Controller\Plugin\FlashMessenger $flashMessenger
     */
    public function setFlashMessenger($flashMessenger)
    {
        $this->flashMessenger = $flashMessenger;
    }

    /**
     * Check if confirmation view should be rendered.
     *
     * @return int
     */
    public function getConfirmation()
    {
        return $this->confirmation;
    }

    /**
     * Set if the confirmation view should be rendered.
     *
     * @param $confirmation
     * @return $this
     */
    public function setConfirmation($confirmation)
    {
        $this->confirmation = $confirmation;
        return $this;
    }

    /**
     * @param $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * @return mixed
     */
    public function getLayout()
    {
        return $this->layout;
    }

    public function getBookmarkIds()
    {
        $user        = $this->handleUser([]);
        $bookmarks   = $this->getEntityManager()->getRepository(Bookmark::getEntityName())->readByUserId($user->getId());
        $bookmarkIds = [];
        foreach ($bookmarks as $bookmark) {
            $bookmarkIds[$bookmark->getPortalId()][$bookmark->getEntity()][$bookmark->getId()] = $bookmark->getEsId();
        }
        $this->bookmarkIds = $bookmarkIds;
    }

    /**
     * @param  string $entity
     * @param  array  $mainRoutes
     *
     * @return string
     */
    protected function getMainCategory($entity, $mainRoutes)
    {
        $main = '';
        if (strpos($entity, '/') !== false ) {
            $entityParts = explode('/',$entity);
            $entity      = $entityParts[1];
        }
        foreach ($mainRoutes as $mainRoute => $mainRouteConfig) {
            if ($mainRouteConfig['entity'] === $entity) {
                $main = $mainRoute;
            }
        }

        return $main;
    }

    protected function getPortalConfig($portal)
    {
        // -------------------------- check if there are news in the API ---
        $initCount = $this->getApiClient()->getRepository('QP\Common\Entity\Content')->getInitCount();
        $cache     = $this->getCacheAdapter();
        $success   = false;
        $result    = $cache->getItem('itemCount', $success);
        // if the cache can not find the key
        // if the api returns a different count the the cache
        if (!$success || ($initCount != $result)) {
            \FirePHP::getInstance(true)->warn('cache flushed');
            $cache->flush();
            $cache->setItem('itemCount', $initCount);
        }

        // ------------------------------------------ get the navigation ---
        // fetch nav data from cache or api
        $key      = md5('nav_'.$portal);
        $success  = false;
        /** @var $nav \QP\Common\Entity\Nav */
        $nav      = $cache->getItem($key, $success);
        if (!$success) {
            $nav = $this->getApiClient()->getRepository('QP\Common\Entity\Nav')->readOne($portal);
            $cache->setItem($key, $nav);
        }

        return [
            'nav'          => $nav,
            'aggWhiteList' => [
                'search/post'           => ["tags_p1","tags_sub","role","type"],
                'search/event'          => ["additionalType","tags_p1","origin","tags_sub","format","careerLevel","month","location","costType","language"],
                'search/education'      => ["additionalType","tags_p1","origin","tags_sub","format","careerLevel","month","location","costType","language"],
                'search/event-external' => ["additionalType","tags_p1","origin","tags_sub","format","careerLevel","month","location","costType","language"],
                'search/job'            => ["tags_p1","tags_sub","company","job_location","work_experience","work_hours","type_of_employment"],
                'search/person'         => ["tags_p1","tags_sub","role","tags_sector"],
                'search/company'        => ["tags_p1","tags_sub","tags_sector"],
                'search/all'            => ["index","tags_p1","language"],
                'search/bookmarks'      => ["index","month","language"],
            ],
            'tags' => [
                'Arbeitsrecht' => [
                    'AGG',
                    'Arbeitgeberverband',
                    'Arbeitnehmerüberlassung',
                    'Arbeitsvertrag',
                    'Arbeitszeugnis',
                    'Aufhebungsvertrag',
                    'betriebsrat',
                    'Datenschutz',
                    'Diksriminierung',
                    'Entlassungsmanagement',
                    'Kündigung',
                    'Mindestlohn',
                    'Mitbestimmungsgesetz',
                    'Sozialpartner',
                    'Tarifvertrag',
                    'Teilzeit',
                    'Urlaubsrecht',
                    'Werkvertrag',
                    'Zeitarbeit',
                ],
                'Change Management' => [
                    'Betriebsverlagerung',
                    'Change-Kommunikation',
                    'Merger & Acquisition',
                    'Restrukturierung',
                ],
                'Employer Branding' => [
                    'Arbeitgeberattraktivität',
                    'Arbeitgebermarke',
                    'Candidate Experience',
                    'Employer Value Proposition',
                    'Hochschulmarketing',
                    'Personalmarketing',
                    'Unternehmenskommunikation',
                ],
                'Entgelt' => [
                    'betriebliche Altersvorsorge',
                    'Lohnabrechnung',
                    'Variable Vergütung',
                ],
                'Future of Work' => [
                    'Arbeiten 4.0',
                    'Digital Workplace',
                    'Digitale Transformation',
                    'Mobile Working',
                ],
                'Gesundheitsmanagement' => [
                    'betriebliches Eingliederungsmanagment',
                    'Burnout',
                    'Fehlzeiten',
                    'Gefährdungsbeurteilung',
                    'Krankenrückkehrgespräch',
                    'Selbstverwirklichung',
                    'Work-Life-balance',
                ],
                'HR Technologie' => [
                    'big Data',
                    'Digitale Personalakte',
                    'enterprise content management',
                    'hr software',
                    'People Analytics',
                    'Social Intranet',
                ],
                'Leadership' => [
                    'Agiles Management',
                    'Collaborative Leadership',
                    'Digital Leadership',
                    'Führungsstile',
                    'Mitarbeiterführung',
                    'Motivation',
                    'Partizipation',
                ],
                'Organisationsentwicklung' => [
                    'Agilität',
                    'Arbeitsorganisation',
                    'Corporate Learning',
                    'Demokratisierung',
                    'Diversity',
                    'hr business Partner',
                    'Outplacement',
                    'Outsourcing',
                    'Unternehmenskultur',
                    'Wissensmanagement',
                ],
                'Performance Management' => [
                    'Benefits',
                    'Bonus',
                    'HR Compliance',
                    'HR-Kennzahlen',
                    'Personalcontrolling',
                ],
                'Personalentwicklung' => [
                    'Ausbildung',
                    'Coaching',
                    'Führungskräfteentwicklung',
                    'Gamification',
                    'Lernen',
                    'Mentoring',
                    'Mitarbeiterbefragung',
                    'Personalgespräch',
                    'Zeitmanagement',
                ],
                'Personalmanagement' => [
                    'Personalplanung',
                    'Personalstrategie',
                ],
                'Szene' => [],
                'Recruiting' => [
                    'Active Sourcing',
                    'Bewerbermanagement',
                    'Bewerbungsgespräch',
                    'Headhunter',
                    'Mobile Recruiting',
                    'Onboarding',
                    'Potenzialanalyse',
                    'Social Media Recruiting',
                    'Stellenanzeigen',
                ],
                'Talent Management' => [
                    'Collaboration',
                    'Entsendung',
                    'Mitarbeiterbindung',
                    'Talentförderung',
                ]
            ]
        ];
    }

    /**
     * @param  array  $config
     * @param  string $portalId
     *
     * @return string $html
*/
    protected function preparePage($config, $portalId)
    {
        //\FirePHP::getInstance(true)->info($config);
        $content    = $config['page']->getTemplateContent();
        $contentCount = count($content['modules']);

        for ($c = 0; $c < $contentCount; $c++) {
            $items['data'] = [];
            if (isset($content['modules'][$c]['items'])) {
                $itemCount = count($content['modules'][$c]['items']);
                for ($i = 0; $i < $itemCount; $i++) {
                    if (isset($content['modules'][$c]['items'][$i]['entity']) && isset($content['modules'][$c]['items'][$i]['query']) &&
                        $content['modules'][$c]['items'][$i]['entity']) {
                        $items = $this->getItems($content['modules'][$c]['items'][$i]['entity'], $content['modules'][$c]['items'][$i]['query'], $config['config']['tags'], $portalId);
                        //\FirePHP::getInstance(true)->info($items);
                        $content['modules'][$c]['items'][$i]['items'] = $items['data'];
                        if (isset($items['pagination'])) {
                            $content['modules'][$c]['items'][$i]['pagination'] = $items['pagination'];
                        }
                        if (isset($items['aggs'])) {
                            $content['modules'][$c]['items'][$i]['aggWhiteList'] = $config['config']['aggWhiteList'][$content['modules'][$c]['items'][$i]['entity']];
                            $content['modules'][$c]['items'][$i]['aggs'] = $items['aggs'];
                        }
                    }
                }
            } else {
                if (isset($content['modules'][$c]['entity']) && isset($content['modules'][$c]['query']) &&
                    $content['modules'][$c]['entity']) {
                    $items = $this->getItems($content['modules'][$c]['entity'], $content['modules'][$c]['query'], $config['config']['tags'], $portalId);
                    //\FirePHP::getInstance(true)->warn($items);
                    $content['modules'][$c]['items'] = $items['data'];
                    if (isset($items['pagination'])) {
                        $content['modules'][$c]['pagination'] = $items['pagination'];
                    }
                    if (isset($items['aggs'])) {
                        $content['modules'][$c]['aggWhiteList'] = $config['config']['aggWhiteList'][$content['modules'][$c]['entity']];
                        $content['modules'][$c]['aggs'] = $items['aggs'];
                    }
                }
            }
        }
        return $content;
    }

    /**
     * @param array $aggs
     * @param array $tagList
     *
     * @return array
     */
    private function cleanUpAggs($aggs, $tagList)
    {
        //\FirePHP::getInstance(true)->info($tagList);
        //\FirePHP::getInstance(true)->info($aggs);
        $allSubTags = [];
        foreach ($tagList as $subTags) {
            foreach ($subTags as $subTag) {
                $allSubTags[] = [
                    'key'       => $subTag,
                    'doc_count' => 0,
                ];
            }
        }

        $tagTemp = [];
        foreach ($aggs as $name => $aggList) {
            if ($name === 'tags_p1' || $name === 'tags_p2' || $name === 'tags_p3') {
                foreach ($aggList['buckets'] as $i => $bucket) {
                    if(isset($tagList[$bucket['key']])) {
                        if(isset($tagTemp[$bucket['key']])) {
                            $tagTemp[$bucket['key']]['doc_count'] = $tagTemp[$bucket['key']]['doc_count'] + $bucket['doc_count'];
                        } else {
                            $tagTemp[$bucket['key']] = [
                                'key'       => $bucket['key'],
                                'doc_count' => $bucket['doc_count'],
                            ];
                        }
                    }
                }
            }
        }
        $tagP1 = [];
        foreach ($tagTemp as $tag) {
            $tagP1[] = $tag;
        }
        $aggs['tags_p1']['buckets'] = $tagP1;
        $aggs['tags_sub']['buckets'] = $allSubTags;

        unset($aggs['tags_p2']);
        unset($aggs['tags_p3']);
        return $aggs;
    }

    /**
     * @param  string $entity
     * @param  string $query
     * @param  array  $tagList
     * @param  string $portalId
     *
     * @return mixed
     */
    protected function getItems($entity, $query, $tagList, $portalId)
    {
        if ($entity === 'search/education') {
            if($query === '') {
                $query = '?type=educationEvent';
            } else {
                $query .= '&type=educationEvent';
            }
        }
        if ($entity === 'search/event') {
            if($query === '') {
                $query = '?type=event';
            } else {
                $query .= '&type=event';
            }
        }
        if (strpos($query, 'html') === false ) {
            if (strpos($query, 'portalId=') === false ) {
                if($query === '') {
                    $query .= '?portalId='.$portalId;
                } else {
                    $query .= '&portalId='.$portalId;
                }
            }
        }
        if ($entity === 'search/event' || $entity === 'search/education') {
            if (strpos($query, 'origin') === false) {
                if($query === '') {
                    $query .= '?origin=internal';
                } else {
                    $query .= '&origin=internal';
                }
            }
        }
        if ($entity === 'search/event-external') {
            if (strpos($query, 'origin') === false) {
                if($query === '') {
                    $query .= '?origin=external';
                } else {
                    $query .= '&origin=external';
                }
            }
        }
//        if ($entity === 'search/all') {
//            $items = $this->getGlobalItems($query, $portalId);
//        } else
        if ($entity === 'search/bookmarks') {
            $items = $this->getBookmarkItems($query, $portalId);
        } else {
            $items = $this->getCacheAwareItems($entity, $query);
        }

        if (isset($items['aggs'])) {
            $items['aggs'] = $this->cleanUpAggs($items['aggs'], $tagList);
        }
//        \FirePHP::getInstance(true)->info($items);
//        \FirePHP::getInstance(true)->info($this->bookmarkIds);
//        \FirePHP::getInstance(true)->info($portalId);

        if (is_array($items['data'])) {
            foreach ( $items['data'] as $key => $item) {

                // add the bookmark flag when found in users bookmarks (has nothing to do with shop foo :)
                if (isset($this->bookmarkIds[$portalId][$items['data'][$key]['_index']]) &&
                    in_array($items['data'][$key]['_id'], $this->bookmarkIds[$portalId][$items['data'][$key]['_index']]))
                {
                    $items['data'][$key]['_isBookmarked'] = true;
                }

                /* Do shop stuff */
                // don't call the shop unless its a detail page
                if (strpos($query, 'html') !== false ) {
                    $shop_form_parameter = [
                        'root-directory'                 => 'vendor/qmb/core-tante-emma/',
                        'form-input-show-label'          => false,
                        'form-input-show-placeholder'    => true,
                        'alternative-template-directory' => 'module/QP/Hrm/view/qp/hrm/partials/shop-form-elements/',
                        'expand_frist_product_initially' => false,
                        'success-order'                  => 'https://example.com',
                    ];

                    $view = $this->viewRenderer->viewModel()->getView();
                    $translation_array = [
                        'delete'                     => $view->translate('SHOP_FORM_DELETE_PARTICIPANT'),
                        'add-person-text'            => $view->translate('SHOP_FORM_ADD_PARTICIPANT'),
                        'nth-person-hl'              => $view->translate('SHOP_FORM_PARTICIPANTS_HL'),
                        'submit-button-order'        => $view->translate('SHOP_FORM_ORDER_BUTTON_TITLE'),
                        'download-fax-pdf'           => $view->translate('SHOP_FORM_DOWNLOAD_FAX_PDF'),
                        'shopping-cart-button-title' => $view->translate('SHOP_FORM_SHOPPING_CART_BUTTON_TITLE')
                    ];

                    $shop_logger    = \TanteEmma\Logger\LoggerFactory::generateDevelopmentLogger();
                    $shop_config    = new \TanteEmma\Config($shop_logger, $shop_form_parameter);
                    $shop_connector = new \TanteEmma\Connector($shop_config);

                    $shop_response = $shop_connector->doKTRRequest( '1715105', 'shop2.hrm-forum.eu' );
                    $shop_data     = $shop_connector->getKTRData( $shop_response );
                    $shop_form     = $shop_connector->getForm( $shop_data, 'de', $translation_array );

                    // TODO: prepare prices and phases here not in every single template!
                    //$items['data'][$key]['shopData'] = $shop_data;
                    $items['data'][$key]['shopForm'] = $shop_form;
                }
                /* Shop stuff done. */
            }
        }

        return $items;
    }

    private function getBookmarkItems($query, $portalId)
    {
        //--------------------------------------------------------- defaults ---
        $items = [
            'function'   => 'bookmarks',
            'success'    => TRUE,
            'data'       => [],
            'aggs'       => [],
        ];
        $aggs = [];

        //--------------- get filter params from query or leave the defaults ---
        $filter     = ['from' => 0, 'size' => 15];
        $query      = str_replace('?', '', $query);
        $queryParts = explode('&',$query);
        foreach ($queryParts as $params) {
            $paramParts = explode('=',$params);
            $filter[$paramParts[0]] = $paramParts[1];
        }
        $allowedIndices = [];
        if (isset($filter['index']) && $filter['index']) {
            $allowedIndices = explode('%2C', $filter['index']);
        }

        //---- fetch the data form the API accordingly to the bookmarked IDs ---
        //\FirePHP::getInstance(true)->warn($this->bookmarkIds);
        foreach ($this->bookmarkIds[$portalId] as $bmEntity => $bmIds) {
            try {
                $aggs[$bmEntity] = 0;
                if(!count($allowedIndices) || in_array($bmEntity, $allowedIndices)) {
                    $bmQuery = '?ids='.implode(',', $bmIds).'&portalId='.$portalId;
                    $bmItems = $this->getCacheAwareItems($bmEntity, $bmQuery);
                    $bmItemCount     = count($bmItems['data']);
                    $aggs[$bmEntity] = $bmItemCount;
                    for ($i = 0; $i < $bmItemCount; $i++) {
                        if ($bmItems['data'][$i]['_index'] === $bmEntity) {
                            $items['data'][] = $bmItems['data'][$i];
                        }
                    }
                }
            }
            catch(\Exception $e){}
        }

        //---------------------- count the indices for the filter aggregates ---
        $total = 0;
        foreach ($aggs as $entityName => $entityCount) {
            $total = $total + $entityCount;
            $items['aggs']['index']['buckets'][] = [
                'key'       => $entityName,
                'doc_count' => $entityCount
            ];
        }

        //----- now take the pagination and apply them to the whole data set ---
        $paginatedItems = [];
        foreach ($items['data'] as $i => $item) {
            if($i >= $filter['from'] && $i < ($filter['from'] + $filter['size'])) {
                $paginatedItems[] = $items['data'][$i];
            }
        }
        $items['data'] = $paginatedItems;

        //---------------- get the real size and fetch the pagination object ---
        $size = count($items['data']);
        $items['pagination'] = $this->getPagination($total, $size, $filter);

        return $items;
    }

    /**
     * @param $entity
     * @param $query
     *
     * @return array
     */
    private function getCacheAwareItems($entity, $query)
    {
        \FirePHP::getInstance(true)->warn($entity.$query);

        $cache   = $this->getCacheAdapter();
        $key     = md5($entity.$query);
        $success = false;
        $items   = $cache->getItem($key, $success);
        //\FirePHP::getInstance(true)->info($items);
        if (!$success || $entity === 'job' || $entity === 'search/job') {
            $items = $this->getApiClient()->getRepository('QP\Common\Entity\Content')->query($entity, $query);
            //\FirePHP::getInstance(true)->warn($items);
            $cache->setItem($key, $items);
        }

        return $items;
    }

    /**
     * @param string $total
     * @param string $size
     * @param array  $filter
     *
     * @return array
     */
    protected function getPagination($total, $size, $filter)
    {
        $pagination['total'] = $total;
        $pagination['size']  = $size;

        if (isset($filter['from'])) {
            $pagination['from'] = (int)$filter['from'];
        } else {
            $pagination['from'] = 0;
        }
        if (isset($filter['size'])) {
            $pagination['to'] = (int)$pagination['from'] + (int)$pagination['size'] - 1;
            $pagination['desiredSize'] = (int)$filter['size'];
        } else {
            $pagination['to'] = $pagination['size'] - 1;
            $pagination['desiredSize'] = (int)$pagination['size'];
        }
        if ($pagination['from'] > 0) {
            $pagination['prev'] = $pagination['from'] - $pagination['desiredSize'];
            if ($pagination['prev'] < 0) {
                $pagination['prev'] = 0;
            }
        } else {
            $pagination['prev'] = 0;
        }
        if ((($pagination['from'] + $pagination['desiredSize'] - 1) <= $pagination['to']) &&
            isset($filter['size'])) {
            $pagination['next'] = $pagination['to'] + 1;
        } else {
            $pagination['next'] = 0;
        }

        if ($pagination['total'] !== -1 && isset($filter['size']) && $filter['size'] > 0) {
            $pageCount = ceil($pagination['total']/$filter['size']);
            $rest      = $pagination['total'];
            for($i = 0; $i < $pageCount; $i++) {
                //\FirePHP::getInstance(true)->info($rest);
                $amount = $filter['size'];
                if ($rest <= $filter['size']) {
                    $amount = $rest;
                }
                if($rest < 1) {
                    continue;
                }
                $pagination['pages'][$i]['from']   = (int)($pagination['total']-$rest);
                $pagination['pages'][$i]['amount'] = (int)$amount;
                $pagination['pages'][$i]['query']  = 'from='.$pagination['pages'][$i]['from'].'&size='.$pagination['desiredSize'];

                $rest = $rest - $filter['size'];
            }
        }

        return $pagination;
    }

    /**
     * @param  array  $pathParts
     * @param  array  $aggs
     * @param  string $q
     *
     * @return array
     */
    protected function matchParamsByAggs($pathParts, $aggs, $q)
    {
        $query      = '';
        $queryParts = [];
        foreach ($aggs as $aggName => $agg) {
            if($aggName === 'tags_sub') {
                $aggName = 'tags_p1';
            }
            foreach ($agg['buckets'] as $aggBucket) {
                if (in_array(CryptService::makeUrlWorthy($aggBucket['key']), $pathParts) ||
                    in_array($aggBucket['key'], $pathParts)) {
                    $queryParts[$aggName][] = urlencode($aggBucket['key']);
                    if(($key = array_search(CryptService::makeUrlWorthy($aggBucket['key']), $pathParts)) !== false) {
                        unset($pathParts[$key]);
                    }
                    if(($key = array_search($aggBucket['key'], $pathParts)) !== false) {
                        unset($pathParts[$key]);
                    }
                }
                if($aggName === 'month' && isset($aggBucket['key_as_string'])) {
                    $when        = new \DateTime($aggBucket['key_as_string'].' 00:00:00');
                    $currentYear = date('Y',time());
                    if (in_array(CryptService::makeUrlWorthy($this->viewRenderer->viewModel()->getView()->translate($when->format('F'))), $pathParts) &&
                       ($currentYear === $when->format('Y'))) {
                        $queryParts[$aggName][] = $aggBucket['key_as_string'];
                        if(($key = array_search(CryptService::makeUrlWorthy($this->viewRenderer->viewModel()->getView()->translate($when->format('F'))), $pathParts)) !== false) {
                            unset($pathParts[$key]);
                        }
                    }
                }
            }
        }
        foreach ($queryParts as $paramName => $params) {
            $query .= $paramName.'='.implode(',',$params).'&';
        }
        if (substr($query, -1)) {
            $query = substr($query, 0, -1);
        }

        $ret['hsc'] = 200;
        if (count($pathParts)) {
            $ret['hsc'] = 404;
            $notMatchingParts = implode(',', $pathParts);
            if ($q !== '') {
                $q .= ','.$notMatchingParts;
            } else {
                $q = $notMatchingParts;
            }
        }
        $ret['q'] = $q;
        if ($q !== '') {
            if ($query !== '') {
                $query .= '&q='.$q;
            } else {
                $query = 'q='.$q;
            }
        }
        if ($query !== '') {
            $query = '?'.$query.'&from=0&size=15';
        } else {
            $query = '?from=0&size=15';
        }

        $ret['query'] = $query;
        return $ret;
    }

    /**
     * @param $module
     * @return string containing class names for the template
     */
    protected function getModulesClasses( $module ) {
        $classes = [];
//        \FirePHP::getInstance(true)->info($module);
        $classes[] = $module['template'];

        if(!empty( $module['hidden' ])) {
            foreach( $module['hidden'] as $viewport ) {
                $classes[] = "hidden-" . $viewport;
            }
        }

        return implode( " ", $classes);
    }

    /**
     * @param  array $post
     * @param  bool  $cookieOnly
     *
     * @return null|User
     */
    public function handleUser($post, $cookieOnly = false)
    {
        /** @var User $user */
        $user = null;

        // check for the email address
        if (isset($post['email'])) {
            $user = $this->getEntityManager()
                ->getRepository(User::getEntityName())
                ->readOneByEmail($post['email']);
        }

        if (is_null($user)) {
            // check for cookie
            $user = $this->getEntityManager()
                ->getRepository(User::getEntityName())
                ->readOneByCookie();

            if (is_null($user)) {
                // check for session
                $sessionUser = $this->getAuthenticationService()->getIdentity();
                if (isset($sessionUser['tmp_user_hash'])) {
                    $user = $this->getEntityManager()
                        ->getRepository(User::getEntityName())
                        ->readOneByIdHash($sessionUser['tmp_user_hash']);
                }

                // if this user has no cookie or no user could be found by the
                // given email -> add a new empty user
                if (is_null($user)) {
                    $user = new User();
                    $userArray = [
                        'state'       => User::STATE_ACTIVE,
                        'sessionOnly' => 1,
                        'createDate'  => new \DateTime()
                    ];
                    if ($cookieOnly) {
                        $userArray['cookieOnly'] = $cookieOnly;
                        unset($userArray['sessionOnly']);
                    }

                    $user->exchangeArray($userArray);
                    $this->getEntityManager()->getRepository(\QP\User\Entity\User::getEntityName())->save($user);
                    $this->getEntityManager()->flush();
                    $user->createIdHash();
                    $this->getEntityManager()->getRepository(\QP\User\Entity\User::getEntityName())->save($user);
                    $this->getEntityManager()->flush();

                    $this->getAuthenticationService()->getStorage()->write(['tmp_user_hash' => $user->getIdHash()]);
                }
            }
        }
        // if not already set -> update the new information
        $user->exchangeArray($post);
        $this->getEntityManager()->getRepository(\QP\User\Entity\User::getEntityName())->save($user);
        $this->getEntityManager()->flush();

        return $user;
    }
}
