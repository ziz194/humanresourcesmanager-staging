<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\ViewLogic;

/**
 * An interface for view logics.
 *
 * @package QP\Common\ViewLogic
 */
interface ViewLogicInterface
{

    /**
     * Get the response view model
     *
     * @return \QP\Common\ViewLogic\Response\AbstractResponse
     */
    public function getResponse();
}
