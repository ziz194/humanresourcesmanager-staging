<?php
/**
 * Quadriga Plattform Webfrontend
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Validator to check for the minimum password policy.
 *
 * @author Andy Killat
 */
class PasswordPolicy extends AbstractValidator {

    const LENGTH  = 'length';
    const UPPER   = 'upper';
    const LOWER   = 'lower';
    const DIGIT   = 'digit';
    const SPECIAL = 'special';

    protected $messageTemplates = [
        self::LENGTH  => 'COMMON_PASSWORD_TO_SHORT',
        self::UPPER   => 'COMMON_PASSWORD_NO_UPPERCASE',
        self::LOWER   => 'COMMON_PASSWORD_NO_LOWERCASE',
        self::DIGIT   => 'COMMON_PASSWORD_NO_DIGIT',
        self::SPECIAL => 'COMMON_PASSWORD_NO_SPECIAL',
    ];

    public function isValid($value)
    {
        $this->setValue($value);

        $isValid = true;

        if (strlen($value) < 8) {
            $this->error(self::LENGTH);
            $isValid = false;
        }

        if (preg_match_all('/[A-Z]/', $value) < 2) {
            $this->error(self::UPPER);
            $isValid = false;
        }

        if (preg_match_all('/[a-z]/', $value) < 2) {
            $this->error(self::LOWER);
            $isValid = false;
        }

        if (preg_match_all('/[0-9]/', $value) < 2) {
            $this->error(self::DIGIT);
            $isValid = false;
        }

        if (preg_match_all('/[!@#$%^&*()\-_=+{};:,<.>]/', $value) < 1) {
            $this->error(self::SPECIAL);
            $isValid = false;
        }

        return $isValid;
    }
}