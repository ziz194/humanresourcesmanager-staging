<?php
/**
 * Quadriga Plattform Webfrontend
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Validator;

use QP\Common\Entity\Nav;
use Zend\Validator\AbstractValidator;

/**
 * Validator to check the maximum depth to add nodes to the nac.
 *
 * @author Andy Killat
 */
class MaxNavDepth extends AbstractValidator {

    const DEPTH = 'depth';

    protected $messageTemplates = [
        self::DEPTH => 'COMMON_NAV_LEVEL_DEPTH_EXCEEDED',
    ];

    public function isValid($value)
    {
        $this->setValue($value);

        $isValid = true;

        if (substr_count($value, '/') >= Nav::MaxDepth) {
            $message = sprintf($this->getTranslator()->translate('COMMON_NAV_LEVEL_DEPTH_EXCEEDED'), Nav::MaxDepth);
            $this->setMessage($message, self::DEPTH);
            $this->error(self::DEPTH);
            $isValid = false;
        }

        return $isValid;
    }
}