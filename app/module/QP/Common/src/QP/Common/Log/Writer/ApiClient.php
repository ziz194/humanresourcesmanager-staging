<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */
namespace QP\Common\Log\Writer;

use Zend\Log\Writer\AbstractWriter;

/**
 * Class ApiClient
 * @package QP\Common\Log\Writer
 */
class ApiClient extends AbstractWriter
{

    /**
     * @var \QP\Common\Service\ApiClientService
     */
    private $apiClient;

    /**
     * @var \QP\User\Entity\User
     */
    private $user;

    /**
     * @var \Zend\Mvc\I18n\Translator
     */
    private $translator;

    /**
     * @var string
     */
    private $routeName;

    /**
     * @var string
     */
    private $hostname;

    /**
     * ElasticSearch constructor.
     *
     * @param \QP\Common\Service\ApiClientService   $apiClient
     * @param \Zend\Mvc\I18n\Translator             $translator
     * @param \QP\User\Entity\User                  $user
     * @param string                                $routeName
     * @param string                                $hostname
     */
    public function __construct($apiClient, $translator, $user, $routeName, $hostname)
    {
        $this->apiClient   = $apiClient;
        $this->translator  = $translator;
        $this->routeName   = $routeName;
        $this->hostname    = $hostname;
        $this->user        = $user;
    }

    /**
     * Write to the log.
     *
     * @param array $event
     */
    protected function doWrite(array $event)
    {
        $logData = [
            'message'      => $this->translator->translate($event['message']),
            '@version'     => '1',
            '@timestamp'   => date('c'),
            'type'         => $event['message'],
            'priority'     => $event['priority'],
            'priorityName' => $event['priorityName'],
            'host'         => $this->hostname,
            'route'        => $this->routeName
        ];

        //\FirePHP::getInstance(true)->info($logData);
        // when the user is about to login, forgot or recover the password the
        // identity not set when this service is being initialized.
        // So in that case leave the user blank it will be set through the
        // $event['extra'] in the LoginLogic, ForgotLogic and RecoverLogic
        if (in_array($event['message'], ['AUTH_LOGGER_USER_LOGIN', 'AUTH_LOGGER_FORGOT_PASS', 'AUTH_LOGGER_USER_RECOVER_PASS'])) {
            $logData['user'] = '';
        } else {
            $logData['user'] = $this->user->getArrayCopy();
        }

        $logData = array_merge($logData, $event['extra']);

        $this->apiClient->create('log', $logData);
    }
}
