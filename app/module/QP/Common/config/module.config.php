<?php
/**
 * Quadriga Plattform WEB
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c] 2016 Quadriga Media GmbH
 */
namespace QP\Common;

return [
    //------------------------------------------------------------- Services ---
    'service_manager' => [
        'abstract_factories' => [
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ],
        'factories' => [
            'Crypt'                               => 'QP\Common\Service\CryptService',
            'Tracker'                             => 'QP\Common\Service\Tracker',
            'ApiClient'                           => 'QP\Common\Service\ApiClientService',
            'LogService'                          => 'QP\Common\Service\LogService',
            'navigation'                          => 'Zend\Navigation\Service\DefaultNavigationFactory',
            'ErrorLogger'                         => 'QP\Common\Service\ErrorLogger',
            'EntityManager'                       => 'QP\Common\Service\EntityManagerService',
            'ApiClientManager'                    => 'QP\Common\Service\ApiClientManagerService',
            'ElasticSearchManager'                => 'QP\Common\Service\ElasticSearchManagerService',
            'Zend\Session\SessionManager'         => 'Zend\Session\Service\SessionManagerFactory',
            'Zend\Session\Config\ConfigInterface' => 'Zend\Session\Service\SessionConfigFactory',
            'Zend\Cache\Storage\Factory' => function() {
                return \Zend\Cache\StorageFactory::factory([
                        'adapter' => [
                            'name'    => 'filesystem',
                            'options' => [
                                'dirLevel'           => 2,
                                'cacheDir'           => 'data/cache',
                                'dirPermission'      => 0755,
                                'filePermission'     => 0666,
                                'namespaceSeparator' => '-db-'
                            ],
                        ],
                        'plugins' => ['serializer'],
                    ]
                );
            }
        ],
        'aliases' => [
            'translator' => 'MvcTranslator',
        ],
        'invokables' => [
            'ViewLogicFactory' => '\QP\Common\ViewLogic\ViewLogicFactory',
            'FormFactory'      => '\QP\Common\Form\FormFactory',
        ]
    ],
    //---------------------------------------------------------- Controllers ---
    'controllers' => [
        'invokables' => [
            'QP\Common\Controller\Ajax'  => 'QP\Common\Controller\AjaxController',
            'QP\Common\Controller\Form'  => 'QP\Common\Controller\FormController',
            'QP\Common\Controller\Index' => 'QP\Common\Controller\IndexController',
        ],
    ],
    //---------------------------------------------------------------- Views ---
    'view_helpers'  => [
        'invokables'=> [
            // general view helper
            'formErrors'     => 'QP\Common\View\Helper\FormErrors',
            'systemMessage'  => 'QP\Common\View\Helper\SystemMessage',
            'makeUrlWorthy'  => 'QP\Common\View\Helper\MakeUrlWorthy',
            'ndPartial'      => 'QP\Common\View\Helper\NdPartial',
            'getAggList'     => 'QP\Common\View\Helper\GetAggList',
            // specific view helper
            'myCaptcha'      => 'QP\Common\View\Helper\Custom\Captcha\ViewHelperCaptcha',
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
//        'not_found_template'       => 'error/404',
        'not_found_template'       => 'qp/common/error/404',
        'exception_template'       => 'error/index',
        'template_map'             => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'controller_map' => [
            'QP\Common' => true,
        ],
    ],
    //--------------------------------------------------------------- Routes ---
    'router' => [
        'routes' => [
            'error' => [
                'type'    => 'Zend\Mvc\Router\Http\Literal',
                'options' => [
                    'route'    => '/error',
                    'defaults' => [
                        'controller' => 'QP\Common\Controller\Index',
                        'action'     => 'error',
                    ],
                ],
            ],
            'white-rabbit' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/ajax/white-rabbit',
                    'defaults' => [
                        'controller' => 'QP\Common\Controller\Ajax',
                        'action'     => 'follow',
                    ],
                ],
            ],
            'bookmark' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/ajax/bookmark',
                    'defaults' => [
                        'controller' => 'QP\Common\Controller\Ajax',
                        'action'     => 'bookmark',
                    ],
                ],
            ],
            'newsletter-send' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/newsletter/send',
                    'defaults' => [
                        'controller' => 'QP\Common\Controller\Form',
                        'action'     => 'newsletter',
                    ],
                ],
            ],
            'kontakt-send' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/kontakt/send',
                    'defaults' => [
                        'controller' => 'QP\Common\Controller\Form',
                        'action'     => 'kontakt',
                    ],
                ],
            ],
            'events-melden-send' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/events/melden/send',
                    'defaults' => [
                        'controller' => 'QP\Common\Controller\Form',
                        'action'     => 'eventSubmit',
                    ],
                ],
            ],
            'looking-glass' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/looking-glass/:scope[/:date]',
                    'defaults' => [
                        'controller' => 'QP\Common\Controller\Index',
                        'action'     => 'lookingGlass',
                    ],
                ],
            ],
        ],
    ],
    //------------------------------------------------------------- Doctrine ---
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/' . str_replace('\\', '/', __NAMESPACE__) . '/Entity'
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ],
    //----------------------------------------------------------- Translator ---
    'translator' => [
        'locale' => 'de_DE',
        'translation_file_patterns' => [
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php'
            ],
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../../../../vendor/zendframework/zend-i18n-resources/languages/',
                'pattern'  => '%s/Zend_Captcha.php'
            ],
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../../../../vendor/zendframework/zend-i18n-resources/languages/',
                'pattern'  => '%s/Zend_Validate.php'
            ]
        ],
    ],
];
