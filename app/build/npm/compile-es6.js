/**
 * cli call: webpack --config webpack-hrm.config.js
 */
'use strict';

if ( typeof process.env.npm_package_config_project === 'undefined' || process.env.npm_package_config_project === 'undefined' ) {
    console.error( "PROJECT IS NOT DEFINED" );
    console.error( "For more information, see readme.md" );
    return false;
}

const project = process.env.npm_package_config_project;

const webpack = require( "webpack" );
const fs = require( 'fs' );
const path = require( 'path' );
const input = `./src/${project}/es6`;
const output = `../public/${project}/js`;

const compiler = webpack( {
    entry:   input + '/app.js',
    output:  {
        path:              output,
        filename:          'app.js',
        sourceMapFilename: 'app.js.map'
    },
    devtool: 'source-map',
    module:  {
        loaders: [
            {
                test:    path.join( __dirname, input ),
                exclude: /(node_modules|bower_components)/,
                loader:  'babel-loader',
                query:   {
                    presets: [ 'es2015' ]
                }
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin( {
            jQuery: 'jquery',
            $:      'jquery',
            jquery: 'jquery'
        } )
    ]
} );

compiler.run( function ( error, stats ) {
    if ( error ) {
        console.log( error );
    } else {
        console.log( 'if' );
        if ( stats.hasErrors() ) {
            console.log( stats.toString( "errors-only" ) );
            console.log( stats.toString( {
                errorDetails: true
            } ) );
        } else if ( stats.hasWarnings() ) {
            console.log( stats.toString( "minimal" ) );
        } else {
            console.log( stats.toString( {
                timings: true,
                hash:    true,
                modules: true,
                chunks:  false
            } ) );
        }
    }
} );