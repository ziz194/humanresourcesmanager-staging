/**
 * cli call: npm-run-all --parallel build:*
 */
'use strict';

const runAll = require( "npm-run-all" );

runAll( [ "build:*" ], {
    parallel:        true,
    silent:          true,
    continueOnError: true,
    printLabel:      true,
    printName:       true,
} )
    .then( results => {
        console.log( "done!" );
        results.forEach( result => {
            console.log( `${result.name}: ${result.code}` )
        } );
    } )
    .catch( err => {
        console.log( "failed!" );
        console.log( err );
    } );
