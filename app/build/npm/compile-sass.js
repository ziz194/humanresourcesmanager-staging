/**
 * cli call: node-sass --output-style compressed --source-map true src/hrm/sass/style.scss ../public/hrm/css/style.css
 */
'use strict';

if ( typeof process.env.npm_package_config_project === 'undefined' || process.env.npm_package_config_project === 'undefined' ) {
    console.error( "PROJECT IS NOT DEFINED" );
    console.error( "For more information, see readme.md" );
    return false;
}

const project = process.env.npm_package_config_project;

const sass = require( 'node-sass' );
const fs = require( 'fs' );
const input = `src/${project}/sass/style.scss`;
const output = `../public/${project}/css`;

sass.render( {
        file:              input,
        outFile:           `${output}/style.css`,
        sourceMap:         true,
        sourceMapEmbed:    false,
        sourceMapContents: true,
        sourceMapRoot:     `${output}/style.css.map`,
        outputStyle:       'compressed'
    },
    function ( error, result ) { // node-style callback from v3.0.0 onwards
        if ( !error ) {
            // No errors during the compilation, write this result on the disk

            // check directory
            if ( !fs.existsSync( output ) ) {
                fs.mkdirSync( output );
            }

            // write out css files
            fs.writeFile( `${output}/style.css`, result.css, function ( err ) {
                if ( err ) {
                    console.log( err );
                }
            } );

            // write out sourcemap
            fs.writeFile( `${output}/style.css.map`, result.map, function ( err ) {
                if ( err ) {
                    console.log( err );
                }
            } );

            //console.log( result.stats.includedFiles );
            console.log( 'Entry : ', result.stats.entry );
            console.log( 'Duration: ', result.stats.duration );
        } else {

            fs.writeFile( `${output}/style.css`, error, function ( err ) {
                if ( err ) {
                    console.log( err );
                }
            } );

            console.log( '#############################################' );
            console.log( '################### ERROR ###################' );
            console.log( '#############################################' );
            console.log( '### File: ' + error.file );
            console.log( '### Line: ' + error.line );
            console.log( '### Column: ' + error.column );
            console.log( '### ' + error.message );
            console.log( '#############################################' );
            throw new Error( error );
        }
    }
);