/**
 * cli call: npm-run-all --parallel watch:*
 */
'use strict';

const runAll = require( "npm-run-all" );

runAll( [ "watch:sass", "watch:es6" ], {
    parallel:        true,
    silent:          true,
    continueOnError: true,
    printLabel:      true,
    printName:       true,
} )
    .then( ( results ) => {
        console.log( "done!" );
        console.log( results );

        results.forEach( result => {
            console.log( `${result.name}: ${result.code}` )
        } );
    } )
    .catch( err => {
        console.log( "failed!" );
        console.log( err );
    } );
