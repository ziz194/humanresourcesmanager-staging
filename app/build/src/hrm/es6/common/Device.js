/*!
 * Device.js
 * (c) 2014 Matthew Hudson
 * Device.js is freely distributable under the MIT license.
 * For all details and documentation:
 * http://matthewhudson.me/projects/device.js/
 *
 * Device.js makes it easy to write conditional CSS and/or JavaScript based on
 * device operating system (iOS, Android, Blackberry, Windows, Firefox OS, MeeGo),
 * orientation (Portrait vs. Landscape), and type (Tablet vs. Mobile).
 *
 * @see https://github.com/matthewhudson/device.js
 *
 * modified to ES6 module by Marcus Merchel <kontakt@marcusmerchel.de>
 */

'use strict';
export class Device {

    constructor() {
        window.device = {};

        this._orientation_event = '';
        this._supports_orientation = '';
        this.previousDevice = window.device;
        this._doc_element = window.document.documentElement;
        this._user_agent = window.navigator.userAgent.toLowerCase();

        this.checkDevice();
    };

    checkDevice() {
        if ( this.ios() ) {
            if ( this.ipad() ) {
                this._addClass( 'ios ipad tablet' );
            } else if ( this.iphone() ) {
                this._addClass( 'ios iphone mobile' );
            } else if ( this.ipod() ) {
                this._addClass( 'ios ipod mobile' );
            }
        } else if ( this.android() ) {
            if ( this.androidTablet() ) {
                this._addClass( 'android tablet' );
            } else {
                this._addClass( 'android mobile' );
            }
        } else if ( this.blackberry() ) {
            if ( this.blackberryTablet() ) {
                this._addClass( 'blackberry tablet' );
            } else {
                this._addClass( 'blackberry mobile' );
            }
        } else if ( this.windows() ) {
            if ( this.windowsTablet() ) {
                this._addClass( 'windows tablet' );
            } else if ( this.windowsPhone() ) {
                this._addClass( 'windows mobile' );
            } else {
                this._addClass( 'desktop' );
            }
        } else if ( this.fxos() ) {
            if ( this.fxosTablet() ) {
                this._addClass( 'fxos tablet' );
            } else {
                this._addClass( 'fxos mobile' );
            }
        } else if ( this.meego() ) {
            this._addClass( 'meego mobile' );
        } else {
            this._addClass( 'desktop' );
        }

        if ( this.cordova() ) {
            this._addClass( 'cordova' );
        }

        this._supports_orientation = 'onorientationchange' in window;

        this._orientation_event = this._supports_orientation ? 'orientationchange' : 'resize';

        const callback = this._handleOrientation();
        if ( window.addEventListener ) {
            window.addEventListener( this._orientation_event, callback, false );
        } else if ( window.attachEvent ) {
            window.attachEvent( this._orientation_event, callback );
        } else {
            window[ this._orientation_event ] = callback;
        }

        this._handleOrientation();
    };

    ios() {
        return this.iphone() || this.ipod() || this.ipad();
    };

    iphone() {
        return this._find( 'iphone' );
    };

    ipod() {
        return this._find( 'ipod' );
    };

    ipad() {
        return this._find( 'ipad' );
    };

    android() {
        return this._find( 'android' );
    };

    androidPhone() {
        return this.android() && this._find( 'mobile' );
    };

    androidTablet() {
        return this.android() && !this._find( 'mobile' );
    };

    blackberry() {
        return this._find( 'blackberry' ) || this._find( 'bb10' ) || this._find( 'rim' );
    };

    blackberryPhone() {
        return this.blackberry() && !this._find( 'tablet' );
    };

    blackberryTablet() {
        return this.blackberry() && this._find( 'tablet' );
    };

    windows() {
        return this._find( 'windows' );
    };

    windowsPhone() {
        return this.windows() && this._find( 'phone' );
    };

    windowsTablet() {
        return this.windows() && this._find( 'touch' ) && !this._find( 'phone' );
    };

    fxos() {
        return (this._find( '(mobile;' ) || this._find( '(tablet;' )) && this._find( '; rv:' );
    };

    fxosPhone() {
        return this.fxos() && this._find( 'mobile' );
    };

    fxosTablet() {
        return this.fxos() && this._find( 'tablet' );
    };

    meego() {
        return this._find( 'meego' );
    };

    mobile() {
        return this.androidPhone() || this.iphone() || this.ipod() || this.windowsPhone() || this.blackberryPhone() || this.fxosPhone() || this.meego();
    };

    tablet() {
        return this.ipad() || this.androidTablet() || this.blackberryTablet() || this.windowsTablet() || this.fxosTablet();
    };

    desktop() {
        return !this.tablet() && !this.mobile();
    };

    cordova() {
        return window.cordova && location.protocol === 'file:';
    };

    portrait() {
        return (window.innerHeight / window.innerWidth) > 1;
    };

    landscape() {
        return (window.innerHeight / window.innerWidth) < 1;
    };

    noConflict() {
        window.device = this.previousDevice;
        return this;
    };

    _find( needle ) {
        return this._user_agent.indexOf( needle ) !== -1;
    };

    _hasClass( class_name ) {
        let regex;
        regex = new RegExp( class_name, 'i' );
        return this._doc_element.className.match( regex );
    };

    _addClass( class_name ) {
        if ( !this._hasClass( class_name ) ) {
            return this._doc_element.className += ' ' + class_name;
        }
    };

    _removeClass( class_name ) {
        if ( this._hasClass( class_name ) ) {
            this._doc_element.className = this._doc_element.className.replace( class_name, '' );
        }
    };

    _handleOrientation() {
        if ( this.landscape() ) {
            this._removeClass( "portrait" );
            this._addClass( "landscape" );
        } else {
            this._removeClass( "landscape" );
            this._addClass( "portrait" );
        }
        return;
    };
}

export let device = new Device();