import { find, each } from 'lodash';

'use strict';
export class Bookmark {

    constructor()
    {
        //console.log('bookmark');
        this.initEventListeners();
        $(document).on('filterListUpdated', () => this.initEventListeners());

    };

    initEventListeners()
    {
        $('.interaction__icons__bookmark').on('click', (e) => this.bookmarkMe(e));
    }

    bookmarkMe(e)
    {
        e.preventDefault();

        let bookmark = {};

        // already bookmarked
        if (e.currentTarget.classList.contains('bookmarked')) {
            e.currentTarget.classList.remove('bookmarked');
            bookmark.addBookmark = false;

        // needs to be bookmarked
        } else {
            e.currentTarget.classList.add('bookmarked');
            bookmark.addBookmark = true;
        }
        bookmark.portal = e.currentTarget.dataset.portal;
        bookmark.entity = e.currentTarget.dataset.entity;
        bookmark.esId   = e.currentTarget.dataset.esId;

        $.post( {
            url:      "/ajax/bookmark",
            data:     bookmark,
            dataType: "json"
        });
    }
}

export let bookmark = new Bookmark();