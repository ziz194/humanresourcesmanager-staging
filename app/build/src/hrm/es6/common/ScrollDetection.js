/**
 * ScrollDetection.js
 *
 * Detects page scrollings and adds some additionalbody classes to do some animations with header or something else
 *
 * @see https://medium.com/@mariusc23/hide-header-on-scroll-down-show-on-scroll-up-67bbaae9a78c#.f0jjs3z4v
 */

'use strict';
export class ScrollDetection {

    constructor() {
        this.didScroll = false;
        this.lastScrollTop = 0;
        this.delta = 15;
        this.navbarHeight = $( '.header-fixed-container' ).outerHeight();

        this.addEventListener();
    }

    addEventListener() {
        // on scroll, let the interval function know the user has scrolled
        $( window ).scroll( ( event ) => {
            this.didScroll = true;
        } );

        // run hasScrolled() and reset didScroll status
        setInterval( () => {
            if ( this.didScroll ) {
                this.hasScrolled();
                this.didScroll = false;
            }
        }, 250 );
    }

    hasScrolled() {
        const scrollTop = $( document ).scrollTop();

        // Make sure they scroll more than delta
        if ( Math.abs( this.lastScrollTop - scrollTop ) <= this.delta ) {
            return;
        }

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if ( scrollTop > this.lastScrollTop && scrollTop > this.navbarHeight ) {
            // Scroll Down
            $( 'body' ).removeClass( 'scroll-up' ).addClass( 'scroll-down' );
        } else {
            // Scroll Up
            if ( scrollTop + $( window ).height() < $( document ).height() ) {
                $( 'body' ).removeClass( 'scroll-down' ).addClass( 'scroll-up' );
            }
        }

        this.lastScrollTop = scrollTop;
    }
}

export let scrollDetection = new ScrollDetection();