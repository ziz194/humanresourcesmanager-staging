export class Common {
    constructor() {
    }

    germanDateToDBDate(dateString) {
        let dateArray = dateString.split(".");
        return dateArray[2]+'-'+dateArray[1]+'-'+dateArray[0];
    }
}

export let common = new Common();