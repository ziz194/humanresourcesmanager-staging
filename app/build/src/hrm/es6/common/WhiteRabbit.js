/**
 * Add lpad function to all Strings. This function will add a padding string left to a given string.
 * Example: ("000").toString().lpad("1", 6) => "111000"
 *
 * @param string pad String to be attached preceding the given string.
 * @param number length Length of the string with padding.
 * @return string New string with length of <length> with preceding (repeated) <pad>.
 */
if (!String.prototype.lpad) {
    String.prototype.lpad = function(pad, length) {
        let s = this;
        while (s.length < length)
            s = pad + s;
        return s;
    }
}
import { find, each } from 'lodash';

'use strict';
export class WhiteRabbit {

    constructor()
    {
        //console.log('follow the white rabbit');
        $('.no-cookie-button .button').on('click', (e) => this.follow(e));
    };

    /**
     * follow function.
     */
    follow(e)
    {
        e.preventDefault();

        let wr  = {};
        let d   = new Date();
        let dpi = this.getDPI();

        wr.systemColors            = this.getSystemColors();
        wr.document_referrer	   = this.getProperty("document.referrer");
        wr.navigator_appCodeName   = this.getProperty("navigator.appCodeName");
        wr.navigator_appName	   = this.getProperty("navigator.appName");
        wr.navigator_appVersion	   = this.getProperty("navigator.appVersion");
        wr.navigator_cookieEnabled = (this.getProperty("navigator.cookieEnabled") == true ? 1 : 0);
        wr.navigator_language	   = this.getProperty("navigator.language || navigator.browserLanguage");
        wr.navigator_platform	   = this.getProperty("navigator.platform");
        wr.navigator_userAgent	   = this.getProperty("navigator.userAgent");
        wr.screen_width			   = this.getProperty("screen.width");
        wr.screen_height		   = this.getProperty("screen.height");
        wr.screen_avail_width	   = this.getProperty("screen.availWidth");
        wr.screen_avail_height	   = this.getProperty("screen.availHeight");
        wr.color_depth			   = this.getProperty("(screen.colorDepth ? screen.colorDepth : screen.pixelDepth)");
        wr.devicePixelRatio		   = this.getProperty("window.devicePixelRatio * 100");
        wr.java_enabled			   = (this.getProperty("navigator.javaEnabled()") == true ? 1 : 0);
        wr.dpi_x				   = dpi[0];
        wr.dpi_y				   = dpi[1];
        wr.timezone_offset		   = d.getTimezoneOffset();
        wr.plugin_adobe_acrobat	   = this.getAdobeAcrobatVersion();
        wr.mimetypes			   = this.getMimeTypes();
        wr.plugins				   = this.getPlugins();

        //console.log(wr);

        // start ajax request
        this.xhr = $.post( {
            url:      "/ajax/white-rabbit",
            data:     wr,
            dataType: "json"
        });
        $('.no-cookie').remove();
    }
    /**
     * getProperty function.
     *
     * Evaluates given parameter. This function is a helper function only and fetches potential errors, if a property does not exist.
     *
     * @param {string} prop Expression (property) to be evaluated.
     * @return {string} The result of the evaluated statement.
     */
    getProperty(prop) 
    {
        try {
            return eval(prop);
        } catch (e) {
            return null;
        }
    }

    /**
     * getAdobeAcrobatVersion function.
     *
     * Retrieves the version number of an installed Adobe Reader/Acrobat plugin.
     * On how to detect a plugin in Internet Explorer, see also: http://www.matthewratzloff.com/blog/2007/06/26/detecting-plugins-in-internet-explorer-and-a-few-hints-for-all-the-others/
     *
     * @return {?string} Version number in case of Adobe Reader/Acrobat can be found, NULL otherwise.
     */
    getAdobeAcrobatVersion()
    {
        /** If browser is ActiveX-capable (Internet Explorer)... */
        if (window.ActiveXObject) {
            let obj = null;

            try {
                /** New versions of Acrobat/Reder **/
                obj = new ActiveXObject('AcroPDF.PDF');
            } catch(e) {

            }

            if (!obj) {
                try {
                    /** Try to check for old versions, if first approach failed. **/
                    obj = new ActiveXObject('PDF.PdfCtrl');
                } catch (e) {
                    return null;
                }
            }

            /** If one of the approached succeeded, try to get the version number. */
            if (obj) {
                let version = obj.GetVersions().split(',');
                version = version[0].split('=');
                version = parseFloat(version[1]);
                return version;
            } else {
                return null;
            }
            /** Try to get version number the non-ActiveX-way. **/
        } else {
            for (let i = 0; i < navigator.plugins.length; i++) {
                if (navigator.plugins[i].name.indexOf('Adobe Acrobat') != -1) {
                    return navigator.plugins[i].description.replace(/\D+/g, ".").match(/^.?(.+),?$/)[1];
                }
            }
            return null;
        }

    }

    /**
     * getPlugins function.
     *
     * Retrieves all installed plugins. Works in all browsers that support navigator.plugins, i.e. all except Internet Explorer.
     *
     * @return {?Array.<string>} Array of all installed plugins (or null if they cannot be retrieved); elements (Strings) format: <plugin name>: <plugin description> (<plugin filename>)
     */
    getPlugins()
    {
        let a = [];

        try {
            for (let i = 0; i < navigator.plugins.length; i++) {
                a.push(navigator.plugins[i].name + ': ' + navigator.plugins[i].description + ' (' + navigator.plugins[i].filename + ')');
            }
            return a;
        } catch (e) {
            return null;
        }
    }


    /**
     * getMimeTypes function.
     *
     * Retrieves all supported Mime types. Works in all browsers that support navigator.mimeTypes, i.e. all except Internet Explorer.
     *
     * @return {?Array.<string>} Array of all supported Mime types (or null if they cannot be retrieved); elements (Strings) format: <mimeType type>: <mimeType description>
     */
    getMimeTypes()
    {
        let a = [];

        try {
            for (let i = 0; i < navigator.mimeTypes.length; i++) {
                a.push(navigator.mimeTypes[i].type + ': ' + navigator.mimeTypes[i].description);
            }
            return a;
        } catch (e) {
            return null;
        }
    }

    /**
     * getDPI function.
     *
     * Calculates User's DPI setting (experimental).
     * I found this script here:
     * http://stackoverflow.com/questions/476815/can-you-access-screen-displays-dpi-settings-in-a-javascript-function
     * During my testing period it always returned 96, let's see whether it returns something else on other computers.
     *
     * @return {?Array.<number>} DPI-Setting, [0]: width, [1]: height. In case of error, return (0,0).
     */
    getDPI()
    {
        let dpi = new Array(2);

        /** Create an empty DIV container, width = height = 1 inch */
        try {
            let div = document.createElement('div');
            div.style.position = 'absolute';
            div.style.left = '-100%';
            div.style.top = '-100%';
            div.style.width = '1in';
            div.style.height = '1in';
            div.id = 'dpi';
            document.getElementsByTagName('body')[0].appendChild(div);
        } catch (e) {
            document.write('<div id="dpi" style="position:absolute; left:-100%; top:-100%; width:1in; height:1in"></div>');
        }

        /** Get real pixel width / height */
        try {
            dpi[0] = document.getElementById('dpi').offsetWidth;
            dpi[1] = document.getElementById('dpi').offsetHeight;
            return dpi;
        } catch (e) {
            return [0, 0];
        }
    }

    /**
     * getSystemColors function.
     *
     * This function tries to get the system colors of the user's OS. These include the color for windows (usually white) or Desktop background color.
     * The function tries to create an empty DIV object and sets the background color to a system color. Subsequently, the RGB values is read by
     * JavaScript's function getComputedValue.
     *
     * @return {?Object.<string, string>} Associative array ("System Color Keyword" => hex value with preceding '#'), NULL if RGB values cannot be retrieved.
     */
    getSystemColors()
    {
        /** All system colors specified by W3C, see: http://www.w3.org/TR/css3-color/#css-system */
        let colors = ['ActiveBorder', 'ActiveCaption', 'AppWorkspace', 'Background', 'ButtonFace', 'ButtonHighlight', 'ButtonShadow', 'ButtonText',
            'CaptionText', 'GrayText', 'Highlight', 'HighlightText', 'InactiveBorder', 'InactiveCaption', 'InactiveCaptionText',
            'InfoBackground', 'InfoText', 'Menu', 'MenuText', 'Scrollbar', 'ThreeDDarkShadow', 'ThreeDFace', 'ThreeDHighlight',
            'ThreeDLightShadow', 'ThreeDShadow', 'Window', 'WindowFrame', 'WindowText'];

        /** Results, saved in an associative array */
        let obj = {};

        /** Try to create an empty DIV object */
        try {
            let div = document.createElement('div');
            div.style.display = 'block';
            div.style.visibility = 'hidden';
            div.style.position = 'absolute';
            div.style.left = '-100px';
            div.style.top = '-100px';
            div.style.width = '5px';
            div.style.height = '5px';
            div.style.backgroundColor = '#000000';
            div.id = 'syscolor';
            document.getElementsByTagName('body')[0].appendChild(div);
        } catch (e) {
            document.write('<div id="syscolor" style="display:block; visibility:hidden"></div>');
        }

        try {
            let div = document.getElementById('syscolor');

            /** Regular expression, detect CSS-style RGB color information like 'rgb(255,255,0)' */
            let re = /rgb\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})\)/;
            let c;

            /** Identifier is used for names in associative array obj. These match the names of coloumns in the MySQL table (system color with 'color_' prefix). */
            let identifier;

            for (let i = 0; i < colors.length; i++) {
                /** Set background color to one of the system colors */
                div.style.backgroundColor = colors[i];

                identifier = 'color_' + colors[i].toString().toLowerCase();

                /** Different methods of getting real RGB values */
                if (div.currentStyle) {	/** Browser that supports 'div.currentStyle' (Internet Explorer */
                    c = div.currentStyle['backgroundColor'];
                } else { /** Other browsers */
                    c = document.defaultView.getComputedStyle(div, null).getPropertyValue('background-color');
                }

                /**
                 * Check if retrieved color is not null and is not the name of the system color.
                 * When run in Internet Explorer most versions only return the name of the system color (e.g. 'ActiveBorder'),
                 * so we cannot retrieve real RGB values.
                 */
                if (c != null && c.toString().toLowerCase() != colors[i].toLowerCase()) {
                    c = c.toString();

                    /** Check if returned value is hex-formatted (e.g. #ffff00) or in 'rgb(r,g,b)' format */
                    let results = re.exec(c);

                    if (results != null) {
                        /** 'rgb(r,g,b)' format: Use return values and shift them bitwise to create hex-format */
                        //obj[identifier] = ('#' + (parseInt(results[1]) << 16 | parseInt(results[2]) << 8 | parseInt(results[3])).toString(16)).toString();
                        obj[identifier] = '#' + (parseInt(results[1])).toString(16).lpad("0", 2) + (parseInt(results[2])).toString(16).lpad("0", 2) + (parseInt(results[3])).toString(16).lpad("0", 2);

                    } else {
                        /** Probably hex format: Check if color information already includes a hash at the beginning. If not, add it. */
                        if (c.substr(0, 1) != '#')
                            c = '#' + c;

                        /** Check if string does not contain more than 7 characters. If so, it is probably a well formated hex-coded color information. */
                        if (c.length <= 7)
                            obj[identifier] = c.toString();
                    }
                } else {
                    /** Color information cannot be retrieved. */
                    obj[identifier] = null;
                }
            }
            return obj;
        } catch(e) {
            return obj;
        }
    }
}

export let whiteRabbit = new WhiteRabbit();