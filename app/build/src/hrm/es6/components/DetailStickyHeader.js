export class DetailStickyHeader {

    constructor() {
        this.elem = $( '.detail__sticky-header' );

        if ( this.elem.length > 0 ) {
            this.setHeaderElements();
        }
    }

    setHeaderElements() {
        const stageItems = $( '.stage-items' );
        const mainInfo = $( '.detail__main-info' );
        const headline = stageItems.find( '.stage-items__headline' );
        const price = mainInfo.find( '.detail__main-info__price' );
        const button = mainInfo.find( '.detail__main-buttons' );
        const social = mainInfo.find( '.interaction' );

        // add/move elememnts
        this.elem.find( '.detail__sticky-header__headline' ).html( headline.clone() ).find( 'h1' );
        this.elem.find( '.detail__sticky-header__social' ).html( social.clone() );
        this.elem.find( '.detail__sticky-header__price' ).html( price.clone() )
            .find( '.detail__main-info__price' )
            .removeClass( 'detail__main-info__col' );
        this.elem.find( '.detail__sticky-header__button' ).html( button.clone() );

        this.addEventListener();
    }

    addEventListener() {
        this.elem.on( 'affix.bs.affix', () => {
            console.log( 'affix.bs.affix' );
            this.calculateTextWidth();
        } );
    }

    calculateTextWidth() {
        const asideElementWidth = parseInt( this.elem.find( '.detail__sticky-header__button' ).outerWidth( true ), 10 )
            + parseInt( this.elem.find( '.detail__sticky-header__price' ).outerWidth( true ), 10 )
            + parseInt( this.elem.find( '.detail__sticky-header__social' ).outerWidth( true ), 10 );
        const textWidth = parseInt( this.elem.find( '.detail__main-info' ).outerWidth(), 10 ) - asideElementWidth;

        this.elem.find( '.stage-items__headline' ).width( textWidth - 50 );
    }
}

export let detailStickyHeader = new DetailStickyHeader();


