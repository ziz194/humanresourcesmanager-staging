export class JumpTop {

    constructor() {
        this.addEventListener();
        this.removeEventListner();
    }

    addEventListener() {
        const that = this;
        $( "a[href^=\\#]" ).on( 'click', function ( e ) {
            const target = $( this ).attr( 'href' );
            that.scrollTo( target );
            e.preventDefault();
        } );
    }

    scrollTo( target ) {
        let $target = $( target );
        if ($target && (typeof $target.offset() !== 'undefined')) {
            let offset = $target.offset().top;
            if (target === '#filter') {
                offset = offset - 150;
            }
            $( 'html, body' ).animate( {
                scrollTop: offset
            }, 'slow' );
        }
    };

    removeEventListner() {
        $( '.stop-jump-top').off('click');
        $( '.stop-jump-to').off('click');
    }
}

export let jumpTop = new JumpTop();


