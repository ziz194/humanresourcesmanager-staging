import { find, each } from 'lodash';
const eventNamespace = 'NavSearch';

export class NavSearch {

    constructor() {
        this.init();

        $(window).scroll(function () {
            let scroll = $(window).scrollTop();
            $('.mobile-search-field').addClass('hide');

            if (scroll > 280) {
                $('.search-field').show();
            }
            if ((scroll < 280) && $('.search-field').data('home') === true) {
                $('.search-field').hide();
            }
        });

    }

    init() {
        $('.header-search__submit').on('click', function (event) {
            if($('.header-search__inputs').hasClass('hide')){
                event.preventDefault();
                $('.header-search__inputs').removeClass('hide');
            }
        });

        $('.right-nav__mobile__search').on('click', function (event) {
            event.preventDefault();
            if($('.mobile-search-field').hasClass('hide')){
                $('.mobile-search-field').removeClass('hide');
            }
            else{
                $('.mobile-search-field').addClass('hide');
            }
        });

        $('body').on(`mousedown.${eventNamespace}`, (e) => {
            let headerSearch      = $('.header-search__inputs');
            let autoCompletePanel = $('.auto-complete__panel');
            let autoComplete      = ['fake_to_provoke_false'];

            if (autoCompletePanel.length) {
                autoComplete = autoCompletePanel;
            }
            if ($.contains(headerSearch[0], e.target) === false &&
                $.contains(autoComplete[0], e.target) === false) {
                headerSearch.addClass('hide');
                // headerSearch.find('input[name=tags_p1]').val('');
            }
        });

        $('.global-js-search .submit').bind('mousedown', (e) => this.globalJsSearch(e));
        $('.global-js-search input').bind('keydown', (e) => this.globalJsSearchKeyDown(e));
    }

    // ------------- global search forms on home page or header of all pages ---
    globalJsSearchKeyDown(e) {
        if(e.keyCode == 13) {
            e.preventDefault();
        }
    }
    globalJsSearch(e) {
        e.preventDefault();

        let form        = $(e.currentTarget).closest("form");
        let searchInput = form.find('input[name=tags_p1]');
        let url         = form.data().searchRoute;
        let q           = [];

        if (searchInput.val() !== '') {
            form.find('input').each((i, input) =>  {
                let param = '';
                if (input.value) {
                    if ($(input).data().acDataSet) {
                        $.each($(input).data().acDataSet, (urlFilter, filter) => {
                            //console.log(filter + ' === ' + input.value);
                            if (filter === input.value) {
                                param = param+urlFilter+'/';
                            }
                        });
                        url = url+param;
                    }
                    if (param === '') {
                        q.push(encodeURIComponent(input.value));
                    }
                }
            });
            if (url.substr(url.length - 1 === '/')) {
                url = url.substring(0, url.length-1);
            }
            if (q.length) {
                url = url+'?q='+q.join();
            }

            if (url+'/' === form.data().searchRoute) {
                url = url+'?q';
            }

            // console.log(url);
            window.location.href = url;
        }
    }
}
export let navsearch = new NavSearch();