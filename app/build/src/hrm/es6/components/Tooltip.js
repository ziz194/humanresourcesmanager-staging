export class Tooltip {

    constructor() {
        this.init();
    }
    init(){
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    }
}
export let tooltip = new Tooltip();