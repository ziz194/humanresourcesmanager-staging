export class TabAccordionLimited {

    constructor() {
        this.element = $( '.tab-accordion--limited' );
        this.reInitStatus = false;
        this.init();
        this.addEventListener();
    }

    init() {
        //trigger resize event to reintialise slider inside tabs
        $('.panel-heading a,.collapse-link').on('click',function () {
            $( window ).resize();
        })
    }

    addEventListener() {
        var tab = $( '.tab-accordion__tabs li' );

        tab.on( 'show.bs.tab', (e) => {
           tab.each( (index, elem) => {
                $(elem).removeClass('active');
                var target = $(elem).attr('href');
                console.log(target);
                $(target).removeClass('active');
           });
        });
    }
}

export let tabAccordionLimited = new TabAccordionLimited();