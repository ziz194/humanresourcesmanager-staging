export class LightBox {

    constructor() {
        this.init();

    }

    init() {

        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                alwaysShowClose: true
            });
        });
    }
}
export let lightBox = new LightBox();








