export class GalleryLine {

    constructor() {
        this.module = $( '.module--gallery-line' );
        this.reInitStatus = false;
        this.init();
        this.addEventListener();
    }

    init() {
        // documentation: @see https://www.npmjs.com/package/slick-carousel
        this.module.find( '.slick-carousel').each( (index,elem) => {
            this.buildCarousel($(elem));
        });

    }

    buildCarousel( carousel ) {
        let slides = {
            l: 6,
            m: 4,
            s: 2,
            xs: 1
        };

        if(carousel.data('slides') !== undefined) {
            slides = carousel.data('slides');
        }

        carousel.slick( {
            prevArrow:      '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_left</i></button>',
            nextArrow:      '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_right</i></button>',
            dots:           false,
            infinite:       true,
            slidesToShow:   slides.l,
            slidesToScroll: slides.l,
            responsive:     [
                {
                    breakpoint: 993,
                    settings:   {
                        slidesToShow:   slides.m,
                        slidesToScroll: slides.m
                    }
                },
                {
                    breakpoint: 769,
                    settings:   {
                        slidesToShow:   slides.s,
                        slidesToScroll: slides.s
                    }
                },
                {
                    breakpoint: 300,
                    settings:   {
                        slidesToShow:   slides.xs,
                        slidesToScroll: slides.xs
                    }
                }
            ]
        } );

    }

    addEventListener() {
        this.module.closest( '.mobile-collapse' ).on( 'shown.bs.collapse', () => {
            if ( this.reInitStatus === false ) {
                console.log( 'reinit slick' );
                this.module.find( '.slick-carousel' ).slick( 'unslick' );
                this.init();
                this.reInitStatus = true;
            }
        } );
    }
}

export let galleryLine = new GalleryLine();


