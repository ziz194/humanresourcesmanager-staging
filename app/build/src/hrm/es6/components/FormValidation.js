import 'formvalidation' ;
import 'formvalidation/src/js/framework/bootstrap'
export class FormValidation {

    constructor() {

        $(document).ready(function () {
            $('.form-global').formValidation({
                framework: 'bootstrap',
                live:'enabled',
                excluded: ':disabled'
            }).on('err.field.fv', function(e) {
                $('.error-row').show();
            }).on('success.field.fv', function(e) {
                $('.error-row').hide();
            });
        });
    }
}
export let formvalidation = new FormValidation();