export class HighlightCards {

    constructor() {
        this.initSlider();
    }

    initSlider() {
        const module = $( '.highlight__cards' );
        let slides = {
            l:  4,
            m:  3,
            s:  2,
            xs: 1
        };

        if ( module.data( 'entity' ) === 'education' ) {
            slides = {
                l:  4,
                m:  3,
                s:  2,
                xs: 1
            };
        }

        // documentation: @see https://www.npmjs.com/package/slick-carousel
        module.find( '.slick-carousel' ).slick( {
            prevArrow:      '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_left</i></button>',
            nextArrow:      '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_right</i></button>',
            dots:           false,
            infinite:       true,
            slidesToShow:   slides.l,
            slidesToScroll: slides.l,
            responsive:     [
                {
                    breakpoint: 993,
                    settings:   {
                        slidesToShow:   slides.m,
                        slidesToScroll: slides.m
                    }
                },
                {
                    breakpoint: 769,
                    settings:   {
                        slidesToShow:   slides.s,
                        slidesToScroll: slides.s
                    }
                },
                {
                    breakpoint: 400,
                    settings:   {
                        slidesToShow:   slides.xs,
                        slidesToScroll: slides.xs
                    }
                }
            ]
        } );
    }
}

export let highlightCards = new HighlightCards();


