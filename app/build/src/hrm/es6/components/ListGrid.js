export class ListGrid {
    constructor() {
        this.listgrid_view();
        $(document).ready(function(){
            $('#listView').addClass('active');
        })
    }

     listgrid_view() {

        $('#listView').click(function (event) {
            $(this).addClass('active');
            $('#gridView').removeClass('active');
            event.preventDefault();
            $('.filter-content').addClass('list-group-item');
            $('.filter-content').removeClass('grid-group-item');
        });
        $('#gridView').click(function (event) {
            $(this).addClass('active');
            $('#listView').removeClass('active');
            event.preventDefault();
            $('.filter-content').removeClass('list-group-item');
            $('.filter-content').addClass('grid-group-item');
        });

    }
}


export let listgrid = new ListGrid();








