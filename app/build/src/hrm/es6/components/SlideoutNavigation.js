'use strict';

import Slideout from 'slideout';

export class SlideoutNavigation {

    constructor() {
        this.init();
        this.eventListener();
    }

    init() {
        this.slideout = new Slideout( {
            'panel':     document.getElementById( 'panel' ),
            'menu':      document.getElementById( 'mobile-menu' ),
            'padding':   300,
            'tolerance': 70
        } );
    }

    eventListener() {
        let that = this;
        $( '.navbar-toggle' ).on( 'click', function () {
            that.slideout.toggle();
            // $('.header-fixed-container.affix').css('top','187px')
        } );

        // close on window resize
        $( window ).on( 'resize', function () {
            that.slideout.close();
            $( '#panel').off('click');
        });

        // close on window resize
        that.slideout.on('translateend', function() {
            $( '#panel').off('click');
        });

        // close on page click
        that.slideout.on('open', function() {
            $( '#panel').on( 'click', function () {
                that.slideout.close();
                $( '#panel').off('click');
            });
        });

        //Make the header on top when slideOut Menu is open
        that.slideout.on('beforeopen', function() {
            // Show the header on the top
            var scrolly = $(document).scrollTop();
            if ($('.header-fixed-container').hasClass('affix-top') == false ){
                $('.header-fixed-container').css('position','relative');
                $('.header-fixed-container').css('top',scrolly-80+'px');
            }
        });

        //Make the header fixed again
        that.slideout.on('beforeclose', function() {
            if ($('.header-fixed-container').hasClass('affix-top') == false ) {
                $('.header-fixed-container').css('position', 'fixed');
                $('.header-fixed-container').removeAttr('style');
            }
        });

        $( '#mobile-menu' )
            // add redirection on navigation point and collapse only on caret icons clicks
            .find( '.dropdown-toggle' ).not( '.nav-caret' ).on( 'click', function ( event ) {

            if ( $( event.target ).hasClass( 'nav-caret' ) === false ) {
                // ToDo[Marcus Merchel]: Add righ opener event, to open link in same window!!! - 03.03.2017
                window.open( $( this ).attr( 'href' ) );
            }
        } );
    }

}
export let slideoutNavigation = new SlideoutNavigation();
