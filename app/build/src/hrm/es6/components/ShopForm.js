export class TanteEmma {
    constructor() {
        //this.module = $('#tante-emma-registration');
        //this.products = {};

        //this.readAllProducts();


        //console.log(this.module);
        // let shopForm = $('shopForm');
        // if (shopForm.lenght) {
        //     this.initShopForm();
        // }
        this.oldEmmaCode();
    }

    oldEmmaCode() {
        $(window).load(function() {
            var count_tn;

            $('.choose-date').click(function () {
                $('.choose-date').removeClass('active');
                $(this).addClass('active');
                
                var sku = $(this).attr('sku');
                $('.products').addClass('hide');
                $('#product_' + sku).removeClass('hide');
            });


            $('.products').each(function () {
                var persons = [];
                var id = $(this).attr('id');

                $(this).find('.more_participants').each(function () {
                    persons.push($(this));
                    $(this).remove();
                });
                persons.reverse();

                $('#' + id).find(".btn.add_person").click(function () {
                    var person = persons.pop();
                    $('#' + id + ' .participants').append(person);
                    count_tn = $(this).closest('.products').find('#participant .participant').length;
                    if (persons.length === 0) {
                        $(this).find('.add_person').addClass('hide');
                        $(this).find('.max_user').removeClass('hide');
                    }
                });

                $(document).on("click", "#" + id + " .btn.del_person", function () {
                    console.log('del_person');
                    var del_person = $(this).closest('.more_participants');
                    persons.push(del_person);
                    $(del_person).remove();
                    $(this).find('.add_person').removeClass('hide');
                    $(this).find('.max_user').addClass('hide');
                });

                $(document).on('keyup', '.products input', function () {
                    var data_name = $(this).attr('data-name');
                    $(document).find('.products input[data-name=' + data_name + ']').val($(this).val());
                });

            });

            var expand_frist_product_initially = true;
            if ($('#quadriga-tante-emma-expand-frist-product-initially').length) {
                if (!$('#quadriga-tante-emma-expand-frist-product-initially').data().value) {
                    expand_frist_product_initially = false;
                }
            }

            if (expand_frist_product_initially) {
                $('.optionen > .list-group-item-1').trigger('click');
            }

        }); //$(window).load
    }

    /*readAllProducts() {
        this.module.find('.products').each((index, elem) => {
            console.log(elem);
            var id = $(this).attr('id');
            var persons = this.hideAdditionalPersons( $(this) );
            this.products->{id} = person;
        });
    }

    hideAdditionalPersons( $elem ) {
        persons = []
        $(this).find('.more_participants').each((index, elem) => {
            persons.push($(this));
            $(this).remove();
        });
        persons.reverse();
        return persons;
    }*/

    // initShopForm() {
    //     console.log('initShop');
    // }
}

export let tanteEmma = new TanteEmma();