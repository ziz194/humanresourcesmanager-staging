export class CustomerVoices {

    constructor() {
        this.initMainSlider();
        this.initDetailPageSlider();
    }

    /**
     * initMainSlider
     * init customer voices teaser on all pages, thats not a detail page
     */
    initMainSlider() {
        /**
         * documentation:
         * @see https://www.npmjs.com/package/slick-carousel
         */
        $( '.customer-voices, .product-pros' ).find( '.slick-carousel' ).slick( {
            prevArrow:      '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_left</i></button>',
            nextArrow:      '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_right</i></button>',
            dots:           false,
            infinite:       true,
            //speed:          300,
            slidesToShow:   1,
            slidesToScroll: 1
        } );

        this.rerangeElements( '.customer-voices', '.customer-voices__wrapper');
        this.addEventlistener();
        this.setItemHeight();
    }

    /**
     * initDetailPageSlider
     * init customer voices teaser on detail pages
     */
    initDetailPageSlider() {
        /**
         * documentation:
         * @see https://www.npmjs.com/package/slick-carousel
         */
        $( '.customer-voices-detail' ).find( '.slick-carousel' ).slick( {
            prevArrow:      '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_left</i></button>',
            nextArrow:      '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_right</i></button>',
            dots:           false,
            infinite:       true,
            slidesToShow:   1,
            slidesToScroll: 1
        } );

        this.rerangeElements( '.customer-voices-detail', '.slick-carousel' );
    }

    addEventlistener() {
        $( window ).on( 'resize', () => {
            this.setItemHeight();
        } );
    }

    setItemHeight() {
        const element = $( '.customer-voices' );

        if ( $( window ).width() > 769 ) {
            const height = parseInt( element.outerHeight( true ), 10 );
            element.find( '.customer-voices__item' ).height( height );
        } else {
            element.find( '.slide' ).height( 'height' );
        }
    }

    rerangeElements(element, target) {
        element = $( element );
        const carousel = element.find( '.slick-carousel' );

        carousel.closest( target ).before( element.find( '.slick-prev' ) );
        carousel.closest( target ).after( element.find( '.slick-next' ) );
    }
}

export let customerVoices = new CustomerVoices();


