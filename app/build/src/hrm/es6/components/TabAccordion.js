export class TabAccordion{

    constructor() {
        this.tabArrowPosition();
        this.kontaktLink();
    }

    tabArrowPosition(){
        $(document).ready(function(){
            var tab = $( '.tab-accordion__tab' );
            tab.on( 'shown.bs.tab', (e) => {
                var tabPos = $(e.target).data('tab'); // Tab Position
                if (tabPos == 1 && $(window).width() > 765){
                    document.styleSheets[0].addRule('.mobile-accordion__collapse::before','left:10%');
                }
                else if(tabPos != 1 && $(window).width() > 765){
                    document.styleSheets[0].addRule('.mobile-accordion__collapse::before','left:calc('+ tabPos*15 + '% - 10px)')
                }
                else document.styleSheets[0].addRule('.mobile-accordion__collapse::before','left:calc(50% - 10px)');
            });

        });
    }

    kontaktLink(){
        $(document).ready(function(){
            $('.detail--studium .button-order').on('click',function(e){
                $('#headingAnsprechpartner a').click();
            });
        });
    }
}

export let tabAccordion = new TabAccordion();