/**
 * Created by ahaddad on 05.04.2017.
 */
export class GallerySlider {

    constructor() {
        this.initSlider();

    }


    initSlider(){
        /**
         * documentation:
         * @see https://www.npmjs.com/package/slick-carousel
         */
        const module = $( '.gallery' );
        let slides = {
            l:  4,
            m:  3,
            s:  1,
            xs: 1
        };
        module.find( '.slick-carousel' ).slick( {
            prevArrow:      '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_left</i></button>',
            nextArrow:      '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_right</i></button>',
            dots:           false,
            infinite:       true,
            //speed:          300,
            speed: 300,

            slidesToShow: slides.l,
            slidesToScroll: slides.l,
            responsive:     [
                {
                    breakpoint: 993,
                    settings:   {
                        slidesToShow:   slides.m,
                        slidesToScroll: slides.m
                    }
                },
                {
                    breakpoint: 769,
                    settings:   {
                        slidesToShow:   slides.s,
                        slidesToScroll: slides.s
                    }
                },
                {
                    breakpoint: 400,
                    settings:   {
                        slidesToShow:   slides.xs,
                        slidesToScroll: slides.xs
                    }
                }
            ]
        });
    }
}
export let gallerySlider = new GallerySlider();






