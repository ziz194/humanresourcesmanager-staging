'use strict';

import slideoutNavigation from './SlideoutNavigation';
import navigation from './Navigation';
import './AutoComplete';
import './JumpTop';
import './ListGrid';
import './Dropdown' ;
import './Search';
import './FormValidation';
import './ModalCall';
import './LightBox';
import './Popover';
import './Tooltip'
import './GalleryLine';
import './ReviewSlider';
import './CustomerVoices';
import './Highlights';
import './JobsHomeSlider';
import './FormScript';
import './NavSearch';
import './TabAccordionLimited';
import './DetailStickyHeader';
import './PricingSlider';
import './HighlightCards';
import './EllipsisScript';
import './TabAccordion';
import './ShopForm';
import './PricingMagazineScript';
import './MagazineArchiveScript'
import './MobileAccordion';
import './GallerySlider';


// import './CollapseFilter'