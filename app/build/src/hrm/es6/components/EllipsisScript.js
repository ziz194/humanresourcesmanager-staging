/**
 * Created by ahaddad on 27.03.2017.
 */

export class EllipsisScript {

    constructor() {
        this.initDotDotDot();
    }


    initDotDotDot(){
        $(document).ready(function() {
            $(".event_teaser .teaser_text, .person_teaser .teaser_text, .teaser_text").dotdotdot({
            /*	The text to add as ellipsis. */
                ellipsis	: '',
                /*	Optionally set a max-height, can be a number or function.
                 If null, the height will be measured. */
                height		: 65,
                /*	Deviation for the height-option. */
                tolerance	: 10,
                /*	jQuery-selector for the element to keep and put after the ellipsis. */
                after		: 'a.read-more',
            });

            $(".search-item__post__teaser .teaser_text").dotdotdot({
                /*	The text to add as ellipsis. */
                ellipsis	: '',
                /*	Optionally set a max-height, can be a number or function.
                 If null, the height will be measured. */
                height		: 60,
                /*	Deviation for the height-option. */
                tolerance	: 10,
                /*	jQuery-selector for the element to keep and put after the ellipsis. */
                after		: 'a.read-more',
            });

            //Limit media-icons in jobs homepage to 2 lines
            $(".media-body .media-body__content,.highlight-box__item__info__title").dotdotdot({
                /*	The text to add as ellipsis. */
                ellipsis	: '...',
                /*	Optionally set a max-height, can be a number or function.
                 If null, the height will be measured. */
                height		: 50,
                /*	Deviation for the height-option. */
                tolerance	: 10
            });

        });
    }

}
export let ellipsisScript = new EllipsisScript();






