export class Popover {

    constructor() {
        this.init();
    }
    init() {
        $(document).ready(function () {
            $('[data-toggle="popover"]').click(function (e) {
                // e.preventDefault();
            });

            $(".share-popover").popover({
                content: function () {
                    return $('#share-popover-content').html();
                },
                // trigger: 'focus',
                html: true
            });

            $(".pricing-table__info-icon").each( function() {
                $(this).on('click',function(){
                    // Hide other popovers
                    $('[data-toggle="popover"]').popover('hide');
                });
                var $elem = $(this);
                $elem.popover({
                    // trigger: "hover",
                    content: function () {
                        return $(this).data('info');
                    },
                    // container: $elem,
                    offset: 10
                });
            });

            $(".agenda__item__performer__image").each(function () {
                var $elem = $(this);
                $elem.popover({
                    trigger: "hover",
                    content: function () {
                        return $('.agenda__item__performer__information__' + $(this).data('performer')).html();
                    },
                    html: true,
                    container: $elem,
                    offset: 10
                });
            });

            $(".search-item__caption__image").each(function () {
                var $elem2 = $(this);
                $elem2.popover({
                    trigger: "hover",
                    content: function () {
                        return $('.agenda__item__performer__information__' + $(this).data('performer')).html();
                    },
                    html: true,
                    container: 'body',
                    offset: 10
                });
            });

            //dismiss popoever on page click
            $('body').on('click', function (e) {
                // did not click a popover toggle, or icon in popover toggle, or popover
                if ($(e.target).data('toggle') !== 'popover'
                    && $(e.target).parents('[data-toggle="popover"]').length === 0
                    && $(e.target).parents('.popover.in').length === 0) {
                    $('[data-toggle="popover"]').popover('hide');
                }
            });

            $('.share-popover').on('shown.bs.popover', function () {
                //     setTimeout(function () {
                //         $.getScript("//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58b816a659deb56c");
                //         addthis.init();
                //         addthis.toolbox('.addthis_toolbox');
                //     },1000);
                // })
                addthis.init();
                addthis.toolbox('.addthis_toolbox')

            });
        });
    }
}
export let popover = new Popover();