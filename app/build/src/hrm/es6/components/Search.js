import 'eonasdan-bootstrap-datetimepicker';
import { find, each } from 'lodash';
import { common } from '../common/Common';
import { jumpTop } from './JumpTop';
import { device } from '../common/Device';

export class Search {
    constructor() {
        // TODO: max value to load maximum contents in mobileSelected()

        this.xhr         = null;
        this.$search     = $('.search-listing');
        this.$filter     = this.$search.find('#filter');
        this.$counter    = this.$search.find('.search-listing__result-counter');
        this.$results    = this.$search.find('.search-listing-results');
        this.$content    = this.$search.find('.filter-content');
        this.$spinner    = this.$content.find('.spinner');
        this.$pagination = this.$search.find('.pagination');
        this.$alphabet   = this.$search.find('.filter-alphabet');

        this.$q = '';
        if (this.$search.data('q')) {
            this.$q = 'q='+this.$search.data('q');
        }

        this.$dateQuery  = '';
        this.$alphQuery  = '';
        this.getPaginationQuery('.pagination__item--current');
        this.initEventListener();
        this.initUnTagEventListeners();

        // init datepicker
        $( '.datepicker' ).datetimepicker({
            format: "DD.MM.YYYY",
            icons: {
                time:     'mdi mdi-access-time',
                date:     'mdi mdi-perm-contact-calendar',
                up:       'mdi mdi-keyboard-arrow-up',
                down:     'mdi mdi-keyboard-arrow-down',
                previous: 'mdi mdi-chevron-left',
                next:     'mdi mdi-chevron-right',
                today:    'mdi mdi-today',
                clear:    'mdi mdi-delete',
                close:    'mdi mdi-remove-circle'
            }
        });

        // on mobile devices, collapse all filter on first view
        if ( device.mobile() ) {
            $( '#filterGroup' ).removeClass( 'in' );
        }

        this.searchAfterLoading = this.$search.find('#searchAfterLoading');
        if (this.searchAfterLoading.length && this.$search.data('main') !== 'jobboerse') {
            this.updateFilterList();
        }
    }

    // ------------------------------------ Go through Filters and send Ajax ---
    updateFilterList()
    {
        if (!this.searchAfterLoading.length) {
            $('.search-item__no-results').addClass('hidden');
        }
        const that = this;
        let $filterList = {};
        let $browserUri = '';
        let $apiQuery   = '';
        let $groups = this.$filter.find('.filter__group__content');
        $groups.each( (i,group) => {
            let $apiGroupFilterQuery  = '';
            let $oneInGroupIsSelected = false;
            let $filterItems = $(group).find('.filter__group__item__input');
            $filterItems.each ( (j,filter) => {
                if ($filterList.hasOwnProperty(filter.dataset.group) === false){
                    $filterList[filter.dataset.group] = {};
                }
                if ($oneInGroupIsSelected === false) {
                    $oneInGroupIsSelected = filter.checked;
                }
                $filterList[filter.dataset.group][filter.dataset.name] = filter.checked;
                if (filter.checked) {
                    $apiGroupFilterQuery = $apiGroupFilterQuery + filter.dataset.name.replace('&', '.amp.') + ',';
                    //console.log( group.dataset );
                    $browserUri = $browserUri + '/' + filter.dataset.keyUrl;
                }
            });
            if ($oneInGroupIsSelected === false) {
                this.$filter.find('#'+group.dataset.groupId+'_all').prop("checked", true);
            } else {
                $apiGroupFilterQuery = $apiGroupFilterQuery.substring(0, $apiGroupFilterQuery.length - 1);
                $apiQuery = $apiQuery+'&'+group.dataset.groupId+'='+$apiGroupFilterQuery;
                this.$filter.find('#'+group.dataset.groupId+'_all').prop("checked", false);
            }
        });

        $apiQuery = $apiQuery.substr(1);

        if (this.$alphQuery !== '') {
            if ($apiQuery === '') {
                $apiQuery = this.$alphQuery;
            } else {
                $apiQuery = this.$alphQuery+'&'+$apiQuery;
            }
        }
        if (this.$dateQuery !== '') {
            this.$filter.find('#month_all').prop("checked", false);
            if ($apiQuery === '') {
                $apiQuery = this.$dateQuery;
            } else {
                $apiQuery = this.$dateQuery+'&'+$apiQuery;
            }
        }
        if(this.xhr && this.xhr.readyState !== 4){
            this.xhr.abort();
        }

        if (this.$search.data('main')) {
            $browserUri = '/' + this.$search.data('main') + $browserUri;
        }
        if (this.$q !== '') {
            if ($apiQuery === '') {
                $apiQuery = this.$q;
            } else {
                $apiQuery = this.$q+'&'+$apiQuery;
            }
            $browserUri = $browserUri+'?'+this.$q;
        }

        // console.log( '$browserUri', $browserUri );
        if (!this.searchAfterLoading.length && this.$search.data('main') !== 'jobboerse') {
            window.history.pushState('page2', document.title, $browserUri);
        }
        //console.log( '$apiQuery', $apiQuery );
        //console.log( '$pageQuery', this.$pageQuery );

        // start loading animation
        if ( device.mobile() ) {
            $( '.mobile-load-more' ).find( '.mobile-spinner' ).fadeIn( );
        } else {
            if (!this.searchAfterLoading.length) {
                this.$results.find('.search-item').hide();
                let spinnerSize = this.$pageQuery.split('=')[2];

                for (let count = 0; count < spinnerSize; count++) {
                    this.$results.append(this.$spinner.clone());
                    this.$results.find('.spinner').fadeIn('slow');
                }
            }
        }

        // jump top
        if ( device.mobile() === false ) {
            jumpTop.scrollTo( '#filter' );
        }

        // start ajax request
        this.xhr = $.post( {
            url:      "/ajax/" + this.$search.data( 'entity' ) + '?' + this.$pageQuery,
            data:     $apiQuery,
            dataType: "json"
        } )
            .done( function ( data ) {
                //console.log( 'xhr done' );
                that.addResults(data);
            } )
            .fail( function ( ) {
                //console.log( "xhr error" );
                // on fail, show old result items again
                if ( device.mobile() === false ) {
                    that.$results.find( '.search-item' ).fadeIn( 'slow' );
                }
            } )
            .always( function ( ) {
                //console.log( "xhr finished" );
                // hide loading animation after ajax response
                that.$results.find( '.spinner' ).remove();
                $('.mobile-load-more').find( '.mobile-spinner' ).fadeOut();
                $(document).trigger('filterListUpdated');
            } );
    }

    // ------------------------------------------------------ After the ajax ---
    addResults(data)
    {
        if (!this.searchAfterLoading.length) {
            if( device.mobile() ) {
                this.$results.append(data.list);
            } else {
                this.$results.html(data.list);
            }

            this.$pagination.html(data.pagination);
            if (data.alph !== '') {
                this.$alphabet.html(data.alph);
                this.initAlphabetFilterEventListeners();
            }

            this.$counter.html(data.total);

            if (data.fail === 'fail') {
                $('.search-item__no-results').removeClass('hidden');
            } else {
                $('.search-item__no-results').addClass('hidden');
            }
        } else {
            this.searchAfterLoading = '';
        }

        // console.log(data.aggs);
        if (data.aggs !== '') {
            let $groups = this.$filter.find('.filter__group__content');
            $groups.each( (i,group) => {
                let $filterItems = $(group).find('.filter__group__item__input');
                $filterItems.each ( (j,filter) => {
                    if (group.dataset.groupId !== 'tags_p1' && group.dataset.groupId !== 'index') {
                        if (filter.checked === false) {
                            try {
                                $('#'+filter.dataset.itemId+'-group-item').addClass('disabled');
                                $('#'+filter.id).attr('disabled', 'disabled');
                                // $('#select_'+group.dataset.groupId+' option[value='+filter.dataset.name+']').attr('disabled', 'disabled');
                                // $('#doc_counter_'+filter.id).html(0);
                            } catch(err) {
                                console.log('err1 ' + filter.dataset.itemId);
                            }
                        }
                        if (data.aggs.hasOwnProperty(group.dataset.groupId)) {
                            $.each(data.aggs[group.dataset.groupId].buckets, (i, refreshFilter) => {
                                if (group.dataset.groupId+'_'+refreshFilter.id === filter.id) {
                                    if (refreshFilter.doc_count > 0) {
                                        try {
                                            $('#'+refreshFilter.id+'-group-item').removeClass('disabled');
                                            $('#'+filter.id).removeAttr('disabled');
                                            // $('#select_'+group.dataset.groupId+' option[value='+refreshFilter.key+']').removeAttr('disabled');
                                        } catch(err) {
                                            console.log('err2 ' + filter.id);
                                        }
                                    }
                                    // try {
                                    //     $('#doc_counter_'+group.dataset.groupId+'_'+refreshFilter.id).html(refreshFilter.doc_count);
                                    // } catch(err) {
                                    //console.log('err3');
                                    // }
                                }
                            });
                        }
                    }
                });
            });
        }
        this.initPaginatorEventListeners();
    }

    // --------------------------------------------------------- Init Events ---
    initEventListener()
    {
        this.$filter.find('input[type=checkbox]').on('click', (e) => this.filterChecked(e));
        this.$filter.find('select').on('change', (e) => this.filterSelected(e));
        this.$filter.find('.filter__group__item.hidden').on('click', (e) => this.filterUnSelected(e));
        this.initAlphabetFilterEventListeners();
        this.initPaginatorEventListeners();

        // custom dates
        this.$filter.find('.filter__group__date_checkbox').on('click', (e) => this.filterCustomDate(e));
        this.$filter.find( '#month_custom_start_date' ).on( 'focusout', ( e ) => this.filterStartDate( e ) );
        this.$filter.find( '#month_custom_end_date' ).on( 'focusout', ( e ) => this.filterEndDate( e ) );

        $('.filter__group').find('a').off('click');
    }
    initPaginatorEventListeners()
    {
        this.$search.find('.pagination__item').on('click', (e) => this.pageSelected(e));
        this.$search.find('.mobile-load-more').on('click', (e) => this.mobileSelected( e ));
    }
    initAlphabetFilterEventListeners()
    {
        this.$search.find('.filter-alphabet__letter').on('click', (e) => this.filterAlphSelected(e));
    }
    initUnTagEventListeners()
    {
        this.$search.find('.tag_result .tag').on('click', (e) => this.unTag(e));
    }

    // ---------------------------------------------------------- Tag Filter ---
    filterChecked(e)
    {
        if (e.currentTarget.dataset.name === 'COMMON_FILTER_CHECK_ALL') {
            let $group = this.$filter.find('#collapse'+e.currentTarget.dataset.group);
            let $filterList = $group.find('.filter__group__item__input');
            $filterList.each ( (i,filter) => {
                $(filter).prop("checked", false);
                this.toggleHashTags(false, filter.id);
            });
        }
        if (e.currentTarget.dataset.name !== 'custom_date_checkbox') {
            this.toggleHashTags(e.currentTarget.checked, e.currentTarget.id);
            this.filter(e.currentTarget.dataset.group);
        }
    }
    filterSelected(e)
    {
        let $elem = this.$filter.find('#'+e.currentTarget.value+'-group-item');
        $elem.removeClass('hidden');
        let $checkbox = $elem.find('input');
        $checkbox.prop("checked", true);
        e.currentTarget.value = '';
        this.toggleHashTags(true, $checkbox.attr('id'));
        this.filter($checkbox.data('group'));
    }
    filter(group) {
        this.getPaginationQuery('.pagination__item--first');
        if (group === 'month') {
            this.eraseCustomDateFilter();
        }
        this.updateFilterList()
    }
    filterUnSelected(e)
    {
        e.currentTarget.classList.add('hidden');
        this.toggleHashTags(false, e.currentTarget.id);
    }
    unTag(e)
    {
        if (e.currentTarget.classList.contains('tag_q')) {
            e.currentTarget.remove();
            let q = [];
            this.$search.find('.tag_q').each ( (j,tag_q) => {
                q.push(tag_q.dataset.value);
            });
            if (q.length) {
                this.$q = 'q='+q.join();
            } else {
                this.$q = '';
            }
            this.updateFilterList();

        } else {
            e.currentTarget.classList.add('hidden');
            let $elemCheckbox = this.$filter.find('input[data-item-id='+e.currentTarget.dataset.itemId+']');
            $elemCheckbox.trigger("click");
        }
    }
    toggleHashTags(checked, name)
    {
        if (checked) {
            $('#tag_'+name).removeClass('hidden');
        } else {
            $('#tag_'+name).addClass('hidden');
        }
    }

    // ---------------------------------------------------------- Pagination ---
    getPaginationQuery(classSelector)
    {
        let currentPage = this.$search.find(classSelector);
        if (typeof currentPage.data('query') !== 'undefined') {
            this.$pageQuery = currentPage.data('query');
        }
    }
    pageSelected(e)
    {
        if (e.currentTarget.classList.contains('pagination__item--inactive') === false &&
            e.currentTarget.classList.contains('pagination__item--current') === false) {
            this.$pageQuery = e.currentTarget.dataset.query;
            this.updateFilterList();
        }
    }

    mobileSelected( event ) {
        if ( typeof this.currentFromMobile === 'undefined' || typeof this.itemSizeMobile === 'undefined' ) {
            const $button = $( event.currentTarget );
            const buttonData = $button.data( 'query' );
            this.currentFromMobile = parseInt( buttonData.split( '&' )[ 0 ].split( '=' )[ 1 ], 10 );
            this.itemSizeMobile = parseInt( buttonData.split( '&' )[ 1 ].split( '=' )[ 1 ], 10 );
        } else {
            this.currentFromMobile = parseInt( this.currentFromMobile, 10 ) + parseInt( this.itemSizeMobile, 10 );
        }

        this.$pageQuery = `from=${this.currentFromMobile}&size=${this.itemSizeMobile}`;

        // do content query
        this.updateFilterList();
    }

    // ----------------------------------------------------- Alphabet Filter ---
    filterAlphSelected(e) {
        if (e.currentTarget.classList.contains('filter-alphabet__letter--inactive') === false &&
            e.currentTarget.classList.contains('filter-alphabet__letter--current') === false) {
            this.$alphQuery = e.currentTarget.dataset.query;
            $('.filter-alphabet__letter').removeClass('filter-alphabet__letter--current');
            if (e.currentTarget.classList.contains('filter-alphabet__letter--all') === false) {
                e.currentTarget.classList.add('filter-alphabet__letter--current');
            }
            this.getPaginationQuery('.pagination__item--first');
            this.updateFilterList();
        }
    }

    // -------------------------------------------------- Custom Date Filter ---
    filterCustomDate(e) {
        if (e.currentTarget.checked === false) {
            this.$filter.find('#month_custom_end_date').val('');
            this.$filter.find('#month_custom_start_date').val('');
            if ( this.$dateQuery !== '') {
                this.$dateQuery = '';
                this.updateFilterList();
            }
        }
    }
    filterStartDate(e) {
        let $endElem  = this.$filter.find('#month_custom_end_date');
        let startDate = e.currentTarget.value;
        let endDate   = $endElem.val();

        if ($endElem.val() === '') {
            let startDateArray = startDate.split(".");
            endDate = startDateArray[0]+'.'+startDateArray[1]+'.'+(Number(startDateArray[2])+1);
            $endElem.val(endDate);
        }

        this.filterDate(startDate, endDate);
    }
    filterEndDate(e) {
        let startDate = this.$filter.find('#month_custom_start_date').val();
        this.filterDate(startDate, e.currentTarget.value);
    }
    filterDate(startDate, endDate) {
        if (startDate !== '' && endDate !== '') {
            this.getPaginationQuery('.pagination__item--first');
            this.$dateQuery = 'startDate='+common.germanDateToDBDate(startDate)+'&endDate='+common.germanDateToDBDate(endDate);

            this.$filter.find('div[data-group-id=month] input[type=checkbox]').each ( (j,filter) => {
                filter.checked = false;
                this.toggleHashTags(false, filter.id)
            });

            this.updateFilterList();
            let $customDate = this.$filter.find('.filter__group__date_checkbox');
            if ($customDate[0].checked === false) {
                $customDate[0].checked = true;
            }
        } else {
            console.log('VALIDATE THE INPUT')
        }
    }
    eraseCustomDateFilter() {
        this.$filter.find('#month_custom_end_date').val('');
        this.$filter.find('#month_custom_start_date').val('');
        this.$filter.find('.filter__group__date_checkbox')[0].checked = false;
        this.$dateQuery = '';
    }
}

export let search = new Search();