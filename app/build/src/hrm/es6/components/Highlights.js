export class Highlights {

    constructor() {
        // documentation: @see https://www.npmjs.com/package/slick-carousel
        $( '.highlights' ).find( '.slick-carousel' ).slick( {
            prevArrow:      '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_left</i></button>',
            nextArrow:      '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_right</i></button>',
            dots:           false,
            infinite:       true,
            //speed:          300,
            slidesToShow:   2,
            slidesToScroll: 2,
            responsive:     [
                {
                    breakpoint: 769,
                    settings:   {
                        slidesToShow:   1,
                        slidesToScroll: 1
                    }
                }
            ]
        } );
    }
}

export let highlights = new Highlights();


