import { find, each } from 'lodash';
const eventNamespace = 'AutoComplete';

export default class AutoComplete {
    constructor() {
        $('.auto-complete').each((i, elem) => {
            $(elem).attr('id','ac_'+i);
            this.initEvents($(elem));
        });
        this.showingResults = false;
    }

    initEvents(elem) {
        elem.bind('keyup', (e) => this.onKeyUp(e));
    }
    val(elem) {
        return $.trim(elem.val());
    }
    onKeyUp(e) {
        let elem = $('#'+e.currentTarget.id);
        if (this.val(elem).length > 0) {
            this.requestResults(elem);
        } else {
            if (this.showingResults) {
                this.hide();
            }
        }
    }

    createPanel(elem) {
        if (this.$panel) this.$panel.remove();

        this.$panel = $('<div></div>').addClass('auto-complete__panel');

        $('body').append(this.$panel);

        this.$panel.css({
            zIndex:10
        });

        $(window).on(`resize.${eventNamespace}`, () => this.position(elem));
        $(window).on(`scroll.${eventNamespace}`, () => this.position(elem));
        this.$panel.show();
        this.initClickOutsideComponent();
        this.position(elem);
    }

    hide() {
        $(window).off(   `resize.${eventNamespace}`);
        $(window).off(   `scroll.${eventNamespace}`);
        $('body').off(`mousedown.${eventNamespace}`);
        this.$panel.addClass('hidden');
        this.showingResults = false;
    }

    initClickOutsideComponent() {
        $('body').on(`mousedown.${eventNamespace}`, (e) => {
            if (!$.contains(this.$panel[0], e.target)) {
                this.hide();
            }
        });
    }

    requestResults(elem) {
        let q       = this.val(elem);
        let data    = elem.data().acDataSet;
        let results = [];
        $.each(data, (urlFilter, filter) => {
            if (typeof filter === 'string') {
                let rgxp    = new RegExp(q, "gi");
                let matches = filter.match(rgxp);
                if (matches) {
                    let markedFilter = filter;
                    matches.forEach((match, i) => {
                        markedFilter = markedFilter.replace(match,'<b>'+match+'</b>');
                    });
                    results = results+'<div class="auto-complete__item" data-value="'+filter+'">'+markedFilter+'</div>';
                }
            }
        });
        this.showResults(elem, results);
    }

    showResults(elem, results) {
        this.createPanel(elem);
        this.$panel.html(results);
        this.$panel.find('.auto-complete__item').bind('mousedown', e => this.onSelectItem(e,elem));
        this.showingResults = true;
    }

    onSelectItem(e, elem) {
        let $li   = $(e.target);
        let value = $li.data().value;

        elem.val(value);
        this.hide();
    }

    position(elem) {
        this.$panel.css({
            left: elem.offset().left,
            top: elem.offset().top + elem.outerHeight(),
            width: Math.min(elem.outerWidth(), 300)
        });
    }
}

export let autocomplete = new AutoComplete();