/**
 * Created by ahaddad on 17.03.2017.
 */
export class PricingSlider {

    constructor() {
        this.initSlider();
        this.initDropdown();
    }


    initSlider(){
        /**
         * documentation:
         * @see https://www.npmjs.com/package/slick-carousel
         */
        $( '.pricing-table' ).find( '.slick-carousel' ).slick( {
            prevArrow:      '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_left</i></button>',
            nextArrow:      '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_right</i></button>',
            dots:           false,
            infinite:       false,
            speed: 300,
            slidesToShow: 1,
            centerMode: true,
            variableWidth: false,
            placeholders: false,
            slidesToScroll: 1
        });

        /* below code handles removing of first and last arrows on infinite slider*/
        $('.pricing-table').find( '.slick-carousel' ).on('afterChange', function(){
            var currentSlide = $('.pricing-table').find( '.slick-carousel' ).slick('slickCurrentSlide');
            if(currentSlide==0)
            {
                $('.slick-prev').hide();
                $('.slick-next').show();
            }
            else if(currentSlide==2)
            {
                $('.slick-next').hide();
                $('.slick-prev').show();
                $('.pricing-table').find( '.slick-carousel' )
            }

            if(currentSlide>0 && currentSlide<2)
            {

                $('.slick-prev').show();
                $('.slick-next').show();
            }
        });

        $(document).ready(function(){
            var currentSlide = $('.pricing-table').find( '.slick-carousel' ).slick('slickCurrentSlide');
            if(currentSlide==0)
            {
                $('.slick-prev').hide();
            }
            else if(currentSlide==2)
            {
                $('.slick-next').hide();
            }
        });
        /**/
    }

    initDropdown(){
        $('.pricing-table__options__title').on('click',function() {

            if ($('.pricing-table__options__toggle').is(':visible')){
                $('.pricing-table__options__toggle').slideUp(100);
                $('.pricing-table__options__arrow').html('keyboard_arrow_down');
            }
            else {
                $('.pricing-table__options__toggle').slideDown(100);
                $('.pricing-table__options__arrow').html('keyboard_arrow_up');
            }
        });


        $('.pricing-table__suboptions__title').on('click',function() {

            if ($('.pricing-table__suboptions__toggle-'+$(this).data('option')).is(':visible')){
                $('.pricing-table__suboptions__toggle-'+$(this).data('option')).slideUp(100);
                $('.pricing-table__suboptions__arrow').html('keyboard_arrow_down');
            }
            else {
                $('.pricing-table__suboptions__toggle-'+$(this).data('option')).slideDown(100);
                $('.pricing-table__suboptions__arrow').html('keyboard_arrow_up');
            }
        });
    }
}
export let pricingSlider = new PricingSlider();






