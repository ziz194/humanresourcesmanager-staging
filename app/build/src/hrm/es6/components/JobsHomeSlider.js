export class JobsHomeSlider {

    constructor() {
        var slides = {
            l: 2,
            m: 2,
            s: 1,
            xs: 1
        };
        // documentation: @see https://www.npmjs.com/package/slick-carousel
        $( '.module--jobboerse' ).find( '.slick-carousel' ).slick( {
            prevArrow:      '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_left</i></button>',
            nextArrow:      '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"><i class="material-icons">keyboard_arrow_right</i></button>',
            dots:           false,
            infinite:       true,
            variableWidth: false,
            rows: 3,
            slidesPerRow: slides.l,
            slidesToScroll: 1,
            responsive:     [
                {
                    breakpoint: 993,
                    settings:   {
                        slidesPerRow:   slides.m,
                        slidesToScroll: slides.m
                    }
                },
                {
                    breakpoint: 769,
                    settings:   {
                        slidesPerRow:   slides.s,
                        slidesToScroll: slides.s,
                        rows: 3
                    }
                },
                {
                    breakpoint: 300,
                    settings:   {
                        slidesPerRow:   slides.xs,
                        slidesToScroll: slides.xs,
                        rows: 3,
                    }
                }
            ]
        });
    }
}

export let jobsHomeSlider = new JobsHomeSlider();


