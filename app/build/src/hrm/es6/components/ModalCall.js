export class ModalCall {

    constructor() {
        this.changeModalContent();
    }

    changeModalContent(){
        let modal_trigger = $('.modal-trigger');
        let modal = $('.modal');
        modal_trigger.each(function(index) {
            $(this).on('click',function(){
                modal.attr('id',$(this).id);
                let modal_content = ($(this).data("modal-target"));
                $(".modal-content").html($('#'+ modal_content).html());
                modal.modal('show');

                //reinitialise form
                $('.form-global').formValidation({
                    framework: 'bootstrap',
                    live:'enabled',
                    excluded: ':disabled'
                }).on('err.field.fv', function(e) {
                    $('.error-row').show();
                }).on('success.field.fv', function(e) {
                    $('.error-row').hide();
                });
            })
        });

    }
}
export let modalCall = new ModalCall();







