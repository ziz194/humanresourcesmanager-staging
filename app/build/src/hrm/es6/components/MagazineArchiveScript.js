
/**
 * Created by ahaddad on 12.04.2017.
 */
export class MagazineArchiveScript {

    constructor() {
        this.initDropdown();
    }



    initDropdown(){

        $('.magazin-archive__year__toggle').on('click',function() {

            if ($('.magazin-archive__year__content-'+$(this).data('year')).is(':visible')){
                $('.magazin-archive__year__content-'+$(this).data('year')).slideUp(100);
                $('.magazin-archive__year__arrow-'+$(this).data('year')).html('keyboard_arrow_down');
            }
            else {
                ($('.magazin-archive__year__content-'+$(this).data('year'))).slideDown(100);
                $('.magazin-archive__year__arrow-'+$(this).data('year')).html('keyboard_arrow_up');
            }
        });
    }
}
export let magazineArchiveScript = new MagazineArchiveScript();






