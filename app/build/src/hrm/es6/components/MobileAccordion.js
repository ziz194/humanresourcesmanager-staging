
/**
 * Created by ahaddad on 24.04.2017.
 */
export class MobileAccordionScript {

    constructor() {
        this.initAccordions();
    }



    initAccordions(){

        $('.mobile-accordion__button__wrapper').on('click',function() {

            if ($('#collapse-'+$(this).data('id')).is(':visible')){
                $('.mobile-accordion__button__arrow-'+$(this).data('id')).html('keyboard_arrow_down');
            }
            else {
                $('.mobile-accordion__button__arrow-'+$(this).data('id')).html('keyboard_arrow_up');
            }
        });
    }
}
export let mobileAccordionScript = new MobileAccordionScript();






