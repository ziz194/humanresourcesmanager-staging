/**
 * Created by ahaddad on 10.03.2017.
 */
import { find, each } from 'lodash';

export class FormScript {

    constructor() {
        this.init();

        $('.stage-js-search .submit').bind('mousedown', (e) => this.stageJsSearch(e));
        $('.stage-js-search input').bind('keydown', (e) => this.stageJsSearchKeyDown(e));
    }
    init() {
        $('.open-fromdatetimepicker').click(function(event){
            event.preventDefault();
            $('#from_date').focus();
        });

        $('.open-untildatetimepicker').click(function(event){
            event.preventDefault();
            $('#until_date').focus();
        });

        $('.from_full_day').click(function(event){
            // $('.until-row').toggle();
            $('.from-hours').toggle();
            $('.from-minutes').toggle();
        });

        $('.until_full_day').click(function(event){
            // $('.until-row').toggle();
            $('.until-hours').toggle();
            $('.until-minutes').toggle();
        });
    }

    // ------------------------------------------------- forms in the stages ---
    stageJsSearchKeyDown(e) {
        if(e.keyCode == 13) {
            e.preventDefault();
            this.stageJsSearch(e);
        }
    }
    stageJsSearch(e) {
        e.preventDefault();
        let form = $(e.currentTarget).closest("form");
        let url  = form.data().searchRoute;
        let q    = [];

        if (url.indexOf('jobboerse') !== -1) {
            let q = form.find('input[name=q]').val().trim();
            let location = form.find('input[name=job_location]').val().trim();
            let uri = '';
            if(q) {
                uri = '?q=' + encodeURIComponent(q);
            }
            if(location) {
                if (uri === '') {
                    uri = uri + '?job_location=' + encodeURIComponent(location);
                } else {
                    uri = uri + '&job_location=' + encodeURIComponent(location);
                }
            }
            if (uri === '') {
                uri = '?q';
            }

            url = url.substring(0, url.length-1) + uri;
            // console.log(url);
            window.location.href = url;
        }
        else {
            form.find('input').each((i, input) =>  {
                let param = '';
                if (input.value) {
                    if ($(input).data().acDataSet) {
                        $.each($(input).data().acDataSet, (urlFilter, filter) => {
                            //console.log(filter + ' === ' + input.value);
                            if (filter === input.value) {
                                param = param+urlFilter+'/';
                            }
                        });
                        url = url+param;
                    }
                    if (param === '') {
                        q.push(encodeURIComponent(input.value));
                    }
                }
            });
            if (url.substr(url.length - 1 === '/')) {
                url = url.substring(0, url.length-1);
            }
            if (q.length) {
                url = url+'?q='+q.join();
            }

            if (url+'/' === form.data().searchRoute) {
                url = url+'?q';
            }

            //console.log(url);
            window.location.href = url;
        }
    }
}
export let formscript = new FormScript();