export class CollapseFilter {

    constructor() {
        this.init();
        this.uncollapse();
        this.collapse();
    }

    init(){
        $(document).ready(function(){
            if ($(window).width() < 769) {
                let filter_item = $('.filter__group__content.filter__group__content--collapse');
                filter_item.each(function(index) {
                    if ($(this).hasClass('in')){
                        $(this).removeClass('in');
                    }
                });
            }
        });
    }

    uncollapse(){
        $(window).on('resize', function() {
            if ($(window).width() < 769) {
                let filter_item = $('.filter__group__content.filter__group__content--collapse');
                filter_item.each(function(index){
                    $(this).removeClass('in');
                });
            }
        });
    }

    collapse(){
        $(window).on('resize', function() {
            if ($(window).width() > 769) {
                let filter_item = $('.filter__group__content.filter__group__content--collapse');
                filter_item.each(function(index){
                    $(this).addClass('in');
                });
            }
        });
    }

}
export let collapseFilter = new CollapseFilter();








