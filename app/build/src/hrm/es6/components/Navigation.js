export class Navigation {

    constructor() {
        this.hideDropdownStatus = true;
        this.delayTime = 200;
        this.addEventlistener();
    }

    hideDropdown( element ) {
        if ( element === 'all' ) {
            $( '.dropdown' ).removeClass( 'open' );
            return;
        }

        if ( this.hideDropdownStatus ) {
            element.removeClass( 'open' );
        }
    }

    addEventlistener() {
        const that = this;

        $( '.header-menu' ).on( 'mouseenter', function () {
            that.hideDropdown( 'all' );
            that.hideDropdownStatus = true;
        } );

        // Desktop navigtion
        $( 'header' ).find( '.nav-main' )
            .children( 'li' )
            .on( 'mouseenter', function () {
                that.hideDropdown( 'all' );
                that.hideDropdownStatus = true;
            } ).end()

            .find( '.dropdown' )
            .on( 'mouseenter', function () {
                that.hideDropdown( 'all' );
                that.hideDropdownStatus = false;

                $( this )
                    .addClass( 'open' )
                    .find( '.dropdown-toggle' )
                    .attr( "data-toggle", '' );
            } )
            .on( 'mouseleave', function () {
                const $element = $( that );

                that.hideDropdownStatus = true;

                setTimeout( function () {
                    that.hideDropdown( $element );
                }, that.delayTime );
            } ).end()

            .find( '.dropdown-menu ' )
            .on( 'mouseenter', function () {
                that.hideDropdownStatus = false;
            } )
            .on( 'mouseleave', function () {
                that.hideDropdownStatus = true;
                setTimeout( function () {
                    that.hideDropdown( 'all' );
                }, that.delayTime );
            } ).end();
    }
}

export let navigation = new Navigation();