/**
 * Created by ahaddad on 12.04.2017.
 */
import { device } from '../common/Device';
export class PricingMagazineScript {

    constructor() {
        this.init();
    }


    init(){
        $(".magazine-plan").each(function(){
            $(this).on("click",function(){
                var planButton = $(this).find('a');
                $(".magazine-plan").each(function() {
                    if ($(this).hasClass('pricing-table__featured')) {
                        $(this).removeClass('pricing-table__featured');
                    }

                    if ($(this).find('a').hasClass('button-order')) {
                        $(this).find('a').removeClass('button-order');
                        $(this).find('a').addClass('button-gray');
                    }
                });
                $(this).toggleClass('pricing-table__featured');
                planButton.removeClass('button-gray');
                planButton.addClass('button-order');

                var planPos = $(this).data('pos'); // Plan Position
                var planName = $(this).data('name');  // Plan Name
                var planPrice = $(this).data('price');  // Plan Price.
                if (!(device.mobile()) && !(device.tablet())){
                    switch(planPos) {
                        case 0:
                            $('.abo-form__arrow').css('left','15%');
                            break;
                        case 2:
                            $('.abo-form__arrow').css('left','84%');
                            break;
                        default:
                            $('.abo-form__arrow').css('left','50%');
                    }
                }


                $(".abo-form .abo-type").html(planName);
                $(".abo-form .abo-price").html(planPrice);

                $(".abo-form").show();
                $('html, body').animate({
                    scrollTop: ($(".abo-form").offset().top)-250
                }, 850);
            })
        });

        $('.alt-adresse-yes').on("click",function() {
            $(".alt-adresse-form").show()
        })
        $('.alt-adresse-no').on("click",function() {
            $(".alt-adresse-form").hide()
        })
    }
}
export let pricingMagazineScript = new PricingMagazineScript();






