export class Dropdown {

    constructor() {
        let dropdown_item = $('ul.dropdown-menu li a.dropdown-option');
        dropdown_item.each(function(index){
            $(this).on('click',function(){
                let item_value = $(this).html() ;
                let item_parent =  $(this).attr("data-parent");
                $('#'+item_parent).html(item_value);
                $('.'+item_parent+'-parent').html(item_value);
                $('.'+item_parent+'-parent').addClass('first-select');
                $('.'+item_parent).attr('value',item_value.trim());
            }) ;
        });
    }
}

export let dropdown = new Dropdown();








