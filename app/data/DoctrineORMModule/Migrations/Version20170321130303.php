<?php

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170321130303 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE roles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE follow (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, language VARCHAR(255) DEFAULT NULL, platform VARCHAR(255) DEFAULT NULL, user_agent VARCHAR(255) DEFAULT NULL, screen_width VARCHAR(255) DEFAULT NULL, screen_height VARCHAR(255) DEFAULT NULL, screen_available_width VARCHAR(255) DEFAULT NULL, screen_available_height VARCHAR(255) DEFAULT NULL, color_depth VARCHAR(255) DEFAULT NULL, dpi_x VARCHAR(255) DEFAULT NULL, dpi_y VARCHAR(255) DEFAULT NULL, device_pixel_ratio VARCHAR(255) DEFAULT NULL, timezone_offset VARCHAR(255) DEFAULT NULL, java_enabled VARCHAR(255) DEFAULT NULL, acrobat VARCHAR(255) DEFAULT NULL, mime_types LONGTEXT DEFAULT NULL, browser_plugins LONGTEXT DEFAULT NULL, system_colors LONGTEXT DEFAULT NULL, ip_address_v4 VARCHAR(255) DEFAULT NULL, ip_address_v6 VARCHAR(255) DEFAULT NULL, http_referrer VARCHAR(255) DEFAULT NULL, create_date DATETIME NOT NULL, INDEX IDX_68344470A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, id_hash VARCHAR(255) DEFAULT NULL, firstname VARCHAR(128) DEFAULT NULL, lastname VARCHAR(128) DEFAULT NULL, email VARCHAR(128) DEFAULT NULL, password VARCHAR(100) DEFAULT NULL, salt VARCHAR(100) DEFAULT NULL, state INT DEFAULT NULL, create_date DATETIME DEFAULT NULL, last_modified DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_has_role (user_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_EAB8B535A76ED395 (user_id), INDEX IDX_EAB8B535D60322AC (role_id), PRIMARY KEY(user_id, role_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE follow ADD CONSTRAINT FK_68344470A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE user_has_role ADD CONSTRAINT FK_EAB8B535A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE user_has_role ADD CONSTRAINT FK_EAB8B535D60322AC FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_has_role DROP FOREIGN KEY FK_EAB8B535D60322AC');
        $this->addSql('ALTER TABLE follow DROP FOREIGN KEY FK_68344470A76ED395');
        $this->addSql('ALTER TABLE user_has_role DROP FOREIGN KEY FK_EAB8B535A76ED395');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE follow');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE user_has_role');
    }
}
