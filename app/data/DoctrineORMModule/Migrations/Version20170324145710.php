<?php

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170324145710 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE event_submit (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, request VARCHAR(255) DEFAULT NULL, additional_type VARCHAR(255) DEFAULT NULL, from_date DATETIME NOT NULL, from_time VARCHAR(255) DEFAULT NULL, from_full_day TINYINT(1) DEFAULT NULL, until_date DATETIME NOT NULL, until_time VARCHAR(255) DEFAULT NULL, until_full_day TINYINT(1) DEFAULT NULL, message LONGTEXT DEFAULT NULL, guest_register LONGTEXT DEFAULT NULL, guest_costs LONGTEXT DEFAULT NULL, guest_member LONGTEXT DEFAULT NULL, host VARCHAR(255) DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, street VARCHAR(255) DEFAULT NULL, plz VARCHAR(255) DEFAULT NULL, partner VARCHAR(255) DEFAULT NULL, tel VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, create_date DATETIME NOT NULL, INDEX IDX_DA8C411A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE event_submit ADD CONSTRAINT FK_DA8C411A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE event_submit');
    }
}
