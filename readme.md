#readme

## initial setup

make sure your site root is a descendant of your home folder
```
1) docker-compose -p qpweb up -d
2) ./bin/composer_app install
3) docker exec -it $(docker ps -aqf name=qpweb-node) bash
4) npm install --no-bin-links
```


## commands

### docker
```
./bin/docker-start  // starts docker containers for web and api
./bin/docker-stop   // stops all docker container
```

### Database Migrations
```
# show the migrations available
./bin/doctrine-module migrations:status --show-versions
 
# create a new migration file based on entity changes 
# never use this command before the the 'status'-command, so always check for already existing migrations before creating a new diff script
./bin/doctrine-module migrations:diff
 
# migrate to a revision
./bin/doctrine-module migrations:migrate
 
# create a new and empty migration script
./bin/doctrine-module migrations:generate
```

### update composer
```
# run composer on apache containers
./bin/composer_app update
./bin/composer_app install

#run composer on codeanalysis container
./bin/composer_codeanalysis update
./bin/composer_codeanalysis install
```

### build or watch project files
```
./bin/build [projectname (hrm|pr)]
./bin/watch [projectname (hrm|pr)]
```
#### additional npm scripts
* set project to npm config 
```
npm config set QP-WEB:project [hrm|pr]
```
* after setting config, start npm script
```
npm run build
npm run watch
npm run build:es6
npm run watch:es6
npm run build:sass
npm run watch:sass
```