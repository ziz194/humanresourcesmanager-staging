<?php
/**
 * Quadriga Plattform Web
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c] 2016 Quadriga Media GmbH
*/

return [
    'router' => [
        'routes' => [
            'hrm' => [
                'type'    => 'Zend\Mvc\Router\Http\Hostname',
                'options' => [
                    'route'    => 'qpweb.local', // You should add this name to your hosts -file and let them look to your local (docker)machine
                    'defaults' => [
                        'controller' => 'QP\Hrm\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
            ],
            'pr' => [
                'type'    => 'Zend\Mvc\Router\Http\Hostname',
                'options' => [
                    'route'    => 'qpweb-pr.local', // You should add this name to your hosts -file and let them look to your local (docker)machine
                    'defaults' => [
                        'controller' => 'QP\Pr\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
            ],
        ],
    ],
];