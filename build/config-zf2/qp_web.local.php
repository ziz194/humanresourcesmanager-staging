<?php
/**
 * Quadriga Plattform Webfrontend
 *
 * @author      Andy Killat <andy.killat@quadriga.de>
 * @copyright   Copyright (c) 2016 Quadriga Media GmbH
 */

return [
    'doctrine' => [
        'connection' => [
            // default connection name
            'orm_default' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params'      => [
                    'host'          => 'mysql.qp_web.local',
                    'port'          => '3306',
                    'user'          => 'root',
                    'password'      => 'qp_web_pass',
                    'dbname'        => 'qp_web',
                ]
            ]
        ]
    ],
    'application' => [
        'appPath'     => '/var/www/qp_web',
        'hostname'    => 'qp_web.local',
        'environment' => 'development'
    ],
    'api' => [
        'connection' => 'http://192.168.99.100:8030'
    ],
    'mail' => [
        'transport' => [
            'options' => [
                'host'              => 'smtp.gmail.com',
                'connection_class'  => 'login',
                'port'              => '465',
                'connection_config' => [
                    'username' => 'xyz@gmail.com',
                    'password' => 'xyz123',
                    'ssl'      => 'ssl'
                ],
            ],
        ],
        'system_sender_email' => 'webmaster.wm13@gmail.com',
        'my_mail' => 'HERE YOUR EMAIL ADDRESS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',
    ],
];
